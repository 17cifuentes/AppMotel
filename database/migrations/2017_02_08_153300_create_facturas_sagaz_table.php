<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasSagazTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('facturas_sagaz', function (Blueprint $table) {
            $table->increments('id_factura');
            $table->date('fec_expedicion');
            $table->date('fec_vencimieno');
            $table->string('forma_pago');
            $table->integer('cliente_id');
            $table->integer('empleado_id');
            $table->integer('responsable_id');
            $table->string('estado');
            $table->string('orden');
            $table->string('bruto');
            $table->string('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_sagaz');
    }
}

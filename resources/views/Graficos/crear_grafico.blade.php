@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>CREAR GRAFICO</center></h1>
        <h3><span class="input-group-addon"><h3><center>Mantenimiento</center></h3></span></h3>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('almuerzos/elegir') }}" method="POST" enctype="multipart/form-data">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Item:</span>
                <div class="form-line">
                  <select class="form-control" name="item" id="item" >
                    <option value="">Seleccione</option>
                    <option value="finalizada">Facturacion Finalizada</option>
                    <option value="pendiente">Facturacion Pendiente</option>
                    <option value="users">Empleados</option>
                  </select>                  
                </div>
              </div>
            </div>                                
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Consultar por:</span>
                <div class="form-line">
                  <select class="form-control" name="item" id="tiempo" onchange="busqueda(this.value)">
                    <option value="">Seleccione</option>
                    <option value="años">año</option>
                    <option value="trimestres">Trimestre</option>
                    <option value="meses">Mes</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group" id="años">
                <span class="input-group-addon">Año:</span>
                <div class="form-line">
                  <select class="form-control" name="grafico" id="año">
                    <option value="">Seleccione</option>
                    <option value="2017">2017</option>
                  </select>
                </div>
              </div>
            </div>
            </div>
              <div class="row clearfix">
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Trimestre:</span>
                    <div class="form-line">
                      <select class="form-control" name="grafico" id="trimestre">
                        <option value="">Seleccione</option>
                        <option value="1">Primer</option>
                        <option value="2">Segundo</option>
                        <option value="3">Tercer</option>
                        <option value="4">Cuarto</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Mes:</span>
                    <div class="form-line">
                      <select class="form-control" name="grafico" id="mes">
                        <option value="">Seleccione</option>
                        <option value="01">Enero</option>
                        <option value="02">Febrero</option>
                        <option value="03">Marzo</option>
                        <option value="04">Abril</option>
                        <option value="05">Mayo</option>
                        <option value="06">Junio</option>
                        <option value="07">Julio</option>
                        <option value="08">Agosto</option>
                        <option value="09">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Nobiembre</option>
                        <option value="12">Diciembre</option>
                      </select>
                    </div>
                  </div>
                </div>                                
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Tipo de grafico:</span>
                    <div class="form-line">
                      <select class="form-control" name="grafico" id="grafico">
                        <option value="">Seleccione</option>
                        <option value="bar">bar</option>
                        <option value="horizontalBar">horizontalBar</option>
                        <option value="radar">radar</option>
                        <option value="polarArea">polarArea</option>
                        <option value="pie">pie</option>
                        <option value="doughnut">doughnut</option>
                        <option value="line">line</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <center> <a onclick="otros()" class="btn btn-success">Generar Grafico</a></center>
                        <div class="col-lg-12" style="margin-left: 30%">
                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
           <center><div class="card col-lg-8 col-offset-4" style="margin-left: -20%" >
                <div class="header " ><h2>Estadistica Graficas</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="50%" height="23" id="contenedor"></canvas></div>
                </div>
            </div></center>
        </div>                            
                        </div>              
            </div>
          </form>                
        </div>
      </div>
    </div>
    @include('layouts.dash.footer')
<script type="text/javascript">


 function valoresdos(ruta,grafico,item,tiempo,subtiempo){
    
            $.ajax({
            url : "http://localhost/sagaz/public/"+ruta,
            type : "get",
            data : "subtiempo="+subtiempo+"&tiempo="+tiempo+"&item="+item+"&ruta="+ruta,
            success:function(datos){          

             dato=eval(datos);
             numero = dato[0];
             total = dato[1];
             titulo1 = dato[2];
             titulo2 = dato[3];

 
 $("#contenedor").html("");            
var ctx = document.getElementById("contenedor");
var data = {
    datasets: [{
data: [total,numero],
        backgroundColor: [
            "#FF6384",
            "#4BC0C0",
            "#FFCE56",
            "#E7E9ED",
            "#36A2EB"
        ],
        label: 'Datos Estadisticos' // for legend
    }],
labels: [titulo2,titulo1],};
new Chart(ctx, {
    data: data,
    type: grafico
    
});
}
        })
            }

function otros(){

   ruta = "graficos/graficogerente";
   grafico =$("#grafico").val();
   tiempo = $("#tiempo").val();   
   item =$("#item").val();

   //subtiempo = $("#time").val();
   if(tiempo == "años"){
        subtiempo = $("#año").val();
   }else if(tiempo == "trimestres"){
        subtiempo = $("#trimestre").val();
    }else if(tiempo == "meses"){
        subtiempo = $("#mes").val();
    }
valoresdos(ruta,grafico,item,tiempo,subtiempo);

}
function busqueda(tiempo){
    if(tiempo == "años"){
        $("#años").show();
        $("#time").attr("value","año");
        $("#meses").hide();
        $("#trimestres").hide();        
    }else if(tiempo == "trimestres"){
        $("#trimestres").show();
        $("#time").attr("value","trimestre");
        $("#años").hide();
        $("#meses").hide();        
    }else if( tiempo == "meses"){
        $("#meses").show();
        $("#time").attr("value","mes");
        $("#años").hide();
        $("#trimestres").hide();
    }else if( tiempo == ""){
        $("#meses").hide();
        $("#años").hide();
        $("#trimestres").hide();
    }
}
        </script> 

@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>REPORTES ALMUERZOS </center></h1>
      </div>
        <!-- INICIO DE LA PAGINA -->       
        <div class="row clearfix">    
        @include('alertas.notificacion')                      
  <form action="{{ url('reportes/generaralmu') }}" method="POST">
  <br><br><br>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
  <div class="body">
    <h2 class="card-inside-title"></h2>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row clearfix">
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"> Fecha Inicial :</span>
              <div class="form-line">
                  <input type="date" name="inicio" class="form-control">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon">  Fecha Final :</span>
              <div class="form-line">
                  <input type="date" name="fin" class="form-control">
              </div>
            </div>
          </div>                 
          </div>             
        </div><br><br>
        <div class="row clearfix">
          <center>
            <div class="col-md-12">
              
                <button type="submit"  class="btn btn-primary"   name="opc">Reporte Almuerzos</button>
              
            </div>
          </center>
       </div>
   </div>
 </form>
</div>
</div>
        <!--  FIN DE LA PAGINA-->
    </div>
  </div>
</div>

    @include('layouts.dash.footer')

@component('layouts.admin')
  @slot('cargo')
 Reportes Comercial 
 @endslot
   @slot('volver')
 <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
@slot('contenido')
<br><br><br><br><br><br><br>
<div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('reportes/generar') }}" method="POST">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header col-lg-12">
            <div class="col-lg-7">
                <img src="{{ asset('images/Logos/logo.JPG') }}" >
            </div>
        </div> 
    <div class="header">
      <h1><center>Reportes de Cotizacion o Invercion</center></h1>
    </div>
  <div class="body">
    <h2 class="card-inside-title"></h2>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row clearfix">
          <div class="col-md-3">
            <div class="input-group">
              <span class="input-group-addon"><i class="material-icons"></i></span>
                Cliente:
                <div class="form-line">
                  <select class="form-control" name="cliente" onchange="dararea(this.value)">
                    <option value="">-- No Filtrar --</option>
                      @foreach($clientes as $cliente)
                        <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre_cliente }}</option>
                      @endforeach
                  </select>
                </div>
            </div>
          </div>                                
<div class="col-md-3">
  <div class="input-group">
    <span class="input-group-addon"><i class="material-icons"></i></span>
    Negocio/Area (solo aplica para TQ):
      <select name="area" class="form-control" name="responsable" onchange="darresponable(this.value)"  id="area" >
      <option value="">-- No Filtrar --</option>
        @foreach($areas as $area)
          <option value="{{ $area->id_area }}">{{ $area->area_des }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Responsable :
                                            <select class="form-control" name="responsable" id="responsable">
                                                <option value="">-- No Filtrar --</option>
                                            @foreach($responsables as $responsable)
                                                <option value="{{ $responsable->id_responsable }}">{{ $responsable->contacto }}</option>
                                            @endforeach
                                            </select>
                                    </div>
                                </div>                                
                                <div class="col-md-3">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Estado:
                                        <div class="form-line">
                                              <select class="form-control" name="estado" >
                                                <option value="">-- No Filtrar --</option>
                                                <option value="Pendiente">Pendientes</option>
                                                <option value="Anulada">Anuladas</option>
                                                <option value="Finalizado">Finalizadas</option>
                                                <option value="Pendiente-R">Pendiente-R</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div><br><br>
             <div class="row clearfix">
                                <center><div class="col-md-12">
                                    <div class="col-md-6">
                       <button type="submit" style="height:5em" class="btn btn-success btn-block" onclick="ver(this.value)" value="Reporte Filtrado Facturas" name="opc">REPORTE DE FILTROS</button>            
                                  </div>
                                  <div class="col-lg-6">
                       <button type="submit"  class="btn btn-primary btn-block "  style="height:5em" value="Reporte Total Facturas" name="opc">REPORTE TOTAL</button>          
                       </div>
            </div></center>
       </div>
   </div>
 </form>
</div>
<div id="nuevoProducto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <center><button type="button" class="close" data-dismiss="modal">&times;</button>
        <img src="{{ asset('images/Logos/logo.JPG') }}" >
        <h4 class="modal-title">Crear Producto</h4></center>
      </div>
      <div class="modal-body"><br><br>
        <form action="{{ url('productos/registrarpro') }}" method="POST">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">widgets</i>
                    </span>
                    Nombre Producto:
                   <div class="form-line">
                         <input type="text"   class="form-control date" name="nombre_pro">
                   </div>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="input-group input-group-lg">
                    <br><center><input type="submit" value="Registrar" class="btn btn-success btn-lg" ></center>
                                    
                 </div>
            </div>
        </form>
        <br><br><br><br><br><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

@endslot
@endcomponent
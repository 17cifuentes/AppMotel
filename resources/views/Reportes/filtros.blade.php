@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>REPORTES FACTURACIÓN </center></h1>
      </div>
        <!-- INICIO DE LA PAGINA -->       
        <div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('reportes/generar') }}" method="POST">
  <br><br><br>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
  <div class="body">
    <h2 class="card-inside-title"></h2>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row clearfix">
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon">Cliente :</span>
              <div class="form-line">
                <select class="form-control" name="cliente" onchange="dararea(this.value)">
                  <option value="">-- No Filtrar --</option>
                  @foreach($clientes as $cliente)
                      <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre_cliente }}</option>
                    @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"> Neg/Area (aplica para TQ):</span>
              <div class="form-line">
                <select name="area" class="form-control" name="responsable" onchange="darresponable(this.value)"  id="area" >
                  <option value="">-- No Filtrar --</option>
                  @foreach($areas as $area)
                    <option value="{{ $area->id_area }}">{{ $area->area_des }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>          
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"> Responsable:</span>
              <div class="form-line">
                <select class="form-control" name="responsable" id="responsable">
                  <option value="">-- No Filtrar --</option>
                  @foreach($responsables as $responsable)
                    <option value="{{ $responsable->id_responsable }}">{{ $responsable->contacto }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"> Estado:</span>
              <div class="form-line">
                <select class="form-control" name="estado" >
                  <option value="">-- No Filtrar --</option>
                  <option value="Pendiente">Pendientes</option>
                  <option value="Anulada">Anuladas</option>
                  <option value="Finalizado">Finalizadas</option>
                  <option value="Pendiente-R">Pendiente-R</option>
                </select>
              </div>
            </div>
          </div>             
        </div><br><br>
        <div class="row clearfix">
          <center>
            <div class="col-md-12">
             <!-- <div class="col-md-6">
                <button type="submit" style="height:5em" class="btn btn-success btn-block" onclick="ver(this.value)" value="Reporte Filtrado Facturas" name="opc">REPORTE DE FILTROS</button>            
              </div>-->
              <div class="col-lg-6">
                <button type="submit"  class="btn btn-primary btn-block "  style="height:5em" value="Reporte Total Facturas" name="opc">REPORTE TOTAL</button>
              </div>
            </div>
          </center>
       </div>
   </div>
 </form>
</div>
        <!--  FIN DE LA PAGINA-->
    </div>
  </div>
</div>

    @include('layouts.dash.footer')

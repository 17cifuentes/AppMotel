@component('layouts.admin')
@slot('cargo') Recursos Humanos  @endslot
@slot('volver')
  <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
@endslot
@slot('contenido')
  <br><br><br><br><br><br><br>
  <div class="row clearfix">    
  @include('alertas.notificacion')                      
    <form action="{{ url('reportes/generaralmu') }}" method="POST">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header col-lg-12">
              <div class="col-lg-7">
                  <img src="{{ asset('images/Logos/logo.JPG') }}" >
              </div>
          </div> 
      <div class="header">
        <h1><center>Reportes Almuerzos Sagaz	</center></h1>
      </div>
    <div class="body">
      <h2 class="card-inside-title"></h2>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">                                
  <div class="col-md-6">
    <div class="input-group">
      <span class="input-group-addon"><i class="material-icons"></i></span>
      Fecha Inicial:
  <input type="date" name="inicio">
      </div>
    </div>
    <div class="col-md-6">
      <div class="input-group">
          <span class="input-group-addon"><i class="material-icons"></i></span>
            Fecha Final :
  <input type="date" name="fin">
        </div> 
    </div>                                
  </div>
  <br><br>
    <div class="row clearfix">
      <center>
        <div class="col-md-12">
          <div class="col-md-6">
            <button type="submit" style="height:5em" class="btn btn-success btn-block"  value="Reporte Filtrado Facturas" name="opc">REPORTE DE FILtrOS</button>            
          </div>
         </div>
      </center>
    </div>
  </div>

  </form>
 
@endslot
@endcomponent
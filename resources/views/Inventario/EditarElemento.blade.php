@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>Editar Elemento {{ "IN-".$elemento[0]['id']." ".$elemento[0]['nombre'] }} Del Inventario</center></h1>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('inventario/editarpost') }}" method="POST">
      <input type="hidden" name="id" value="{{ $elemento[0]['id'] }}">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">                               
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon"> Estado <b style="color: orange">{{$elemento[0]['estado'] }}</b>:</span>
                <input type="hidden" name="estadofijo" value="{{$elemento[0]['estado'] }}">
                <div class="form-line">
                                                              <select class="form-control" name="estado">
                                                <option value="">Seleccione</option>
                                                <option value="Buen Estado">Buen Estado</option>
                                                <option value="Daños Fisicos">Daños Fisicos</option>
                                                <option value="Daños Internos">Daños Internos</option>
                                                <option value="Mal Estado">Mal Estado</option>
                                                <option value="Dañado">Dañado</option>
                                            </select>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Cantidad: </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="cantidad" value="{{ $elemento[0]['cantidad']  }}">
                   
                </div>
              </div>
            </div>            
            </div>
              <div class="row clearfix">
                <div class="col-md-12">
                  <div class="input-group">
                   <span class="input-group-addon"> Observacion del Estado:</span>
                    <div class="form-line">
					             	<textarea class="form-control" name="observacion"  placeholder="{{ $elemento[0]['estado']  }}"></textarea>
                        <input type="hidden" name="observacionfijo" value="{{ $elemento[0]['observacion']  }}">
                    </div>
                  </div>
                </div>
              </div>
              <center><input type="submit" value="Registrar" class="btn btn-success"></center>
            </div>
          </form>                
        </div>
      </div>
    </div>
    @include('layouts.dash.footer')

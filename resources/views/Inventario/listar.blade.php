@include('layouts.dash.header')
<meta charset="utf-8">
@include('layouts.dash.menu')
<head>
	<title></title>
</head>
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>INVENTARIO </center></h1>
      </div>
      @include('alertas.notificacion')
<body>
    <div class="col-lg-12"><a data-toggle="modal" data-target="#myModal" class="btn btn-success "> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"> </span> Generar una Solicitud de inventario</a></a><br></div><br>
	<table id="example" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Tipo</th>
                <th>Elemento</th>
                <th>Estado</th>
                <th>Observacion</th>
                <th>Catidad</th>
                <th>Eliminar</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Codigo</th>
                <th>Tipo</th>
                <th>Elemento</th>
                <th>Estado</th>
                <th>Observacion</th>
                <th>Catidad</th>
                <th>Eliminar</th>
                <th>Editar</th>              
            </tr>
        </tfoot>
        <tbody>
            @foreach($elementos as $elemento)
                <tr>
                    <th>IN-{{ $elemento->id }}</th>
                    <td>{{ $elemento->tipo_elemento }}</td>
                    <td>{{ $elemento->nombre}}</td>
                    <td>{{ $elemento->estado }}</td>
                    <td>{{ $elemento->observacion }}</td>
                    <td>{{ $elemento->cantidad }}</td>
                    <td><a class="btn btn-danger" href="{{ url('inventario/eliminar?id='.$elemento->id) }}">Eliminar</a></td>
                    <td><a class="btn btn-warning" href="{{ url('inventario/editar?id='.$elemento->id) }}">Editar</a></td>
                </tr>
            @endforeach 
        </tbody>
    </table>
</body>
<script src="https://code.jquery.com/jquery-1.12.4.js" ></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js" ></script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
} );
$(document).ready(function() {
    var table = $('#example1').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
} );
</script>   

    <!-- jQuery 
    <script src="{{ asset('dash/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('dash/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('dash/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{ asset('dash/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js')}} -->
    <!-- <script src="{{ asset('dash/vendors/Chart.js')}}/dist/Chart.min.js')}}"></script>-->
    <!-- jQuery Sparklines -->
    <script src="{{ asset('dash/vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- morris.js')}} -->
    <script src="{{ asset('dash/vendors/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('dash/vendors/morris.js/morris.js')}}"></script>
    <!-- gauge.js')}} -->
    <script src="{{ asset('dash/vendors/gauge.js/dist/gauge.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('dash/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{ asset('dash/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('dash/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{ asset('dash/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{ asset('dash/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{ asset('dash/vendors/DateJS/build/date.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('dash/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('dash/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('dash/build/js/custom.min.js')}}"></script>
    
 
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background:orange">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 style="color:white" class="modal-title"><center>Solicitud de inventario</center></h1>
      </div>
      <div class="modal-body">
          <div class="col-lg-12"><h1>SLI- #1</h1><hr><h4>Por medio del presente formulario se hace solicitud formal de elementos dentro o fuera del stock de la empresa, para ello se mostraran los datos obtenidos y los elementos necesarios para el cumplimiento exitoso del proceso.</h4></div><hr>
          <form>
            <div class="col-md-6">
              <div class="input-group">
                <span class="input-group-addon">Solicitante: </span>
                <div class="form-line">
                    <input disabled type="text" class="form-control" name="cantidad" value="{{ Auth::user()->name }}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group">
                <span class="input-group-addon">Fecha: </span>
                <div class="form-line">
                    <input disabled type="text" class="form-control" name="cantidad" value="{{ date("y-m-d") }}">
                </div>
              </div>
            </div>
            <hr><br><br>
            <h2><center>Elementos Solicitados</center></h2>
          	<table id="example2" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Elemento</th>
                <th>Catidad</th>
                <th>Borrar</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
          </form>
<div id="exTab1" class="container">	
<ul  class="nav nav-pills">
			<li class="active">
        <a  href="#1a" data-toggle="tab">Agregar elemento de Stock</a>
			</li>
			<li ><a href="#3a" data-toggle="tab">Nuevo Elemento</a>
			</li>
		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
          
          	<table id="example1" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Tipo</th>
                <th>Elemento</th>
                <th>Estado</th>
                <th>Observacion</th>
                <th>Catidad</th>
                <th>Agregar</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Codigo</th>
                <th>Tipo</th>
                <th>Elemento</th>
                <th>Estado</th>
                <th>Observacion</th>
                <th>Catidad</th>
                <th>Eliminar</th>
                <th>Editar</th>              
            </tr>
        </tfoot>
        <tbody>
            @foreach($elementos as $elemento)
                <tr>
                    <th>IN-{{ $elemento->id }}</th>
                    <td>{{ $elemento->tipo_elemento }}</td>
                    <td>{{ $elemento->nombre}}</td>
                    <td>{{ $elemento->estado }}</td>
                    <td>{{ $elemento->observacion }}</td>
                    <td>{{ $elemento->cantidad }}</td>
                    <td><a class="btn btn-info" href="{{ url('inventario/eliminar?id='.$elemento->id) }}">Agregar</a></td>
                </tr>
            @endforeach 
        </tbody>
    </table>
				</div>
        <div class="tab-pane" id="3a"><br><br>
                      <div class="col-md-6">
              <div class="input-group">
                <span class="input-group-addon">Descripcion del producto: </span>
                <div class="form-line">
                    <textarea class="form-control"></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group">
                <span class="input-group-addon">Cantidad: </span>
                <div class="form-line">
                    <input  type="text" class="form-control" name="cantidad">
                </div>
              </div>
            </div
           
				</div><br>
		<div class="col-lg-12"><center><input type="submit" class="btn btn-success" value="Registrar"></center></div>
			</div>
  </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Enviar Solicitud</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div> 
</html>


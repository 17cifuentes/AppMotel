@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>CARGAR PLANTILLA DE INVENTARIO</center></h1>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('inventario/registrar') }}" method="POST">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">
            <div class="col-md-3">
              <div class="input-group">
                <span class="input-group-addon">Plantilla:</span>
                <div class="form-line">
                    <a class="btn btn-success" href="http://www.sagazsas.com/archivos/PlantillaParaInventario.xlsx" download="PlantillaInventario.xlsx">Descargar</a>
                </div>
              </div>
            </div>          
            <div class="col-md-3">
              <div class="input-group">
                <span class="input-group-addon">Cargar: </span>
                <div class="form-line">
                    <input type="file" class="btn btn-success" name="plantilla">
                </div>
              </div>
            </div>
            
              <center><input type="submit" value="Registrar" class="btn btn-success"></center>
            </div>
          </form>                
        </div>
      </div>
    </div>
    @include('layouts.dash.footer')

@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
  <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
@slot('contenido')
 <br><br><br><br><br><br> 


<!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @include('alertas.notificacion') 
                    <div class="card">
                        <div class="header">
                            <center><h1>
                               Inventario Sagaz S.A.S
                            </h1><br>
                            <a class="btn btn-info" href="{{ url('inventario/crear') }}">Crear elemento</a></center>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <?php if($elementos["total"] != null){ echo "<center><h3>No hay elementos registrados</h3></center><br>";?>

                            <img src="{{ asset('images/adicionales/ZORRITO.jpg') }}" style="height:20em">
                            <?php }else{?>                        
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Tipo de Elemento</th>
                                        <th>Elemento</th>
                                        <th>Estado</th>
                                        <th>Observacion</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($elementos as $elemento)
                                    <tr>
                                        <th>SAG-{{ $elemento->id }}</th>
                                        <td>{{ $elemento->tipo_elemento }}</td>
                                        <td>{{ $elemento->nombre}}</td>
                                        <td>{{ $elemento->estado }}</td>
                                        <td>{{ $elemento->observacion }}</td>
                                        <td><a class="btn btn-danger" href="{{ url('inventario/eliminar?id='.$elemento->id) }}">Eliminar</a></td>
                                        <td><a class="btn btn-warning" href="{{ url('inventario/editar?id='.$elemento->id) }}">Editar</a></td>
                                    </tr>
                                    @endforeach
          
                                </tbody>
                            </table>
                            <?php  } ?>
                            {{ $elementos->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
     

        @endslot
        <script type="text/javascript">
        $(function () {
    $('.js-basic-example').DataTable();

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
        </script>
        @endcomponent
@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>INGRESAR ELEMENTO A INVENTARIO</center></h1>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('inventario/registrar') }}" method="POST">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">
            <div class="col-md-3">
              <div class="input-group">
                <span class="input-group-addon">Tipo de Elemento:</span>
                <div class="form-line">
                                            <select class="form-control" name="tipo_elemento" onchange="darelemento(this.value)">
                                                <option value="">Seleccione</option>
                                                <option value="Mubles">Muebles</option>
                                                <option value="Computacion">Elementos de computacion</option>
                                                <option value="Comunicacion">Elementos de Comunicacion</option>
                                                <option value="Cafeteria y Aseo">Cafeteria y Aseo</option>
                                                <option value="Mercado">Mercado Relacional</option>
                                                <option value="Impresion">Impresion</option>
                                                <option value="">Otros</option>
                                            </select>
                </div>
              </div>
            </div>                                
            <div class="col-md-3">
              <div class="input-group">
                <span class="input-group-addon">Elemento: </span>
                <div class="form-line">
                                                             <select name="elemento" class="form-control"  id="elemento" >
                                                <option value="">-- No Filtrar --</option>

                                            </select>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <span class="input-group-addon">Cantidad: </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="cantidad">
                </div>
              </div>
            </div>            
            <div class="col-md-3">
              <div class="input-group">
                <span class="input-group-addon"> Estado:</span>
                <div class="form-line">
                                                              <select class="form-control" name="estado">
                                                <option value="">Seleccione</option>
                                                <option value="Buen Estado">Buen Estado</option>
                                                <option value="Daños Fisicos">Daños Fisicos</option>
                                                <option value="Daños Internos">Daños Internos</option>
                                                <option value="Mal Estado">Mal Estado</option>
                                                <option value="Dañado">Dañado</option>
                                            </select>
                </div>
              </div>
            </div>
            </div>
              <div class="row clearfix">
                <div class="col-md-12">
                  <div class="input-group">
                   <span class="input-group-addon"> Observacion del Estado:</span>
                    <div class="form-line">
						<textarea class="form-control" name="observacion"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <center><input type="submit" value="Registrar" class="btn btn-success"></center>
            </div>
          </form>                
        </div>
      </div>
    </div>
    @include('layouts.dash.footer')

@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
  <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
@slot('contenido')
 <br><br><br><br><br><br> 
<div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('inventario/registrar') }}" method="POST">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1>
                                <center>INGRESAR ELEMENTO A INVENTARIO	</center>
                            </h1>
                        </div>
                              <div class="header col-lg-12">
                              	<div class="col-lg-7">
                            	<img src="{{ asset('images/Logos/logo.JPG') }}" >
                              	</div>
                                <div><h3>Codigo de Elemento</h3><h3>SAG-{{ $codigo + 1 }}</h3></div>
					     </div> <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">

                                 <div class="col-md-3">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                             <i class="material-icons"></i>
                                        </span>
                                        Tipo de Elemento:
                                        <div class="form-line">
                                            <select class="form-control" name="tipo_elemento" onchange="darelemento(this.value)">
                                                <option value="">Seleccione</option>
                                                <option value="Mubles">Muebles</option>
                                                <option value="Computacion">Elementos de computacion</option>
                                                <option value="Comunicacion">Elementos de Comunicacion</option>
                                                <option value="Cafeteria y Aseo">Cafeteria y Aseo</option>
                                                <option value="Mercado">Mercado Relacional</option>
                                                <option value="Impresion">Impresion</option>
                                                <option value="">Otros</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>   
                               
                                <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Elemento:
                                            <select name="elemento" class="form-control"  id="elemento" >
                                                <option value="">-- No Filtrar --</option>

                                            </select>
                                    </div>
                                </div>                                                              
                                <div class="col-md-3">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Estado:
                                        <div class="form-line">
                                            <select class="form-control" name="estado">
                                                <option value="">Seleccione</option>
                                                <option value="Buen Estado">Buen Estado</option>
                                                <option value="Daños Fisicos">Daños Fisicos</option>
                                                <option value="Daños Internos">Daños Internos</option>
                                                <option value="Mal Estado">Mal Estado</option>
                                                <option value="Dañado">Dañado</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Observacion del Estado:
                                        <div class="form-line">
                                            <textarea class="form-control" name="observacion"></textarea>
                                        </div>

                                    </div>
                                </div>                                                              
                            </div>

                            <div class="col-lg-12">


                            </div>


                           <div class="row clearfix">
                                <div class="col-md-12">
                                   
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
									<center><input type="submit" value="Registrar" class="btn btn-success btn-lg" ></center>
                                    </div>
                                </div>
                                 
                             

                                                       </div>
                    </div>
                </form>
                </div>

@endslot
@endcomponent
@extends('layouts.head')

@section('content')
<style type="text/css">
	#mov1{
 margin:0;
 padding: 0;
 position: relative;
 width: 60px;
 height: 40px;
 background-color: #3B5A9A;
 transition-duration:0.5s;
}
 
#mov2{
 margin:0;
 padding: 0;
 position: relative;
 width: 60px;
 height: 40px;
 background-color: #DF4B38;
 transition-duration:0.5s;
}
 
#mov3{
margin:0;
 padding: 0;
 position: relative;
 width: 60px;
 height: 40px;
 background-color: #117BB8;
 transition-duration:0.5s;
}
 
#mov4{
 margin:0;
 padding: 0;
 position: relative;
 width: 60px;
 height: 40px;
 background-color: #26ADE3;
 transition-duration:0.5s;
}
#mov6{
 margin:0;
 padding: 0;
 position: relative;
 width: 60px;
 height: 40px;
 background-color: #05AF2C;
 transition-duration:0.5s;
}
 
#mov5{
 margin:0;
 padding: 0;
 position: relative;
 width: 60px;
 height: 40px;
 background-color: #29A9E1;
 transition-duration:0.5s;
}
 
#mov1:hover{
 width: 100px;
}
 
#mov2:hover{
 width: 100px;
}
 
#mov3:hover{
 width: 100px;
}
 
#mov4:hover{
 width: 100px;
}
 
#mov5:hover{
 width: 100px;
}
#mov6:hover{
 width: 100px;
}
 
#redes{
 z-index: 1000;
 position: fixed;
 bottom: 40%;
 left: -10px;
}

</style>

    <!--
        Fixed Navigation
        ==================================== -->
    <header id="navigation" class="navbar-inverse navbar-fixed-top">
        <div class="container">
        @include('alertas.notificacion')
            <div class="navbar-header">
                <!-- responsive nav button -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- /responsive nav button -->
            </div>

            <!-- main nav -->
            <nav class="collapse navbar-collapse navbar-right" role="navigation">
                <ul id="nav" class="nav navbar-nav">
                    <li><a class="submenu" href="#body">Inicio</a></li>

                </ul>
                <ul class="nav navbar-nav"> 
                 <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Ingresar
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" style="background:rgba(0, 0, 0, 0.58);">
                            <li class="scroll"><a class="submenu" href="{{ url('/corporativo')}}"> Acceso</a></li>
                        </ul>
                    </li>
                </ul>

            </nav>
            <!-- /main nav -->

        </div>
    </header>
    
@include('layouts.registro')
    <main class="site-content" role="main">

        <!-- SECCION SLIDER SAGAZ INICIO -->
        <section id="home-slider">
            <div id="slider" class="sl-slider-wrapper">

                <div class="sl-slider">

                    <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

                        <div class="bg-img bg-img-1"></div>

                    </div>

                    <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">

                        <div class="bg-img bg-img-2"></div>

                    </div>

                    <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">

                        <div class="bg-img bg-img-3"></div>

                    </div>

                </div>
                <!-- /sl-slider -->

                <!-- 
	                <nav id="nav-arrows" class="nav-arrows">
	                    <span class="nav-arrow-prev">Previous</span>
	                    <span class="nav-arrow-next">Next</span>
	                </nav>
	                -->

                <nav id="nav-arrows" class="nav-arrows hidden-xs hidden-sm visible-md visible-lg">
                    <a href="javascript:;" class="sl-prev">
                        <i class="fa fa-angle-left fa-3x"></i>
                    </a>
                    <a href="javascript:;" class="sl-next">
                        <i class="fa fa-angle-right fa-3x"></i>
                    </a>
                </nav>

                <nav id="nav-dots" class="nav-dots visible-xs visible-sm hidden-md hidden-lg">
                    <span class="nav-dot-current"></span>
                    <span></span>
                    <span></span>
                </nav>

            </div>
        </section>
        <!-- SECCION SLIDER SAGAZ FIN -->



 

 

        <!-- SECCION CONTACTENOS INICIO -->
        <section id="contact">
            <div class="container">
                <div class="row animatedParent">
<div class="animated bounceInLeft slower">
                    <div class="sec-title text-center">
                        <h2>Contacto</h2>
                    </div>


                    <div class="col-md-5">
                        <address class="contact-details">
								<h3>Contáctanos</h3>						
								<p>
								<strong>Sede Principal - Cali  </strong>  <br>
								<i class="fa fa-phone" aria-hidden="true"></i>3876790 <br>
								<i class="fa fa-mobile" aria-hidden="true"></i> 57+ 318 415 2099 <br>
								<i class="fa fa-road" aria-hidden="true"></i>Calle 12N # 9N-59 Ofc. 201 <br>
								<i class="fa fa-envelope"></i>info@sagazsas.com<br>
								</p>
								<br>
								<p>
								<!--<strong>Sede Buenaventura </strong>  <br>
								<i class="fa fa-phone" aria-hidden="true"></i>2427118 <br>
								<i class="fa fa-road" aria-hidden="true"></i>Calle 4 # 38B-18 B/ 14 de Julio <br>
								<i class="fa fa-envelope"></i>rarias@sagazsas.com.co <br>
								</p>
								<br><br>
								<h3>Horarios Laborales</h3>						
								<p>
								<strong>Lunes-Viernes: </strong> 8:00 am - 6:00pm <br>
								<strong>Sabados: </strong> Cerrado <br>
								<strong>Domingos:</strong> Cerrado <br>
								</p>-->
								<br>
							</address>
                    </div>
</div>
                </div>
            </div>
        </section>
        <!-- SECCION CONTACTENOS FIN -->

        <!-- SECCION GOOGLE MAP INICIO -->
        <section id="google-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.55475453732!2d-76.53840628569947!3d3.4578162974832756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e30a66e11f04acf%3A0xd9a7d101ec9a1fbe!2ssagaz+sas!5e0!3m2!1ses-419!2sco!4v1485356565750" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
        </section>
        <!-- SECCION GOOGLE MAP FIN -->
    </main>


    


@endsection
﻿@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')Financiero @endslot
 @slot('contenido')
<div class="container-fluid">
   <br><br><br><br>
<br><br><br><br>
<center>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/fac.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Facturación</h2></center>
                <p>
                    <a href="{{ url('financiero/index') }}" class="btn btn-success">Facturar</a> 
                    <a href="{{ url('financiero/listar') }}" class="btn btn-success">Listar</a> 
                    <a href="{{ url('reportes/filtros') }}" class="btn btn-success">Reportes</a>
                </p>  
            </figcaption>
        </figure>
    </li>

</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/animation-bg.jpg')}}" alt=""/>
            <figcaption>
               <center> <h2>Cotización</h2></center>
                <p>
                    <a href="{{ url('cotizacion/cotizar') }}" class="btn btn-success">Cotizar</a> 
                    <a href="{{ url('cotizacion/listar') }}" class="btn btn-success">Listar</a> 
                 </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/animation-bg.jpg')}}" alt=""/>
            <figcaption>
                            <h2><span>Productos </span></h2>
                            <p>
                                <a href="{{ url('productos/crear') }}" class="btn btn-success">Crear</a> 
                                <a href="{{ url('productos/index') }}" class="btn btn-success">Listar</a> 
                            </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/animation-bg.jpg')}}" alt=""/>
            <figcaption>
                            <h2><span>Clientes</span></h2>
                            <p>
                                <a href="{{ url('clientes/crear') }}" class="btn btn-success">Crear</a> 
                                <a href="{{ url('clientes/index') }}" class="btn btn-success">Listar</a> 
                            </p>
            </figcaption>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/animation-bg.jpg')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Responsable</span></h2>
                            <p>
                                <a href="{{ url('responsables/crear') }}" class="btn btn-success">Crear</a> 
                                <a href="{{ url('responsables/index') }}" class="btn btn-success">Listar</a> 
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/animation-bg.jpg')}}" alt=""/>
            <figcaption>
                            <h2><span>INVERSION</span></h2>
                            <p>
                                <a href="{{ url('clientes/index') }}" class="btn btn-success">Hacer Aporte</a> 
                                <a href="{{ url('inversion/listar') }}" class="btn btn-success">Listar</a> 
                            </p> 
            </figcaption>
        </figure>
    </li>
</ul>

   </center>        
    

               
                    </div>
                </div>

 
     @endslot
   @endcomponent
@component('layouts.viewcorparativo')
    @slot('titulo')Corporativo Sagaz @endslot
    @slot('contenido')<br><br><br><br>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('images/sagaz.ico') }}">
    <div  class="clearfix col-lg-12" >
        <div  class="col-lg-6 col-md-4 col-sm-6 col-xs-12 col-lg-offset-3">
            <div class="card " >
                @if(session("error"))
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Upps!</strong>{{ session("error") }}
                    </div>
                @endif
                <div class="alert alert-warning header" >
                    <center><h2 style="color:white"><img style="width:30%" src="{{ asset('images/iconos/sag.png') }}"><br>Ingreso personal sagaz</h2></center>
                </div>
                <div class="body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">    {{ csrf_field() }}
                        <input type="hidden" name="_token" value="<?php  echo csrf_token(); ?>">
                        <div class="fogrorm-up{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Usuario</label>
                            <div class="col-md-6">
                                <input  id="email" type="email" class="form-control" name="email" required>
                            </div>
                        </div>
                        <div class="formg-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input  id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-5">
                                <button type="submit" class="btn btn-default btn-lg">Ingresar</button>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form><br>              
                </div>
            </div>
        </div>
    </div>
    <!--FIN DEL INICIO DE SESION  // ESPACIO DE ESTADISTICAS FINANCIERAS SAGAZ-->
    <h3 ><br><br><br><center>GRAFICOS ESTADISTICOS FINANCIERO</center></h3><hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico2"></canvas></div>
                </div>
            </div>
        </div>
        <h3><br><br><br><center>LOGISTICA</center></h3><hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico3"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico4"></canvas></div>
                </div>
            </div>
        </div>
    <!--  ESPACIO DE ESTADISTICAS LOGISTICA SAGAZ-->
        <h3 ><br><br><br><center>DISEÑO Y PUBLICIDAD</center></h3><hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico5"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico6"></canvas></div>
                </div>
            </div>
        </div>
    <!--  ESPACIO DE ESTADISTICAS DISEÑO SAGAZ-->
        <h3><br><br><br><center>LOGISTICA</center></h3><hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico7"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico8"></canvas></div>
                </div>
            </div>
        </div>
        <!--  ESPACIO DE ESTADISTICAS FINANCIERAS SAGAZ-->
        <h3 ><center>OTROS</center></h3>
       <hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico9"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Estadistica de facturacion</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="49" height="25" id="grafico10"></canvas></div>
                </div>
            </div>
        </div>
        <canvas id="grafico"></canvas>
        <script type="text/javascript" src="{{ asset('js/Graficos/admingrafico.js') }}"></script>
    @endslot @endcomponent
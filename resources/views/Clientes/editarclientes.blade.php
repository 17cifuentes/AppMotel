@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>Editar Cliente  {{ $cliente[0]->nombre_cliente }} </center></h1>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('clientes/update') }}" method="POST">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Nombre Cliente :</span>
                <div class="form-line">
                  <input type="text" class="form-control date"  value="{{ $cliente[0]->nombre_cliente }}" name="nombre">
                  <input type="hidden" class="form-control date"  value="{{ $cliente[0]->id_cliente }}" name="id">
                </div>
              </div>
            </div>                                
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Nit/Documento :</span>
                <div class="form-line">
                  <input type="text" value="{{ $cliente[0]->nit }}" class="form-control date" name="nit">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Ciudad:</span>
                <div class="form-line">
                  <input type="text" value="{{ $cliente[0]->ciudad }}" class="form-control date" name="ciudad" placeholder="Orden De Compra">
                </div>
              </div>
            </div>
            </div>
              <div class="row clearfix">
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Direccion :</span>
                    <div class="form-line">
                      <input type="text" class="form-control date" name="direccion" value="{{ $cliente[0]->direccion }}">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Telefono:</span>
                    <div class="form-line">
                      <input type="number" class="form-control date" name="telefono" value="{{ $cliente[0]->telefono }}">
                    </div>
                  </div>
                </div>                                
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Correo :</span>
                    <div class="form-line">
                      <div class="form-line">
                        <input type="email" class="form-control date" name="correo" value="{{ $cliente[0]->correo }}">
                    </div>
                  </div>
                </div>
              </div>
              <center><input type="submit" value="Registrar" class="btn btn-success"></center>
            </div>
          </form>                
        </div>
      </div>
    </div>
  </div>
  @include('layouts.dash.footer')

@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
 <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 @endslot
@slot('contenido')
 <br><br><br><br><br><br> 
<div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('clientes/registrar') }}" method="POST">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1>
                                <center>CREAR CLIENTE	</center>
                            </h1>
                        </div>
                              <div class="header col-lg-12">
                              	<div class="col-lg-7">
                            	<img src="{{ asset('images/Logos/logo.JPG') }}" >
                              	</div>
					     </div> <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                             <i class="material-icons"></i>
                                        </span>
                                        Nombre Cliente :
                                        <div class="form-line">
                                            <input type="text" class="form-control date" id="fec_expedicion" name="nombre">
                                        </div>

                                    </div>
                                </div>                                
                                <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Nit :
                                        <div class="form-line">
                                            <input type="text" id="venci" class="form-control date" name="nit">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Ciudad:
                                        <div class="form-line">
                                            <input type="text" class="form-control date" name="ciudad" placeholder="Ciudad">
                                        </div>

                                    </div>
                                </div>
                                                                <div class="col-md-3">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>

                                        Direccion :
                                        <div class="form-line">
                                           <input type="text" class="form-control date" name="direccion" placeholder="Direccion">
                                        </div>
                                    </div>
                                </div>                                
                            </div>

                            <div class="col-lg-12">


                            </div>


                            <div class="row clearfix ">

                                <div class="col-md-3">
                                    <p>
                                        <b>Telefono :</b>
                                    </p>
                                    <div class="input-group input-group-s,">

                                        <div class="form-line">
											<input type="number" class="form-control date" name="telefono" placeholder="Telefono">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <p>
                                        <b>Correo :</b>
                                    </p>
                                    <div class="input-group input-group-s,">

                                        <div class="form-line">
											<input type="email" class="form-control date" name="correo" placeholder="Correo">
                                        </div>
                                    </div>
                                </div>
                                                                
                            </div>
                           <div class="row clearfix">
                                <div class="col-md-12">
                                   
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
									<center><input type="submit" value="Registrar" class="btn btn-success btn-lg" ></center>
                                    </div>
                                </div>
                                 
                             

                                                       </div>
                    </div>
                </form>
                </div>
<div id="nuevoProducto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <center><button type="button" class="close" data-dismiss="modal">&times;</button>
        <img src="{{ asset('images/Logos/logo.JPG') }}" >
        <h4 class="modal-title">Crear Area</h4></center>
      </div>
      <div class="modal-body"><br><br>
        <form action="{{ url('productos/registrarpro') }}" method="POST">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">*</i>
                    </span>
                    Nombre Area:
                   <div class="form-line">
                         <input type="text"   class="form-control date" name="nombre_pro">
                   </div>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="input-group input-group-lg">
                    <br><center><input type="submit" value="Registrar" class="btn btn-success btn-lg" ></center>
                                    
                 </div>
            </div>
        </form>
        <br><br><br><br><br><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

@endslot
@endcomponent
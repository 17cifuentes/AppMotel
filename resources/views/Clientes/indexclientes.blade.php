@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>LISTA CLIENTES </center></h1>
      </div>
      <center><a class="btn btn-info" href="{{ url('clientes/crear') }}">Crear Cliente</a></center>
      @include('alertas.notificacion')
      <div class="body table-responsive">
        <?php $dos = $clientes->ToArray();if($dos["total"] == 0){ echo "<center><h3>No hay Clientes registrados</h3></center>";?>
        <img src="{{ asset('images/adicionales/ZORRITO.jpg') }}" style="height:20em">
        <?php }else{?>
        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
          <thead>
            <tr>
              <th>Nombre Cliente</th>
              <th>Nit</th>
              <th>Ciudad</th>
              <th>Direccion</th>
              <th>Telefono</th>
              <th>Correo</th>
              <th>Estado</th>
              <th>Activar</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            @foreach($clientes as $cliente)
            <tr>
              <td>{{ $cliente->nombre_cliente }}</td>
              <td>{{ $cliente->nit }}</td>
              <td>{{ $cliente->ciudad }}</td>
              <td>{{ $cliente->direccion }}</td>
              <td>{{ $cliente->telefono }}</td>
              <td>{{ $cliente->correo }}</td>
              <td>{{ $cliente->estado }}</td>
              @if($cliente->estado == "Activo")
                <td class="success">No Aplica</td>
              @else
              <td><a class="btn btn-success" href="{{ url('clientes/activar?id='.$cliente->id_cliente) }}">Activar</a></td>
              @endif
              <td><a class="btn btn-warning" href="{{ url('clientes/editar?id='.$cliente->id_cliente) }}">Editar</a></td>
              <td><a class="btn btn-danger" href="{{ url('clientes/eliminar?id='.$cliente->id_cliente) }}">Eliminar</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <?php } ?>
      {!! $clientes->render() !!} 
      </div>
    </div>
  </div>
</div>
@include('layouts.dash.footer')

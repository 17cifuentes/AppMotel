@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>CREAR CLIENTE </center></h1>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('clientes/registrar') }}" method="POST">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Nombre Cliente :</span>
                <div class="form-line">
                  <input type="text" class="form-control date" id="fec_expedicion" name="nombre">
                </div>
              </div>
            </div>                                
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Nit/Documento :</span>
                <div class="form-line">
                  <input type="text" id="venci" class="form-control date" name="nit">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Ciudad:</span>
                <div class="form-line">
                  <input type="text" class="form-control date" name="ciudad" placeholder="Ciudad">
                </div>
              </div>
            </div>
            </div>
              <div class="row clearfix">
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Direccion :</span>
                    <div class="form-line">
                      <input type="text" class="form-control date" name="direccion" placeholder="Direccion">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Telefono:</span>
                    <div class="form-line">
                      <input type="number" class="form-control date" name="telefono" placeholder="Telefono">
                    </div>
                  </div>
                </div>                                
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Correo :</span>
                    <div class="form-line">
                      <div class="form-line">
                      <input type="email" class="form-control date" name="correo" placeholder="Correo">
                    </div>
                  </div>
                </div>
              </div>
              <center><input type="submit" value="Registrar" class="btn btn-success"></center>
            </div>
          </form>                
        </div>
      </div>
    </div>
  </div>
  @include('layouts.dash.footer')

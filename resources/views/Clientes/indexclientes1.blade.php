@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
  <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
@slot('contenido')
 <br><br><br><br><br><br> 


<!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @include('alertas.notificacion') 
                    <div class="card">
                        <div class="header">
                            <h1>
                               Clientes
                            </h2>
                            <br>
                            <a class="btn btn-info" href="{{ url('clientes/crear') }}">Crear Cliente</a>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <?php $dos = $clientes->ToArray();if($dos["total"] == 0){ echo "<center><h3>No hay Clientes registrados</h3></center>";?>

                            <img src="{{ asset('images/adicionales/ZORRITO.jpg') }}" style="height:20em">
                            <?php }else{?>
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Nombre Cliente</th>
                                        <th>Nit</th>
                                        <th>Ciudad</th>
                                        <th>Direccion</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Estado</th>
                                        <th>Activar</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($clientes as $cliente)
                                    <tr>
                                        <td>{{ $cliente->nombre_cliente }}</td>
                                        <td>{{ $cliente->nit }}</td>
                                        <td>{{ $cliente->ciudad }}</td>
                                        <td>{{ $cliente->direccion }}</td>
                                        <td>{{ $cliente->telefono }}</td>
                                        <td>{{ $cliente->correo }}</td>
                                        <td>{{ $cliente->estado }}</td>
                                        @if($cliente->estado == "Activo")
                                        <td class="success">No Aplica</td>
                                        @else
                                        <td><a class="btn btn-success" href="{{ url('clientes/activar?id='.$cliente->id_cliente) }}">Activar</a></td>
                                        
                                        @endif
                                        <td><a class="btn btn-warning" href="{{ url('clientes/editar?id='.$cliente->id_cliente) }}">Editar</a></td>
                                        <td><a class="btn btn-danger" href="{{ url('clientes/eliminar?id='.$cliente->id_cliente) }}">Eliminar</a></td>
                                    </tr>
                                    @endforeach
          
                                </tbody>
                            </table>
                            <?php } ?>
                             {!! $clientes->render() !!} 
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
     

        @endslot
        <script type="text/javascript">
        $(function () {
    $('.js-basic-example').DataTable();

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
        </script>
        @endcomponent
@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
 
  <a href="{{ url('clientes/index') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 @endslot
@slot('contenido')
 <br><br><br><br><br><br> 
<div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('clientes/update') }}" method="POST">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <center><h1>Editar Cliente:</h1></center>
                                <center><h3>{{ $cliente[0]->nombre_cliente }}</h3></center>
                            
                        </div>
                              <div class="header col-lg-12">
                              	<div class="col-lg-7">
                            	<img src="{{ asset('images/Logos/logo.JPG') }}" >
                              	</div>
					     </div> <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                             <i class="material-icons"></i>
                                        </span>
                                        Nombre Cliente :
                                        <div class="form-line">
                                            <input type="text" class="form-control date"  value="{{ $cliente[0]->nombre_cliente }}" name="nombre">
                                             <input type="hidden" class="form-control date"  value="{{ $cliente[0]->id_cliente }}" name="id">
                                        </div>

                                    </div>
                                </div>                                
                                <div class="col-md-4">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Nit :
                                        <div class="form-line">
                                            <input type="text" value="{{ $cliente[0]->nit }}" class="form-control date" name="nit">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Ciudad:
                                        <div class="form-line">
                                            <input type="text" value="{{ $cliente[0]->ciudad }}" class="form-control date" name="ciudad" placeholder="Orden De Compra">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">


                            </div>


                            <div class="row clearfix ">
                                                                <div class="col-md-4">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>

                                        Direccion :
                                        <div class="form-line">
                                           <input type="text" class="form-control date" name="direccion" value="{{ $cliente[0]->direccion }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p>
                                        <b>Telefono :</b>
                                    </p>
                                    <div class="input-group input-group-s,">

                                        <div class="form-line">
											<input type="number" class="form-control date" name="telefono" value="{{ $cliente[0]->telefono }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p>
                                        <b>Correo :</b>
                                    </p>
                                    <div class="input-group input-group-s,">

                                        <div class="form-line">
											<input type="email" class="form-control date" name="correo" value="{{ $cliente[0]->correo }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div class="row clearfix">
                                <div class="col-md-12">
                                   
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
									<center><input type="submit" value="Editar Cliente" class="btn btn-success btn-lg" ></center>
                                    </div>
                                </div>
                                 
                             

                                                       </div>
                    </div>
                </form>
                </div>

@endslot
@endcomponent
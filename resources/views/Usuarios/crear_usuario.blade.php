@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>CREAR USUARIO </center></h1>
      </div>
      @include('alertas.notificacion')             
      <form action="{{ url('usuarios/postregistro') }}" method="POST" enctype="multipart/form-data">
        <div class="body"><br><br><br>
          <h2 class="card-inside-title"></h2>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row clearfix">
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Nombre Usuario</span>
                <div class="form-line">
                  <input type="text"  id="venci" class="form-control date" name="nombre">
                </div>
              </div>
            </div>                                
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Documento :</span>
                <div class="form-line">
                  <input type="number"  value="" id="venci" class="form-control date" name="doc">
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group">
                <span class="input-group-addon">Correo:</span>
                <div class="form-line">
                  <input type="text" class="form-control date" name="correo" placeholder="">
                </div>
              </div>
            </div>
            </div>
              <div class="row clearfix">
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Cargo :</span>
                    <div class="form-line">
                      <select class="form-control" name="cargo">
                        <option value="">Seleccione</option>
                        <option value="Financiero">Financiero</option>
                        <option value="Comercial">Comercial</option>
                        <option value="Diseño">Diseño</option>
                        <option value="Auxiliares">Auxiliares</option>
                        <option value="Logistica">Logistica</option>
                        <option value="R-Humanos">Recursos Humanos</option>
                        <option value="Administrador">Administrador</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Imagen de perfil :</span>
                    <div class="form-line">
                      <input type="file"  id="venci" class="form-control date" name="perfil">
                    </div>
                  </div>
                </div>                                
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">Contraseña :</span>
                    <div class="form-line">
                      <input type="password"  id="venci" class="form-control date" name="pass">
                    </div>
                  </div>
                </div>
              </div>
              <center><input type="submit" value="Registrar" class="btn btn-success"></center>
            </div>
          </form>                
        </div>
      </div>
    </div>
    @include('layouts.dash.footer')

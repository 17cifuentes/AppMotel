@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
        <div class="header">
            <h1><center>Perfil {{ Auth::user()->rol }}<br> {{ Auth::user()->name }}</center></h1>
        </div>
        @include('alertas.notificacion')
        <br><br><br>
        <form action="{{ url('usuarios/editar') }}" method="POST" enctype="multipart/form-data">
        <div class="col-lg-6">
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a href="#pablo"><br>
                            <img class="img img-rounded" src="{{ asset(Auth::user()->user_img) }}" width="400" height="290" class="img-reponsive"/>
                        </a>
                    </div>
                </div><br><h6 id="foto"></h6>
                <label class="btn btn-primary btn-file">Cambiar Foto <input type="file" style="display: none;" onchange="ver(this)" name="perfil">
                </label>
            </div>
        </div>
        <div class="card-content">
            <br>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Nombre Usuario</span>
                        <div class="form-line">
                            <input type="text" value="{{ Auth::user()->name }}" class="form-control" name="nombre">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Documento</span>
                        <div class="form-line">
                            <input type="text" value="{{ Auth::user()->documento }}" class="form-control" name="doc">
                        </div>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Correo</span>
                        <div class="form-line">
                            <input type="email" class="form-control" value="{{ Auth::user()->email }}" disabled>
                        </div>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Cargo</span>
                        <div class="form-line">
                            <input type="text" value="{{ Auth::user()->rol }}" class="form-control" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">ID Usuario</span>
                        <div class="form-line">
                            <input type="email" class="form-control" value="{{ 'Usu_'.Auth::user()->id }}" disabled>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Actualizar Perfil</button>
                </div>            
            </div>
        </div>
    </form>             
    </div>
</div>
</div>      
@include('layouts.dash.footer')

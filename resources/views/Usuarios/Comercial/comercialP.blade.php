@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')Comercial @endslot
 @slot('contenido')
      @slot('volver')
 <a href="{{ url('/') }}" class="btn btn-danger" class="bars">Pagina Principal<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
<div class="container-fluid">
   <br><br><br><br>
<br><br><br><br>
<center>

<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Cotización o inversion</h2></center>
                <p>
                    <a href="{{ url('cotizacion/cotizar') }}" class="btn btn-success btn-block">Cotizar</a> 
                    <a href="{{ url('cotizacion/listar') }}" class="btn btn-success btn-block">Opc.Cotizacion</a>
                    <a href="{{ url('inversion/listar') }}" class="btn btn-success btn-block">Opc.Inversion</a> 
                 </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Reportes</h2></center>
                <p>
                    <a href="{{ url('reportes/filtroscom') }}" class="btn btn-success btn-block">Generar Reportes</a>  
                 </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
                 <h2><span>Productos </span></h2>
                    <p>
                        <a href="{{ url('productos/crear') }}" class="btn btn-success btn-block">Crear</a> 
                        <a href="{{ url('productos/index') }}" class="btn btn-success btn-block">Listar</a> 
                     </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
                            <h2><span>Clientes</span></h2>
                            <p>
                                <a href="{{ url('clientes/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a href="{{ url('clientes/index') }}" class="btn btn-success btn-block">Listar</a> 
                            </p>
            </figcaption>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Responsable</span></h2>
                            <p>
                                <a href="{{ url('responsables/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a href="{{ url('responsables/index') }}" class="btn btn-success btn-block">Listar</a> 
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Reportes Graficos Comerciales</span></h2>
                            <p>
                                <a href="{{ url('#') }}" class="btn btn-success btn-block">Crear</a> 
                                
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
   </center>        
    

               
                    </div>
                </div>

 
     @endslot
   @endcomponent
@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')DISEÑO Y PUBLICIDAD @endslot
       @slot('volver')
 <a href="{{ url('home') }}" class="btn btn-danger" class="bars">volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
 @slot('contenido')
<br><br><br><br><br><br>  


<!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @include('alertas.notificacion') 
                    <div class="card">
                        <div class="header">
                            <center><h2>
                                Diseños Sagaz
                            </h2></center>
                            <h2><br>
                                <a class="btn btn-info" href="{{ url('diseno/cargar') }}">Nuevo Diseño</a>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                  

                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  >
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);"  data-toggle="modal" data-target="#myModal" >Nuevo Usuario</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Diseño</th>
                                        <th>Descripcion</th>
                                        <th>Cliente</th>
                                        <th>Descargar</th>
                                        <th>Ver Diseño</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($disenos as $diseno)
                                    <tr>
                                        <td>{{ $diseno->diseño_nombre }}</td>
                                        <td>{{ $diseno->diseño_des }}</td>
                                        <td>{{ $diseno->nombre_cliente }}</td>
                                        <td><a class="btn btn-success" download="diseno" href="{{ asset($diseno->diseño_img) }}">Descargar</a></td>
                                        <td><a class="btn btn-danger" target="_blank" href="{{ asset($diseno->diseño_img) }}">Ver</a></td>
                                        <td><a class="btn btn-danger" href="{{ url('diseno/eliminar?id='.$diseno->diseño_id) }}">Eliminar</a></td>
                                    </tr>
                                    @endforeach
          
                                </tbody>
                            </table>
                            {!! $disenos->render() !!}        
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form action="{{ url('usuarios/registrar') }}" method="POST">
               
                    <div >
                        <div class="header col-lg-12">
                                <div class="">
                                <center><img src="{{ asset('images/Logos/logo.JPG') }}" ></center>
                                </div><br>
                         </div>
                        <div class="header">
                            <h1>
                                <center>CREAR USUARIO   </center>
                            </h1>
                        </div>
                               <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                             <i class="material-icons">person</i>
                                        </span>
                                        Nombre Usuario :
                                        <div class="form-line">
                                            <input type="text" class="form-control date" id="fec_expedicion" name="nombre">
                                        </div>

                                    </div>
                                </div>                                
                                <div class="col-md-4">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        Contraseña :
                                        <div class="form-line">
                                            <input type="password" id="venci" class="form-control date" name="pass">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons">credit_card</i>
                                        </span>
                                        Correo:
                                        <div class="form-line">
                                            <input type="email" class="form-control date" name="correo" placeholder="Example@ejemplo.com">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">


                            </div>

                           <div class="row clearfix">
                                <div class="col-md-12">
                                   
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                    <center><input type="submit" value="Registrar" class="btn btn-success btn-lg" ></center>
                                    </div>
                                </div>
                                 
                             

                                                       </div>
                    
                </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

        @endslot
        <script type="text/javascript">
        $(function () {
    $('.js-basic-example').DataTable();

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
        </script>
        @endcomponent
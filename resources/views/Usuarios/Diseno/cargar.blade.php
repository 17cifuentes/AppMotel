@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')GERENTE @endslot
       @slot('volver')
 
  <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 @endslot
 @slot('contenido')
<br><br><br><br><br><br>  
@include('alertas.notificacion')                      
  <form action="{{ url('diseno/postcargar') }}" method="POST" enctype="multipart/form-data">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1>
                                <center>CARGAR DISEÑO</center>
                            </h1>
                        </div>
                              <div class="header col-lg-12">
                              	<div class="col-lg-7">
                            	<img src="{{ asset('images/Logos/logo.JPG') }}" >
                              	</div>
					     </div> <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                             <i class="material-icons"></i>
                                        </span>
                                        Nombre de Cliente :
                                        <div class="form-line">
                                            <select class="form-control" name="cliente">
                                                <option value="">Seleccione</option>
                                                @foreach($clientes as $cliente ) 
                                                    <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre_cliente }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>                                
                                <div class="col-md-4">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Nombre diseño :
                                        <div class="form-line">
                                            <input type="text"  value="" id="nombre" class="form-control date" name="nombre">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Descripcion del diseño:
                                        <div class="form-line">
                                           <textarea name="descripcion" class="form-control"></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                             <i class="material-icons"></i>
                                        </span>
                                        Diseño :
                                        <div class="form-line"><br>
                                            <label style="font-size:20px" id="venci" class="btn btn-primary btn-block btn-file">
    Subir Diseño <input type="file" style="display: none;" onchange="ver(this)" name="diseno" required>

</label>
                                        </div>

                                    </div>
                                </div>                                
                            </div>


                            <div class="col-lg-12">


                            </div>


                           <div class="row clearfix">
                                <div class="col-md-12">
                                   
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
									<center><input type="submit" value="Registrar" class="btn btn-success btn-lg" ></center>
                                    </div>
                                </div>
                              </div>
                    </div>
                </form>
                </div>

@endslot
@endcomponent
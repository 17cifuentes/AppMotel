@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>LISTA USUARIO </center></h1>
      </div>
      @include('alertas.notificacion')
      <div class="body table-responsive">
        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Documento</th>
              <th>Cargo</th>
              <th>Correo</th>
              <th>Estado</th>
              <th>Activar</th>
              <th>Desactivo</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
          @foreach($usuarios as $usuario)
            <tr>
              <td>{{ $usuario->id }}</td>
              <td>{{ $usuario->name }}</td>
              <td>{{ $usuario->documento }}</td>
              <td>{{ $usuario->rol }}</td>
              <td>{{ $usuario->email }}</td>
              <td>{{ $usuario->user_estado }}</td>
              <td><a class="btn btn-success" href="{{ url('usuarios/cambiarestado?usuario='.$usuario->id.'&accion=Activo') }}">Activar</a></td>
              <td><a class="btn btn-warning" href="{{ url('usuarios/cambiarestado?usuario='.$usuario->id.'&accion=Desactivo') }}">Desactivar</a></td>
              <td><a class="btn btn-danger" href="{{ url('usuarios/eliminar?usuario='.$usuario->id) }}">Eliminar</a></td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {!! $usuarios->render() !!}        
      </div>
    </div>
  </div>
</div>
@include('layouts.dash.footer')

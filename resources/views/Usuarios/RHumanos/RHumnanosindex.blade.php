@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')Recursos Humanos @endslot
     @slot('volver')
 <a href="{{ url('/') }}" class="btn btn-danger" class="bars">Pagina Principal<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
 @slot('contenido')

<div class="container-fluid " style="margin-left: 8em">
   <br><br><br><br>
<br><br><br><br>
<center>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/almu.png')}}" alt=""/>
            <figcaption>
               <center> <h2> Almuerzos</h2></center>
                <p>
                    <a href="{{ url('almuerzos/reportar') }}" class="btn btn-success btn-block">Elegir Almuerzo</a> 
                    <a href="{{ url('reportes/almuerzos') }}" class="btn btn-success btn-block">Reportes de Almuerzos</a> 
                </p>  
            </figcaption>
        </figure>
    </li>

</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/gastos.png')}}" alt=""/>
            <figcaption>
               <center> <h2> Otros Gastos</h2></center>
                <p>
                    <a href="{{ url('humanos/registrogasto') }}" class="btn btn-success btn-block">Registro Nuevo Gasto</a> 
                    <a href="{{ url('reportes/almuerzos') }}" class="btn btn-success btn-block">Reportes de otros gastos</a> 
                </p>  
            </figcaption>
        </figure>
    </li>

</ul>




   </center>        
    


               
                    </div>
                </div>

 
     @endslot
   @endcomponent
@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
  <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
@slot('contenido')
<div class="container-fluid">
   <br><br><br><br><br><br> 
<div class="row clearfix">    
@include('alertas.notificacion')                      
<form action="{{ url('almuerzos/elegir') }}" method="POST" enctype="multipart/form-data">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1>
                                <center>Registro de nuevo gasto</center>
                            </h1>
                        </div>
                              <div class="header col-lg-12">
                                <div class="col-lg-12">
                                <img src="{{ asset('images/Logos/logo.JPG') }}" >
                                </div>
                         </div> <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row clearfix">
                                     <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Cantidad Item:
                                        <div class="form-line">
                                            <input type="number"  value="" id="venci" class="form-control date" name="Cantidad">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Fecha:
                                        <div class="form-line">
                                            <input type="date"  value="" id="venci" class="form-control date" name="Fecha">
                                        </div>

                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Valor del gasto:
                                        <div class="form-line">
                                            <input type="text"  value="" id="venci" class="form-control date" name="Valor">
                                        </div>
                                    </div>
                                </div>  
                                 <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Tipo de gasto:
                                        <div class="form-line">
                                            <input type="text"  value="" id="venci" class="form-control date" name="Valor">
                                        </div>
                                    </div>
                                </div>                                   
                                <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Responsable:
                                        <div class="form-line">
                                            <input type="text"  value="" id="venci" class="form-control date" name="Responsable">
                                        </div>

                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Observacion:
                                        <div class="form-line">
                                            <input type="text"  value="" id="venci" class="form-control date" name="Opservacion">
                                        </div>

                                    </div>
                                </div>                                                                                            

                                <div class="col-md-2">
                                    <div class="input-group">
                                      
                                            <input type="submit"  value="Comfirmar Gasto" class="btn btn-success">
                                      


                                    </div>
                                </div>


                            </div>


        

@endslot
@endcomponent
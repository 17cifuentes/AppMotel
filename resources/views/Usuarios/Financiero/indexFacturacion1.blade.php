@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')
 <a href="{{ url('home') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
@slot('contenido')
<div class="container-fluid" >
   <br><br><br><br><br><br> 
<div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('financiero/crear') }}" method="POST" target="" id="facid">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1>
                                <center>FACTURACION SAGAZ SAS	</center>
                            </h1>
                        </div>
                              <div class="header col-lg-12">
                              	<div class="col-lg-7">
                            	<img src="{{ asset('images/Logos/logo.JPG') }}" >
                              	</div>
                              	<div class="col-lg-4">
                           <h2 style="margin-top:2.5em">Numero de factura : SAG-{{ $id_factura+1}} <input name="fac_id" type="hidden" value="{{ $id_factura}}"></h2>      
                              	</div>                </div> <br><hr>
       <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">
                            <div class="col-md-3">
                                 
                                     <b>   Forma de Pago</b>
                                 
                                    <div class="input-group input-group-sm">

                                        <div class="form-line">
                                           <select  class="form-control" name="forma_pago" onchange="formapago(this.value)" required="1">
                                                <option value="">-- selecione --</option>
                                                <option value="Contado"> Contado </option>
                                                <option value="Credito"> Credito </option>
                                            </select>
                                        </div>
                                        <div id="forma"></div>
                                    </div>
                                </div>
                                <div class="col-md-3 ">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <b>Fecha de Expedicón:</b>
                                        <div class="form-line">
                                            <input type="date" class="form-control datetime" id="fec_expedicion" name="fec_expedicion">
                                             <input type="hidden"  id="guia" value="0" >
                                        </div>

                                    </div>
                                </div>        
                                                        
                                <div class="col-md-3">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                       <b> Fecha de Vencimiento :</b>
                                        <div class="form-line">
                                            <input type="date" id="venci" class="form-control datetime" name="fec_vencimiento">
                                        </div>

                                    </div>
                                </div>
                                       <div class="col-md-3">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>

                                        <b>Cliente :</b>
                                            <div class="form-line">
                                                <select class="form-control" name="cliente" onchange="dararea(this.value)">
                                                    <option><b>-- selecione --</b></option>
                                                @foreach($clientes as $cliente)
                                                    <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre_cliente }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix ">
                                <div class="col-md-3">
                                        <b>Area :</b>
                                    <div class="input-group input-group-lg">
                                        <div class="form-line">
                                           <select class="form-control" onchange="darresponable(this.value)" name="area" id="area">
                                                <option>-- selecione --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                        <b>Responsable :</b>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                           <select class="form-control" name="responsable" id="responsable">
                                                <option>-- selecione --</option>
                                            @foreach($responsables as $responsable)
                                                <option value="{{ $responsable->id_responsable }}">{{ $responsable->contacto }}</option>
                                            @endforeach                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <b>Orden de Compra :</b>
                                        <div class="form-line">
                                            <input type="text" class="form-control datetime" name="orden" placeholder="Orden De Compra">

                                        </div>

                                    </div>
                                </div>

                            </div>
                           <div class="row clearfix">
                                <div class="col-md-4">

                                    <p>
                                    
                                        <b>Producto: </b>
                                    
                                    </p>
                                    
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                           <select class="form-control" id="nombrep"  onchange="nompro(this.value)">
                                                <option>-- selecione --</option>
                                                @foreach($productos as $producto )
                                                    <option value="{{ $producto->id_producto }}">{{ $producto->nombre_producto }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                 
                                 <div class="col-md-4">
                                    <p>
                                        <b>Descripcion :</b>
                                    </p>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                           <select onchange="valorpro(this.value)" class="form-control" id="pro" ><option >Elija Opcion</option></select>
                                        </div>
                                
                                    </div>
                                </div>
                        
                                <div class="col-md-3">
                                    <p>
                                        <b>Valor :</b>
                                    </p>
                                    <div class="input-group input-group-sm">

                                        <div class="form-line">
                                            <input value="0" id="valorproducto"  disabled="1" type="text" class="form-control">
                                        </div>

                                    </div>
                                </div>
                                                                <div class="col-md-1">
                                    
                                    <div class="input-group input-group-sm">

                                        <br>
                                            <a href="#" onclick="editcategoria()" data-toggle="modal" data-target="#editcategoria"  class="btn btn-lg btn-warning">Editar</a>
                                       

                                    </div>
                                </div>
                            </div> 
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Cantidad :</b>
                                    </p>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                            <input type="number" value="1" id="cantproducto" class="form-control" placeholder="Cantidad">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p>
                                        <b>Descuento : </b>
                                    </p>
                                    <div class="input-group">
                                        <div class="form-line">
                                          <input type="number" value="0" id="descuento"  class="form-control" placeholder="Descuento">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                                   
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon ">
                           <a onclick="addproducto()"> <i class="material-icons">+</i></a>
                        </div>
                        <div class="content">
                            <h4 style="margin-top:1.5em">Agregar a Factura</h4>
                            
                        </div>
                    </div>
               </div>
           </div>
     </div>
    </div>
                </div>
                <div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table style="color:black" cellpadding="1" cellspacing="1" class="table table-bordered table-striped panel">
                <thead>
                <tr>                  
                    <th>Quitar Producto</th>
                    <th>Producto</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                    <th>Valor</th>
                    <th>Descuento %</th>
                    <th>Vlr. Descuento</th>
                    <th>Total Neto</th>

                </tr>
                </thead>
                <tbody id="items" class="panel" >
                </tbody>
            </table>
        </div>    
    </div>
</div>

<hr>


<div class="row m-t">
    <div class="col-md-4 col-md-offset-8">
        <table class="table  text-right panelx panel" style="color:black">
            <tbody id="calculos">
            <tr>
                <td><strong>Total Bruto :</strong></td>
               <input type="hidden" value="0" name="totalbruto" id="bruto">
                <td id="totalBruto">$0</td>
            </tr>
            <tr>
                <td><strong>Total Descuento :</strong></td>
                <input type="hidden" value="0" name="totaldescu" id="descuentot">
                <td id="totalDto">$0</td>
            </tr>
            <tr>
                <td><strong>IVA :</strong></td>
                 <input type="hidden" name="totaliva" value="0" id="iva">
                <td id="totalIva">$0</td>
            </tr>
            <tr>
                <td><strong>Retefuente :</strong></td>
                <td>$0</td>
            </tr>
            <tr>
                <td><strong>TOTAL :</strong></td>
                <input type="hidden" value="0" name="totalfact" id="totalfactura">
                <td id="totalFinal">$0</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<hr>
<center>                
                    <div class="col-lg-3">
                        <button class="btn btn-success btn-block"  style="height:5em" type="submit" onclick="ver(this.value)" value="2" name="opc">Facturar</button>
</div>
     <div class="col-lg-3">
<div>
</div>



                </center>
<br>
            </div>
            </form>

  <div id="editcategoria" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <center><button type="button" class="close" data-dismiss="modal">&times;</button>
        <img src="{{ asset('images/Logos/logo.JPG') }}" >
        <h4 class="modal-title">Crear Producto</h4></center>
      </div>
      <div class="modal-body"><br><br>
        <form action="#" method="POST">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">widgets</i>
                    </span>
                    Descripcion:
                   <div class="form-line">
                         <input type="text" value="" id="edit_desc" class="form-control datetime" name="nombre_pro">
                   </div>
                </div>
            </div>
                        <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">widgets</i>
                    </span>
                    Valor:
                   <div class="form-line">
                         <input type="text"  value="" id="edit_valor" class="form-control datetime" name="nombre_pro">
                   </div>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="input-group input-group-lg">
                    <br><center><input type="button" value="Editar" onclick="posteditar()" class="btn btn-success btn-lg" ></center>
                                    
                 </div>
            </div>
        </form>
        <br><br><br><br><br><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>


@endslot




@endcomponent

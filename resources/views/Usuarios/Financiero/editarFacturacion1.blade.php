@component('layouts.admin')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
  @slot('cargo')
 Financiero 
 @endslot
   @slot('volver')

  <a href="{{ url('financiero/listar') }}" class="btn btn-danger" class="bars">Volver<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 @endslot
@slot('contenido')
 <br><br><br><br><br><br> 

<div class="row clearfix">    
@include('alertas.notificacion')                      
  <form action="{{ url('financiero/posteditar') }}" method="POST" target="" id="facid">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1>
                                <center>EDITAR FACTURA SAGAZ SAS	</center>
                            </h1>
                        </div>
                              <div class="header col-lg-12">
                              	<div class="col-lg-7">
                            	<img src="{{ asset('images/Logos/logo.JPG') }}" >
                              	</div>
                              	<div class="col-lg-4">
                           <h2 style="margin-top:2.5em">Numero de factura : SAG-{{  $facturas[0]->id_factura}} <input name="fac_id" type="hidden" value="{{ $facturas[0]->id_factura}}"></h2>      
                              	</div>


                            
                        </div> <br><hr>

                        <div class="body"><br><br><br>
                            <h2 class="card-inside-title"></h2>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <p>
                                        <b>Forma de Pago {{  $facturas[0]->forma_pago }}@if(isset($facturas[0]->plazo))
                                            {{ "(".$facturas[0]->plazo." dias)" }}@endif cambiar selecione:</b>
                                    </p>
                                    <div class="input-group input-group-sm">

                                        <div class="form-line">
                                           <select  class="form-control" name="forma_pago" onchange="formapago(this.value)">
                                                <option value="">-- selecione --</option>
                                                <option value="Contado"> Contado </option>
                                                <option value="Credito"> Credito </option>
                                            </select>
                                            <input type="hidden" name="formafijo" value="{{  $facturas[0]->forma_pago }}" >
                                        </div>
                                        <div id="forma"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Fecha de Expedicón :
                                        <div class="form-line">
                                            <input type="date" class="form-control date" value="{{  $facturas[0]->fec_expedicion }}" id="fec_expedicion" name="fec_expedicion">
                                        </div>

                                    </div>
                                </div>                                
                                <div class="col-md-4">
                                    <div class="input-group">
                                         <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        Fecha de Vencimiento :
                                        <div class="form-line">
                                            <input type="date" id="venci" value="{{  $facturas[0]->fec_vencimiento }}" class="form-control date" name="fec_vencimiento">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <h2 class="card-inside-title">Datos del cliente</h2>
                            <div class="col-lg-12">
                            </div>


                            <div class="row clearfix ">
                                      <center><div class="col-md-4">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                    <p>
                                        <b>Cliente {{  $facturas[0]->nombre_cliente }} cambiar selecione:</b>
                                    </p>
                                        <b>Cliente :</b>
                                       <input name="clientefijo" type="hidden" value="{{  $facturas[0]->id_cliente }}">
                                            <div class="form-line">
                                                <select class="form-control" name="cliente" onchange="dararea(this.value)">
                                                    <option value=""><b>-- selecione --</b></option>
                                                @foreach($clientes as $cliente)
                                                    <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre_cliente }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                    </div>
                                </div></center>                             
                            </div>
                            <div class="row clearfix ">
                                <div class="col-md-4">
                                    <p>
                                        <b>Area : {{  $facturas[0]->area_des }} cambiar selecione:</b>
                                    </p>                                    
                                        
                                    <div class="input-group input-group-lg">
                                        <div class="form-line">
                                           <select class="form-control" onchange="darresponable(this.value)" name="area" id="area">
                                                <option value="">-- selecione --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input name="responsablefijo" type="hidden" value="{{  $facturas[0]->id_responsable }}">
                                    <p>
                                        <b>Responsables : {{  $facturas[0]->contacto }} cambiar selecione:</b>
                                    </p>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                           <select class="form-control" name="responsable" id="responsable">
                                                <option value="">-- selecione --</option>
                                            @foreach($responsables as $responsable)
                                                <option value="{{ $responsable->id_responsable }}">{{ $responsable->contacto }}</option>
                                            @endforeach                                                
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                           <div class="col-md-4">
                                    <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="material-icons"> </i>
                                        </span>
                                        Orden de Compra :
                                        <div class="form-line">
                                            <input type="text" class="form-control date" name="orden" placeholder="Orden De Compra" value="{{  $facturas[0]->orden}}">
                                        </div>

                                    </div>
                                </div>
                            </div>
                           <div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Producto :</b>
                                    </p>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                           <select class="form-control" id="nombrep"  onchange="nompro(this.value)">
                                                <option>-- selecione --</option>
                                                @foreach($productos as $producto )
                                                    <option value="{{ $producto->id_producto }}">{{ $producto->nombre_producto }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                 
                                 <div class="col-md-4">
                                    <p>
                                        <b>Categoria :</b>
                                    </p>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                           <select onchange="valorpro(this.value)" class="form-control" id="pro" ><option >Elija Opcion</option></select>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-md-4">
                                    <p>
                                        <b>Valor :</b>
                                    </p>
                                    <div class="input-group input-group-sm">

                                        <div class="form-line">
                                            <input value="0" id="valorproducto"  disabled="1" type="text" class="form-control">
                                            <input type="hidden"  id="guia" value="<?php echo COUNT($facturas) ?>" >
                                        </div>

                                    </div>
                                </div>
                            </div> 
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <p>
                                        <b>Cantidad :</b>
                                    </p>
                                    <div class="input-group input-group-lg">

                                        <div class="form-line">
                                            <input type="number" value="1" id="cantproducto" class="form-control" placeholder="Cantidad">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p>
                                        <b>Descuento : </b>
                                    </p>
                                    <div class="input-group">
                                        <div class="form-line">
                                          <input type="number" value="0" id="descuento"  class="form-control" placeholder="Descuento">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                                   
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon ">
                           <a onclick="addproducto()"> <i class="material-icons">+</i></a>
                        </div>
                        <div class="content">
                            <h4 style="margin-top:1.5em">Agregar a Factura</h4>
                            
                        </div>
                    </div>
               </div>
                            </div>
                  
                        </div>
                    </div>
                
                <div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table cellpadding="1" cellspacing="1" class="table table-bordered panel" style="color:black">
                <thead>
                <tr>                  
                    <th>Quitar Producto</th>
                    <th>Producto</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                    <th>Valor</th>
                    <th>Descuento %</th>
                    <th>Vlr. Descuento</th>
                    <th>Total Neto</th>

                </tr>
                </thead><?php  //dd($facturas) ?>
                <tbody id="items">
                    @for($x=0;$x < COUNT($facturas);$x++)
                    <?php $y = $x + 1 ?>
                        <tr id="<?php echo 'fila'.$y ?>">
        <td><a onclick="quitar({{$x+1}})"  class="btn btn-danger">Quitar Producto</a></td>
        <td>{{$facturas[$x]->nombre_producto}}</td>
        <td>{{$facturas[$x]->cat_descripcion}}</td>
        <td>{{$facturas[$x]->cantidad}}</td>
        <td>$ {{ number_format($facturas[0]->cat_precio_cliente)}}</td>
        <td>{{$facturas[$x]->descuento}}%</td>
        <td>${{number_format($facturas[0]->valor_des)}}</td>
        <td><input type="hidden" value="{{$facturas[0]->neto}}" id="neto" name="neto[]">$ {{number_format($facturas[$x]->neto)}}</td>        
        <input type="hidden" name="producto[]" value="{{$facturas[$x]->id_producto}}">

        <input type="hidden"  id="<?php echo 'cate'.$y ?>"  name="categoriapro[]" value="{{$facturas[$x]->id_cat}}">
        <input type="hidden"  id="<?php echo 'canti'.$y ?>" name="cantidadpro[]" value="{{$facturas[$x]->cantidad}}">
        <input type="hidden"  id="<?php echo 'precio'.$y ?>" name="precio_cliente[]" value="{{$facturas[$x]->cat_precio_cliente}}">
        <input type="hidden"  id="<?php echo 'desc'.$y ?>" name="descuentopro[]" value="{{$facturas[$x]->descuento}}">
        <input type="hidden"  id="<?php echo 'des_valor'.$y ?>" name="desvalorpro[]" value="{{$facturas[$x]->valor_des}}">
        <input type="hidden"  id="<?php echo 'netpro'.$y ?>" name="netopro[]" value="{{$facturas[$x]->neto}}">
         </tr>
                    @endfor
                </tbody>
            </table>
        </div>    
    </div>
</div>

<hr>


<div class="row m-t">
    <div class="col-md-4 col-md-offset-8">
        <table class="table  text-right panel" style="color:black">
            <tbody id="calculos">
            <tr>
                <td><strong>Total Bruto :</strong></td>
               <input type="hidden" value="{{$facturas[0]->bruto }}" name="totalbruto" id="bruto">
                <td id="totalBruto">${{number_format( $facturas[0]->bruto) }}</td>
            </tr>
            <tr>
                <td><strong>Total Descuento :</strong></td>
                <input type="hidden" value="{{ $facturas[0]->fac_descuento }}" name="descuentot" id="descuentot">
                <td id="totalDto">${{number_format ($facturas[0]->fac_descuento) }}</td>
            </tr>
            <tr>
                <td><strong>IVA :</strong></td>
                 <input type="hidden" name="totaliva" value="{{ $facturas[0]->iva }}" id="totaliva">
                <td id="totalIva">${{number_format($facturas[0]->iva) }}</td>
            </tr>
            <tr>
                <td><strong>Retefuente :</strong></td>
                <td>$0</td>
            </tr>
            <tr>
                <td><strong>TOTAL :</strong></td>
                <input type="hidden" value="{{ $facturas[0]->total }}"name="totalfact" id="totalfactura">
                <td id="totalFinal">${{number_format($facturas[0]->total) }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<hr>
<center>                
<div class="col-lg-3">
<button type="submit" onclick="ver(this.value)" class="btn btn-success btn-block"  style="height:5em" value="2" name="opc">Guardar Cambios</button>
                </div>
           </center>
        <br>
    </div>
</form>
@endslot




@endcomponent

@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>FACTURACION ORGANIZACIÓN SAGAZ S.A.S </center></h1>
      </div>
        <!-- inicio pagina-->
      <div class="container-fluid" >
        <div class="row clearfix">    
          @include('alertas.notificacion')                      
          <form action="{{ url('financiero/crear') }}" method="POST" target="" id="facid">
          <br><br>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                <div class="header col-lg-12">
                  <div class="col-lg-7">
                    <img src="{{ asset('images/Logos/logo.JPG') }}" >
                  </div>
                  <div class="col-lg-4">
                    <h2 style="margin-top:1.5em;font-size: 190%">Numero de factura : SAG-{{ $id_factura+1}} <input name="fac_id" type="hidden" value="{{ $id_factura}}"></h2>      
                  </div>                
                </div><br><br><hr>
                <div class="body"><br><br><br>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="row clearfix">
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Forma de Pago</span>
                        <div class="form-line">
                          <select  class="form-control" name="forma_pago" onchange="formapago(this.value)" required="1">
                            <option value="">-- selecione --</option>
                            <option value="Contado"> Contado </option>
                            <option value="Credito"> Credito </option>
                          </select>
                          <div id="forma"></div>
                        </div>
                      </div>
                    </div>
                                        
                    
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Fecha de Expedicón:</span>
                        <div class="form-line">
                          <input type="date" class="form-control datetime" id="fec_expedicion" name="fec_expedicion">
                          <input type="hidden"  id="guia" value="0" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Fecha de Vencimiento :</span>
                        <div class="form-line">
                          <input type="date" id="venci" class="form-control datetime" name="fec_vencimiento">
                        </div>
                      </div>
                    </div>      
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Cliente :</span>
                        <div class="form-line">
                          <select class="form-control" name="cliente" onchange="dararea(this.value)">
                            <option><b>-- selecione --</b></option>
                            @foreach($clientes as $cliente)
                              <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre_cliente }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Area :</span>
                        <div class="form-line">
                          <select class="form-control" onchange="darresponable(this.value)" name="area" id="area">
                            <option>-- selecione --</option>
                          </select>
                        </div>
                      </div>
                    </div>                    
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Responsable :</span>
                        <div class="form-line">
                          <select class="form-control" name="responsable" id="responsable">
                            <option>-- selecione --</option>
                            @foreach($responsables as $responsable)
                              <option value="{{ $responsable->id_responsable }}">{{ $responsable->contacto }}</option>
                            @endforeach                                                
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Orden de Compra :</span>
                        <div class="form-line">
                          <input type="text" class="form-control datetime" name="orden" placeholder="Orden De Compra">
                        </div>
                      </div>
                    </div>       
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Producto :</span>
                        <div class="form-line">
                          <select class="form-control" id="nombrep"  onchange="nompro(this.value)">
                            <option>-- selecione --</option>
                            @foreach($productos as $producto )
                              <option value="{{ $producto->id_producto }}">{{ $producto->nombre_producto }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Descripcion :</span>
                        <div class="form-line">
                          <select onchange="valorpro(this.value)" class="form-control" id="pro" >
                            <option >Elija Opcion</option>
                          </select>
                        </div>
                      </div>
                    </div>                    
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Valor :</span>
                        <div class="form-line">
                          <input value="0" id="valorproducto"  disabled="1" type="text" class="form-control">
                        </div>
                        <span class="input-group-addon btn-warning" style="background-color: orange;"><a href="#" style="color: white" onclick="editcategoria()" data-toggle="modal" data-target="#editcategoria" class="btn-xs btn-block">Editar</a></span>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Cantidad :</span>
                        <div class="form-line">
                          <input type="number" value="1" id="cantproducto" class="form-control" placeholder="Cantidad">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon">Descuento :</span>
                        <div class="form-line">
                          <input type="number" value="0" id="descuento"  class="form-control" placeholder="Descuento">
                        </div>
                      </div>
                    </div>
                    <div class="icon col-lg-3">
                      <a onclick="addproducto()" class="btn-block btn btn-success "> <i class="material-icons">+ Agregar </i></a><br><br>
                    </div>
                  </div>
                </div>
              </div>
            
            <div class="row">
              <div class="table-responsive col-lg-12">
                <table style="color:black" cellpadding="1" cellspacing="1" class="table table-bordered table-striped panel">
                  <thead>
                    <tr>                  
                      <th>Quitar Producto</th>
                      <th>Producto</th>
                      <th>Descripcion</th>
                      <th>Cantidad</th>
                      <th>Valor</th>
                      <th>Descuento %</th>
                      <th>Vlr. Descuento</th>
                      <th>Total Neto</th>
                    </tr>
                  </thead>
                  <tbody id="items" class="panel" >
                  </tbody>
                </table>
              </div>    
            </div>
          </div>
          <hr>
          <div class="row m-t">
            <div class="col-md-4 col-md-offset-8">
              <table class="table  text-right panelx panel" style="color:black">
                <tbody id="calculos">
                  <tr>
                    <td><strong>Total Bruto :</strong></td>
                    <input type="hidden" value="0" name="totalbruto" id="bruto">
                    <td id="totalBruto">$0</td>
                  </tr>
                  <tr>
                    <td><strong>Total Descuento :</strong></td>
                    <input type="hidden" value="0" name="totaldescu" id="descuentot">
                    <td id="totalDto">$0</td>
                  </tr>
                  <tr>
                    <td><strong>IVA :</strong></td>
                    <input type="hidden" name="totaliva" value="0" id="iva">
                    <td id="totalIva">$0</td>
                  </tr>
                  <tr>
                    <td><strong>Retefuente :</strong></td>
                    <td>$0</td>
                  </tr>
                  <tr>
                    <td><strong>TOTAL :</strong></td>
                    <input type="hidden" value="0" name="totalfact" id="totalfactura">
                    <td id="totalFinal">$0</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <hr>
                       
            <div class="col-lg-3">
              <button class="btn btn-success btn-block"  style="height:5em" type="submit" onclick="ver(this.value)" value="2" name="opc">Facturar</button>
            </div>
        </form>

          
        
        <div id="editcategoria" class="modal fade" role="dialog">
          <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="background: orange">
                <center><button type="button" class="close" data-dismiss="modal">&times;</button>
                <img src="{{ asset('images/Logos/logo.JPG') }}" >
                <h4 class="modal-title" style="color: white">Crear Producto</h4></center>
              </div>
              <div class="modal-body"><br><br>
                <form action="#" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="material-icons">Descripcion:</i>
                        </span>
                        <div class="form-line">
                          <input type="text" value="" id="edit_desc" class="form-control datetime" name="nombre_pro">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="material-icons">Valor:</i>
                        </span>
                        
                        <div class="form-line">
                           <input type="text"  value="" id="edit_valor" class="form-control datetime" name="nombre_pro">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group input-group-lg">
                        <br><center><input type="button" value="Editar" onclick="posteditar()" class="btn btn-success btn-lg" ></center>
                      </div>
                    </div>
                  </form><br><br><br><br><br><br>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
            </div>
          </div>
        <!-- Fin pagina -->       
        </div>
      </div>
</div>
@include('layouts.dash.footer')
<script>
      function nompro(id){
        $.ajax({
            url : "financiero/vercategorias",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#pro").html(datos);

            }
        })


}

    function valorpro(id_pro){
        
        $.ajax({
            url : "financiero/valorpro",
            type : "get",
            data : "id="+id_pro,
            success:function(datos){
             
             $("#valorproducto").attr('value',datos);

            }
        })


}
   function quitar(id){
    
    var indice = id - 1;
    var cate = $("#cate"+id).val();
    var canti = $("#cate"+id).val();
    var precio = $("#precio"+id).val();
    var desc = $("#desc"+id).val();
    var des_valor = $("#des_valor"+id).val();
    var netpro = $("#netpro"+id).val();

    var totalbruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var totaliva = $("#totaliva"+id).val();
    var desc = $("#totalfactura"+id).val();
    
    var a = totalbruto - netpro;
    var b = descuentot - des_valor;
    var iva = a * 0.19;
    var c = a + iva;
    
            $.ajax({
            url : "financiero/quitarproducto",
            type : "get",
            data : "a="+a+"&b="+b+"&c="+c+"&iva="+iva,
            success:function(datos){
             
             $("#calculos").html(datos);
             $("#fila"+id).remove();
            }
        })

}
var conpro = 0;
function addproducto(){

   var producto = $("#nombrep").val();
    var procategoria = $("#pro").val();
    var cantpro = $("#cantproducto").val();
     conpro = conpro + 1; 
    var descuento = $("#descuento").val();
    var bruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var ivacal = $("#iva").val();
    var totalneto = $("#totalneto").val();
    var valor = $("#valorproducto").val();


      $.ajax({
            url : "financiero/addproducto",
            type : "get",
            data : "producto="+producto+"&conpro="+conpro+"&categoria="+procategoria+"&cantidad="+cantpro+"&descuento="+descuento+"&bruto="+bruto+"&totalneto="+totalneto,
            success:function(datos){
             
             $("#items").append(datos);
              var descuento = $("#descuento").val();
              var procategoria = $("#pro").val();
              var neto = $("#neto").val();
              var descuentot = $("#descuentot").val();
              var cantpro = $("#cantproducto").val();

               totales(descuento,procategoria,bruto,descuentot,ivacal,neto,cantpro);

            }
        })
  
}
function totales(descuento,procategoria,bruto,descuentot,ivacal,neto,cantpro){
            $.ajax({
            url : "financiero/totales",
            type : "get",
            data : "cantpro="+cantpro+"&netoca="+neto+"&descuento="+descuento+"&categoria="+procategoria+"&bruto="+bruto+"&descuentot="+descuentot+"&ivacal="+ivacal,
            success:function(datos){
             $("#calculos").html(datos);
             
            }
        })
}
   function darresponable(id_cliente){

        $.ajax({
            url : "financiero/darresponsable",
            type : "get",
            data : "area="+id_cliente,
            success:function(datos){
             
             $("#responsable").html(datos);

            }
        })


}
    function dararea(id_cliente){
        $.ajax({
            url : "financiero/dararea",
            type : "get",
            data : "id="+id_cliente,
            success:function(datos){
             
             $("#area").html(datos);

            }
        })


}
    function posteditar(){
            var edit_desc=$("#edit_desc").val();
            var edit_valor=$("#edit_valor").val();
            var id = $("#pro").val();
            
        $.ajax({
            url : "financiero/editpro",
            type : "get",
            data : "id="+id+"&edit_valor="+edit_valor+"&edit_desc="+edit_desc,
            success:function(){
            var nombrep = $("#nombrep").val(); 
             nompro(nombrep);
             $("#editcategoria").modal("hide");


            }
        })


}
function formapago(forma){
 if(forma == "Credito"){
        $("#forma").html("<label>Plazo(dias) :</label><select class='form-control' name='plazo' onchange='darfecha(this.value)'><option>Elija Opcion</option><option>30</option><option>60</option><option>90</option></select");

    }else{
         $("#forma").html("");
    }
        
}

</script>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PDF</title>

    <style>

        .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #E84E0F;
  text-decoration: none;
}

body {
  position: relative;
  width: 18cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: SourceSansPro;
}

header {
  width: 100%;
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

/*#logo img {
  height: 70px;
}*/

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #E84E0F;
  float: left;
}

#client .name{
  text-transform: none;
}


#invoice {
  float: right;
  text-align: right;
}

#client .to {
  color: #777777;
}

h2.name {
  color: #E84E0F;
  font-size: 1.4em;
  font-weight: bold;
  margin: 0;
}

h2.client {
  color: #173e43;
  font-size: 1.0em;
  font-weight: bold;
  margin: 0;
  width:250px;
}



#invoice h3 {
  color: #E84E0F;
  font-size: 1.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 2px;
}

table th,
table td {
  padding: 2px;
  background: #EEEEEE;
  text-align: center;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: right;
}

table td h3{
  color: #E84E0F;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  /*color: #FFFFFF;*/
  font-size: 1.6em;
  background: #DDDDDD;
  text-align: center;
}

table .desc1 {
  margin-top: 100em;
  text-align: left;
  background: #DDDDDD;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
  text-align: center;
}

table .qty {
  text-align: center;
}

table .total {
  background: #DDDDDD;
  text-align: center;
  /*color: #FFFFFF;*/
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 10px 20px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1.2em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot td.totalfact {
  text-align: center;
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #E84E0F;
  font-size: 1.4em;
  border-top: 1px solid #E84E0F; 

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #E84E0F;  
  font-size: 1.5em;
}

/*#notices .notice {
  font-size: 1.6em;
}*/

#notices .referido {
  text-transform: uppercase;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

        
          /*background: url('../../public/images/dimension.png'); */
        
    </style>  
</head>
<body>
   

    <header class="clearfix">
      <div class="table-responsive">
      <table border="0" cellspacing="0" cellpadding="0"  >
        <thead>
          <tr >
            <th style="background: #ffffff;padding: 0px;">
                <div style="margin-top:2em" id="logo">
                  <img src="images/Logos/LogoSagaz.JPG" alt="matriz">   
                  <!--img src="logo.png"-->
                </div>
            </th>
            <th style="background: #ffffff;">
                <div id="company" style="margin-top:1em">
                  <h2 class="name">SAGAZ S.A.S</h2>
                  <div>Nit 900247117-9</div>
                  <div>Calle 12 Norte N° 9N - 59 Ofc. 201</div>
                  <div>Barrio Granada - Cali, Colombia</div>
                  <div>Telefono: 3876790 </div>  
                  <div><a href="#">info@sagazsas.com.co</a></div>
                  <div><a href="#">www.sagazsas.com.co</a></div>
                </div>
            </th>
          </tr>
        </thead>
      </table>
    </div>
    </header>

    <main><br>

      <div style="margin-top:6em" id="details">
      <center><h3  class="name">DOCUMENTO ORIGINAL</h3></center>
        <table border="0" cellspacing="0" cellpadding="0" >
          <thead>
            <tr >
              <th style="background: #ffffff;padding: 0px;text-align: left;">
                <div id="client">
                    <div class="to">CLIENTE</div>
                    <h2 class="client name">{{ $facturas[0]->nombre_cliente }}</h2>
                    <h2 class="client responsable">{{ $facturas[0]->contacto }}</h2>
                    <h2 class="client name">{{ $facturas[0]->area_des }}</h2>
                    <h2 class="client">Nit-{{ $facturas[0]->nit }}</h2>
                    <h2 class="client">Telefono. {{ $facturas[0]->telefono }}</h2>
                    <h2 class="client">{{ $facturas[0]->direccion }} <br>{{ $facturas[0]->ciudad }}</h2>
                </div>
              </th>
              <th style="background: #ffffff;padding: 0px;"><br>
                <div id="invoice">
                    <h3>FACTURA DE VENTA SAG-{{ $facturas[0]->id_factura}}</h3>
                    <div class="date">Fecha de expedición : {{ $facturas[0]->fec_expedicion }}</div>
                    <div class="date">Fecha de vencimiento : {{ $facturas[0]->fec_vencimiento }}</div>
                    <div class="date">Forma de pago {{ $facturas[0]->forma_pago}} @if(isset($facturas[0]->plazo)) {{ ": ".$facturas[0]->plazo  }} @endif</div>
                    <div class="date">Orden de compra:@if(isset($facturas[0]->orden)){{ $facturas[0]->orden }} @endif</div>
                </div>
              </th>
            </tr>
          </thead>
        </table>
      </div>
<center>
      <table style="margin-top:8em" border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <!--th class="no">#</th-->
            <th  class="desc1" style="width:150px;">SERVICIO</th>
            <th class="desc" style="width:250px;">DESCRIPCIÓN</th>
            <th class="unit">Vr. UNITARIO</th>
            <th class="qty">CANTIDAD</th>
            <th class="total">TOTAL</th>
          </tr>
        </thead>
          <tbody>
             @foreach($facturas as $factura)
               <tr> 
                  <td class="desc" style="width:150px;"><h3>{{ $factura->nombre_producto }}</h3></td>
                  <td class="desc" style="width:150px;"><h3>{{ $factura->cat_descripcion }}</h3></td>
                  <td class="unit">${{ number_format($factura->precio_cliente, 0) }}</td>
                  <td class="qty">{{ $factura->cantidad }}</td>
                  <td class="total">${{ number_format( $factura->neto,0) }}</td>
                </tr>
              @endforeach
              
          </tbody>
          <tfoot>
            <tr>
              <td colspan="2"></td>
              <td colspan="2">Total Bruto</td>
              <td class="totalfact">$<?php echo number_format($facturas[0]->bruto, 0); ?></td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2">Descuento</td>
              <td class="totalfact">$<?php echo number_format($facturas[0]->fac_descuento) ?></td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2">IVA</td>
              <td class="totalfact">$<?php echo number_format($facturas[0]->iva); ?></td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2">Retefuente</td>
              <td class="totalfact">$0</td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2">ICA</td>
              <td class="totalfact">$0</td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2">TOTAL FACTURA</td>
              <td class="totalfact">$<?php echo number_format($facturas[0]->total, 0); ?></td>
            </tr>
          </tfoot>
        
      </table></center>
      <div id="thanks">Valor en letras {{ $letras }}}</div>
      <div id="thanks">Gracias!</div>
      <div id="notices">
       
      </div>
      
    </main>
    <footer>
      <!--div style="position:absolute;bottom:1px;padding:0;">Refererido por 
        
      </div-->
      <div style="position:absolute;left:500px;bottom:70px;padding:0;"> 
        <span style="font-weight:bold;font-size: 0.8em;">Sagaz s.a.s impreso Nit 900247117-9</span>
      </div>
      <img src="{{ asset('images/Logos/lobopeque.png')}}" alt="matriz" style="width:20px">   Piensa Sagaz, hacemos fácil lo dificí <br>
      Resolución DIAN No 50000397902 de 25/08/2015 del N° SAG 6650 hasta 10000 a papel y GAZ 5002 hasta el 10000 a computador. Esta factura
            se asimila para todos sus efectos legales a la letra de cambio Art. 774 del codigo 08679 del 17/10/2013.
    </footer>
</body>
</html> 
@include('layouts.dash.header')
@include('layouts.dash.menu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles" style="margin: 10px 0;">
      <div class="header">
        <h1><center>LISTA FACTURAS </center></h1>
      </div>
      @include('alertas.notificacion')
      <div class="body table-responsive">
        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
          <thead>
            <tr>
              <th>Descargar</th>
              <th>Fecha Expedicion</th>
              <th>Fecha Vencimiento</th>
              <th>Cliente</th>
              <th>Estado</th>
              <th>Total</th>
              <th>Editar</th>
              <th>Anular</th>
              <th>Finalizar</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody>
          @foreach($facturas as $factura)
            <tr>
              <td><a class="btn btn-info" target="_blank" href="{{ url('financiero/pdf?factura='.$factura->id_factura) }}">SAG-{{ $factura->id_factura }}</a></td>
              <td>{{ $factura->fec_expedicion }}</td>
              <td>{{ $factura->fec_vencimiento }}</td>
              <td>{{ $factura->nombre_cliente }}</td>
              <td>{{ $factura->estado }}</td>
              <td>${{ number_format($factura->total)  }}</td>
              <td><a class="btn btn-warning" href="{{ url('financiero/editar?factura='.$factura->id_factura) }}">Editar</a></td>
              <td><a class="btn btn-danger" href="{{ url('financiero/anular?factura='.$factura->id_factura) }}">Anular</a></td>
              <td><a class="btn btn-success" href="{{ url('financiero/finalizar?factura='.$factura->id_factura) }}">Finalizar</a></td>
              @if($factura->estado == "Finalizado" or $factura->estado == "Anulada" )
                <td>Cerrada</td>
              @endif
              @if($factura->estado == "Pendiente" )
                <td><a class="btn btn-primary" href="{{ url('financiero/editar?factura='.$factura->id_factura) }}">Orden</a></td>
              @endif
              @if($factura->estado == "Pendiente-R" )
                <td><a class="btn btn-primary" href="{{ url('financiero/cambioestados?factura='.$factura->id_factura.'&estado='.$factura->estado) }}">Resivido.</a></td>
              @endif
              @if($factura->estado == "Pendiente-P" )
                <td><a class="btn btn-primary" href="{{ url('financiero/cambioestados?factura='.$factura->id_factura.'&estado='.$factura->estado) }}">Pagó.</a></td>
              @endif 
            </tr>
          @endforeach
          </tbody>
        </table> {!! $facturas->render() !!}
      </div>
    </div>
  </div>
</div>
@include('layouts.dash.footer')

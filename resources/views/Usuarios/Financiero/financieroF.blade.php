﻿@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')Financiero @endslot
 @slot('contenido')
   @slot('volver')
 <a href="{{ url('/') }}" class="btn btn-danger" class="bars">Pagina Principal<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
<div class="container-fluid">
   <br><br><br><br>
<br><br><br><br>
<center>


<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/FACTURACION.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Faturacion</h2></center>
                <p>
                    <a href="{{ url('financiero/index') }}" class="btn btn-success btn-block">Facturar</a> 
                    <a href="{{ url('financiero/listar') }}" class="btn btn-success btn-block">Listar</a> 
                    <a href="{{ url('reportes/filtros') }}" class="btn btn-success btn-block">Reportes</a>
                </p>  
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/COTIZACION.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Cotización o inversion</h2></center>
                <p>
                    <a href="{{ url('cotizacion/cotizar') }}" class="btn btn-success btn-block">Cotizar</a> 
                    <a href="{{ url('cotizacion/listar') }}" class="btn btn-success btn-block">Opc.Cotizacion</a>
                    <a href="{{ url('inversion/listar') }}" class="btn btn-success btn-block">Opc.Inversion</a> 
                 </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/PRODUCTOS.png')}}" alt=""/>
            <figcaption>
                 <h2><span>Productos </span></h2>
                    <p>
                        <a href="{{ url('productos/crear') }}" class="btn btn-success btn-block">Crear</a> 
                        <a href="{{ url('productos/index') }}" class="btn btn-success btn-block">Listar</a> 
                     </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/CLIENTES.png')}}" alt=""/>
            <figcaption>
                            <h2><span>Clientes</span></h2>
                            <p>
                                <a href="{{ url('clientes/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a href="{{ url('clientes/index') }}" class="btn btn-success btn-block">Listar</a> 
                            </p>
            </figcaption>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/RESPONSABLE.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Responsable</span></h2>
                            <p>
                                <a href="{{ url('responsables/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a href="{{ url('responsables/index') }}" class="btn btn-success btn-block">Listar</a> 
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul><!--
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Reportes Graficos Financiero</span></h2>
                            <p>
                                <a href="{{ url('graficos/reportegrafico') }}" class="btn btn-success btn-block">Crear</a>  
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>-->
   </center>        
    

               
                    </div>
                </div>

 
     @endslot
   @endcomponent
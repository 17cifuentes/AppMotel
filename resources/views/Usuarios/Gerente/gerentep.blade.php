@component('layouts.admin')
 @slot('titulo')Corporativo Sagaz @endslot
  @slot('cargo')GERENTE @endslot
     @slot('volver')
 <a href="{{ url('/') }}" class="btn btn-danger" class="bars">Pagina Principal<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>
 
 @endslot
 @slot('contenido')

<div class="container-fluid " style="margin-left: 8em">
   <br><br><br><br>
<br><br><br><br>
<center>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/fac.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Usuarios</h2></center>
                <p>
                    <a href="{{ url('usuarios/registrar') }}" class="btn btn-success btn-block">Crear</a> 
                    <a href="{{ url('usuarios/index') }}" class="btn btn-success btn-block">Listar</a> 
                </p>  
            </figcaption>
        </figure>
    </li>

</ul>

<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/FACTURACION.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Facturación</h2></center>
                <p>
                    <a href="{{ url('financiero/index') }}" class="btn btn-success btn-block">Facturar</a> 
                    <a href="{{ url('financiero/listar') }}" class="btn btn-success btn-block">Listar</a> 
                    <a href="{{ url('reportes/filtros') }}" class="btn btn-success btn-block">Reportes</a>
                </p>  
            </figcaption>
        </figure>
    </li>

</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/COTIZACION.png')}}" alt=""/>
            <figcaption>
               <center> <h2>Cotización o inversion</h2></center>
                <p>
                    <a href="{{ url('cotizacion/cotizar') }}" class="btn btn-success btn-block">Cotizar</a> 
                    <a href="{{ url('cotizacion/listar') }}" class="btn btn-success btn-block">Opc.Cotizacion</a>
                    <a href="{{ url('inversion/listar') }}" class="btn btn-success btn-block">Opc.Inversion</a> 
                 </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/PRODUCTOS.png')}}" alt=""/>
            <figcaption>
                 <h2><span>Productos </span></h2>
                    <p>
                        <a href="{{ url('productos/crear') }}" class="btn btn-success btn-block">Crear</a> 
                        <a href="{{ url('productos/index') }}" class="btn btn-success btn-block">Listar</a> 
                     </p> 
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/CLIENTES.png')}}" alt=""/>
            <figcaption>
                            <h2><span>Clientes</span></h2>
                            <p>
                                <a href="{{ url('clientes/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a href="{{ url('clientes/index') }}" class="btn btn-success btn-block">Listar</a> 
                            </p>
            </figcaption>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/RESPONSABLE.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Responsable</span></h2>
                            <p>
                                <a href="{{ url('responsables/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a href="{{ url('responsables/index') }}" class="btn btn-success btn-block">Listar</a> 
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/almu.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Almuerzos Sagaz</span></h2>
                            <p>
                                <a  href="{{ url('almuerzos/reportar') }}" class="btn btn-success btn-block">Reportar Almuerzo</a> 
                               
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/GRAFICOS.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Reportes Graficos</span></h2>
                            <p>
                                <a  href="{{ url('graficos/crear') }}" class="btn btn-success btn-block">Crear</a> 
                               
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/GRAFICOS.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Area</span></h2>
                            <p>
                                <a  href="{{ url('area/crear') }}" class="btn btn-success btn-block">Crear</a> 
                                <a  href="{{ url('area/listar') }}" class="btn btn-success btn-block">Listar</a> 
                               
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>
<ul class="demo-3">
    <li>
        <figure>
            <img src="{{ asset('images/img_menu/GRAFICOS.png')}}" alt=""/>
            <figcaption>
              
                            <h2><span>Inventario</span></h2>
                            <p>
                                <a  href="{{ url('inventario/crear') }}" class="btn btn-success btn-block">Agregar Elemento</a> 
                                <a  href="{{ url('inventario/listar') }}" class="btn btn-success btn-block">Listar Elementos</a> 
                               
                            </p>   
            </figcaption>
        </figure>
    </li>
</ul>


   </center>        
    


               
                    </div>
                </div>

 
     @endslot
   @endcomponent
@component('layouts.viewcorparativo')
 @slot('titulo')
 Corporativo Sagaz 
 @endslot
 @slot('contenido')<br><br><br><br>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('images/sagaz.ico') }}">
 <!--  ESPACIO PARA EL UNICIO DE SESION -->
    <div  class="clearfix col-lg-12" >
        <div  class="col-lg-6 col-md-4 col-sm-6 col-xs-12 col-lg-offset-3">
            <div class="card " >



                @if(session("error"))
                    <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Upps!</strong>{{ session("error") }}
                    </div>
                @endif
                <div class="alert alert-warning header" >
                    <center><h2 style="color:white"><img style="width:30%" src="{{ asset('images/iconos/sag.png') }}"><br>Ingreso personal sagaz</h2></center>
                </div>
                <div class="body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
                    
                         <div class="fogrorm-up{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Usuario</label>
                            <div class="col-md-6">
                                <input  id="email" type="email" class="form-control" name="email" required>

                                </div>
                            </div>
                        <div class="formg-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input  id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                                                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-5">
                                <button type="submit" class="btn btn-default btn-lg">Ingresar</button>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         {!! csrf_field () !!}
                    </form><br>
                                      
                </div>
            </div>
        </div>
    </div>
    <!--FIN DEL INICIO DE SESION-->
    <!--  ESPACIO DE ESTADISTICAS FINANCIERAS SAGAZ-->
    <?php
    setlocale(LC_TIME, 'spanish');  
    $fecha =  strftime("%A %d de %B del %Y");
    $fecha2 = date("m");
    $mes1="";$mes2="";$mes3="";
     if($fecha2 >=01 and $fecha2 <=3){
        $trimestre = "Primer";
        $mes1 = "01";$mes2 = "02";$mes3 = "03";
     }else if($fecha2 >="04"      and $fecha2 <="06"){
        $trimestre = "Segundo";
        $mes1 = "04";$mes2 = "05";$mes3 = "06";
     }else if($fecha2 >="07" and $fecha2 <="09"){
        $trimestre = "Tercer";
        $mes1 = "07";$mes2 = "08";$mes3 = "09";
     }else if($fecha2 >="10" and $fecha2 <="12"){
        $trimestre = "Cuarto";
        $mes1 = "10";$mes2 = "11";$mes3 = "12";
     }
     
     ?>
    
    
    <input type="hidden" id="mes1" value="{{ $mes1 }}">
    <input type="hidden" id="mes2" value="{{ $mes2 }}">
    <input type="hidden" id="mes3" value="{{ $mes3 }}">
    <h3 ><br><br><br><center>Graficos estadisticos del {{ $trimestre }} trimestre </center></h3>
        <hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Graficos de facturacion  </h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="39" height="13" id="grafico"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Grafico Diseño y publicidad</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="39" height="13" id="grafico2"></canvas></div>
                </div>
            </div>
        </div>
            <h3><br><br><br><center>LOGISTICA</center></h3>
        <hr >
        <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Graficos de Clientes</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="39" height="13" id="grafico3"></canvas></div>
                </div>
            </div>
        </div>
         <div  class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="height:28em" >
                <div class="header" ><h2>Graficos de Personal</h2></div>
                <div  class="body">
                    <div class="resultados"><canvas width="39" height="13" id="grafico4"></canvas></div>
                </div>
            </div>
        </div>

        <canvas id="grafico"></canvas>
        <script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
            <script type="text/javascript">


 function valoresdos(ruta,contenedor,grafico,mes1,mes2,mes3){
    
            $.ajax({
            url : ruta,
            type : "get",
            data : "mes1="+mes1+"&mes2="+mes2+"&mes3="+mes3 ,
            success:function(datos){          
             dat=eval(datos);
             m1 = dat[0];
             m2 = dat[1];
             m3 = dat[2];
//alert(m3);
var ctx = document.getElementById(contenedor);
var data = {
    datasets: [{
data: [m1,m2,m3],
        backgroundColor: [
            "#FF6384",
            "#4BC0C0",
            "#FFCE56",
            "#E7E9ED",
            "#36A2EB"
        ],
        label: 'My dataset' // for legend
    }],
labels: ["Mes "+mes1,"Mes "+mes2,"Mes "+mes3],};
new Chart(ctx, {
    data: data,
    type: grafico
    
});
}
        })
            }

function otros(){
mes1 = $("#mes1").val();
mes2 = $("#mes2").val();
mes3 = $("#mes3").val();   
valoresdos('graficos/valores','grafico','doughnut',mes1,mes2,mes3);
valoresdos('graficos/valores','grafico2','bar',mes1,mes2,mes3);
valoresdos('graficos/valores','grafico3','horizontalBar',mes1,mes2,mes3);
valoresdos('graficos/valores','grafico4','polarArea',mes1,mes2,mes3);
}
        </script>        
     @endslot
   @endcomponent
<!DOCTYPE html>
<html>
<fb:login-button 
    appId=" *** " 
    scope="public_profile,email" 
    id="facebook-login-button">
</fb:login-button>
<div id="fbStatus"></div>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>{{ $titulo  or "Sagaz"}}</title>
    <!-- Favicon-->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<style type="text/css">
    .b{
        margin-top: 1em;
    }


</style>
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('css/themes/all-themes.css')}}" rel="stylesheet" />

    <link rel="shortcut icon" href="{{ asset('images/iconos/sagaz.png') }}">
       <script src="{{ asset('plugins/jquery/jquery.js')}}"></script>
    <script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>
    
</head>




<body class="theme-red" onload="otros()" style="background-color: #1f2e3e;color:white">
    <!-- #Top Bar -->
    <section>
           <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html"><strong>SAGAZ-S.A.S</strong> <em>Hacemos Facil Lo Dificil</em></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
          
  
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
<a href="{{ url('/') }}" class="btn btn-danger" class="bars">Pagina Principal<img style="margin-left:1em" src="{{ asset('images/iconos/volver.png')}}"></a>

            </div>
        </div>
    </nav>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
    
    
        <div class="container-fluid">

        <!-- #END# Right Sidebar -->

      
       {{ $contenido }}
  
         <!-- Jquery Core Js -->
 

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js')}}"></script>
   

    <!-- ChartJs -->
    <script src="{{ asset('plugins/chartjs/Chart.bundle.js')}}"></script>

    <!-- Flot Charts Plugin Js -->
        <script src="{{ asset('plugins/node-waves/waves.js')}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('plugins/morrisjs/morris.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js')}}"></script>
    

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js')}}"></script>


    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>





<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    
    
    
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>

    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    
    <script src="{{ asset('js/pages/tables/jquery-datatable.js')}}"></script>



    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js')}}"></script>
    

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="{{ asset('js/facturacion.js')}}"></script>

    <script type="text/javascript">

        function editcategoria(){
            var des=$("#pro option:selected").text();
            var val=$("#valorproducto").val();

            $("#edit_desc").val(des);
            $("#edit_valor").val(val);


}


    function posteditar(){
            var edit_desc=$("#edit_desc").val();
            var edit_valor=$("#edit_valor").val();
            var id = $("#pro").val();
            
        $.ajax({
            url : "http://localhost/sagaz/public/facturacion/editpro",
            type : "get",
            data : "id="+id+"&edit_valor="+edit_valor+"&edit_desc="+edit_desc,
            success:function(){
            var nombrep = $("#nombrep").val(); 
             nompro(nombrep);
             $("#editcategoria").modal("hide");


            }
        })


}
  function nompro(id){
        $.ajax({
            url : "http://localhost/sagaz/public/facturacion/vercategorias",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#pro").html(datos);

            }
        })


}


    function valorpro(id_pro){
        
        $.ajax({
            url : "http://localhost/sagaz/public/facturacion/valorpro",
            type : "get",
            data : "id="+id_pro,
            success:function(datos){
             
             $("#valorproducto").attr('value',datos);

            }
        })


}
    function darresponable(id_cliente){

        $.ajax({
            url : "http://localhost/sagaz/public/facturacion/darresponsable",
            type : "get",
            data : "area="+id_cliente,
            success:function(datos){
             
             $("#responsable").html(datos);

            }
        })


}

    function dararea(id_cliente){

        $.ajax({
            url : "http://localhost/sagaz/public/facturacion/dararea",
            type : "get",
            data : "id="+id_cliente,
            success:function(datos){
             
             $("#area").html(datos);

            }
        })


}


  function ver(opc){
    if(opc == "1"){
        $("#facid").attr("target","_blank");
    }else if(opc == "2"){
        $("#facid").attr("target","");
    }
        
}
function formapago(forma){
 if(forma == "Credito"){
        $("#forma").html("<label>Plazo(dias) :</label><select class='form-control' name='plazo' onchange='darfecha(this.value)'><option>Elija Opcion</option><option>30</option><option>60</option><option>90</option></select");

    }else{
         $("#forma").html("");
    }
        
}

function darfecha(tiempo){
 
  var fecha = ("#fec_expedicion").val();
        dia = fecha.getDate(),
        mes = fecha.getMonth() + 1,
        anio = fecha.getFullYear(),
        addTime = tiempo * 86400;
        fecha.setSeconds(addTime);
        $("#venci").val(fecha);
}
   function quitar(id){
    
    var indice = id - 1;
    var cate = $("#cate"+id).val();
    var canti = $("#cate"+id).val();
    var precio = $("#precio"+id).val();
    var desc = $("#desc"+id).val();
    var des_valor = $("#des_valor"+id).val();
    var netpro = $("#netpro"+id).val();

    var totalbruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var totaliva = $("#totaliva"+id).val();
    var desc = $("#totalfactura"+id).val();
    
    var a = totalbruto - netpro;
    var b = descuentot - des_valor;
    var iva = a * 0.19;
    var c = a + iva;
    
            $.ajax({
            url : "http://localhost/sagaz/public/facturacion/quitarproducto",
            type : "get",
            data : "a="+a+"&b="+b+"&c="+c+"&iva="+iva,
            success:function(datos){
             
             $("#calculos").html(datos);
             $("#fila"+id).remove();
            }
        })

}
    </script>

</body>

</html>
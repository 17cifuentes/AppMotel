<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="es">
<!--<![endif]-->

<head>

    <title> Servicios administractivos | Sagaz - Hacemos facil lo dificil</title>
        
    <!-- meta character set -->
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="max-age=3600, must-revalidate">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Meta Description -->
    <meta name="description" content="Empresa privada dedicada a la prestación de servicios, brindando apoyo en los diferentes procesos administrativos y productivos, Manejo de bases, creación y Diseño de paginas web.">
    <meta name="keywords" content="sagaz, sagaz sas,prestacion de servicios, prestacion de servicios en Cali, diseño paginas web, creación paginas web, manejo bases de datos, creacion aplicativos web">
    <meta name="author" content="sagaz sas">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="{{ asset('images/iconos/sagaz.png') }}">

    <!-- CSS
		================================================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,700,800,900" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Fontawesome Icon font -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- bootstrap.min -->
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
    <!-- bootstrap.min -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <!-- bootstrap.min -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <!-- bootstrap.min -->
    <link rel="stylesheet" href="{{ asset('css/slit-slider.css') }}">
    <!-- bootstrap.min -->
    <!--link rel="stylesheet" href="{{ asset('css/animate.css') }}"-->
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <!--  equipo sagaz cube portfolio -->
    <link rel="stylesheet" href="{{ asset('plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.css') }}">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('plugins/owl-carousel2/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl-carousel2/css/owl.theme.default.css') }}">

    <!-- Modificaciones a Owl Carousel - cdiaz-->
    <link rel="stylesheet" href="{{ asset('css/cssdiaz.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/timeline3d/css/style.css') }}" />


<link rel="shortcut icon" href="{{ asset('css/redes.css') }}">
    <!-- Modernizer Script for old Browsers -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>
</head>

<body id="body">

    <!-- preloader -->
    <div id="preloader">
        <div class="loder-box">
            <div class="battery"></div>
        </div>
    </div>
    <!-- end preloader -->

    @yield('content')

    <!-- Essential jQuery Plugins
        ================================================== -->
    <!-- Main jQuery -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!-- Twitter Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Single Page Nav -->
    <script src="{{ asset('js/jquery.singlePageNav.min.js') }}"></script>
    <!-- jquery.fancybox.pack -->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!-- Google Map API -->
    <!--script src="http://maps.google.com/maps/api/js?sensor=false"></script-->
    <!-- Owl Carousel -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- jquery easing -->
    <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
    <!-- Fullscreen slider -->
    <script src="{{ asset('js/jquery.slitslider.js') }}"></script>
    <script src="{{ asset('js/jquery.ba-cond.min.js') }}"></script>

    <!-- onscroll animation -->
    <!--script type="text/javascript" src="{{ asset('js/jquery.inview.min.js') }}"></script-->
    <!--script type="text/javascript" src="{{ asset('js/wow.min.js') }}"></script-->
    <!--script src="{{ asset('js/wow.min.js') }}"></script-->

    <!-- PLUGIN SCROLLME MASTER-->
    <script src="{{ asset('plugins/scrollme-master/js/jquery.scrollme.min.js') }}"></script>

    <!-- Custom Functions -->
    <script src="{{ asset('js/main.js') }}"></script>

    <!-- linea de tiempono.2 -->
    <script src="{{ asset('plugins/horizontal-timeline/js/jquery.mobile.custom.min.js') }}"></script>
    <script src="{{ asset('plugins/horizontal-timeline/js/modernizr.js') }}"></script>
    <!-- Modernizr -->
    <script src="{{ asset('plugins/horizontal-timeline/js/main.js') }}"></script>
    <!-- Resource jQuery -->

    <!-- load cubeportfolio -->
    <script type="text/javascript" src="{{ asset('plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>
    <!-- init cubeportfolio -->
    <script type="text/javascript" src="{{ asset('plugins/cube-portfolio/cubeportfolio/main.js') }}"></script>
    <!--  LINEA DE TIEMPO -->
    <script src="{{ asset('plugins/timeline/js/jquery.timelinr-0.9.6.js') }}"></script>

    <!-- OWL CAROUSEL2 -->
    <script src="{{ asset('plugins/owl-carousel2/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('plugins/owl-carousel2/vendor/highlight.js') }}"></script>
    <script src="{{ asset('plugins/owl-carousel2/js/app.js') }}"></script>

    <script src="{{ asset('plugins/timeline3d/js/modernizr.custom.63321.js') }}"></script>

    <!--Script para CSS3 ANIMATED -->
    <script src="{{ asset('plugins/animate-master/js/css3-animate-it.js') }}"></script>


    <script>
        $(document).ready(function() {
            var owl = $('.owl-carousel');

            owl.owlCarousel({
                items: 4,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 5,
                        nav: true,
                        loop: true
                    }
                }
            })

            $('.play').on('click', function() {
                owl.trigger('play.owl.autoplay', [1000])
            })

            $('.stop').on('click', function() {
                owl.trigger('stop.owl.autoplay')
            })

            $('.item').mouseout(function() {
                owl.trigger('play.owl.autoplay', [1000])
            })

            $('.item').mouseover(function() {
                owl.trigger('stop.owl.autoplay')
            })
        });

        $(function() {
            $().timelinr({
                autoPlay: 'true',
                autoPlayDirection: 'forward',
                autoPlayPause: 1200000
            })
        });

        $(".revealOnScroll:not(.animated)").each(function () {
  var $this     = $(this),
      offsetTop = $this.offset().top;

  if (scrolled + win_height_padded > offsetTop) {
    if ($this.data('timeout')) {
      window.setTimeout(function(){
        $this.addClass('animated ' + $this.data('animation'));
      }, parseInt($this.data('timeout'),10));
    } else {
      $this.addClass('animated ' + $this.data('animation'));
    }
  }
});
// Hidden...
$(".revealOnScroll.animated").each(function (index) {
   var $this     = $(this),
       offsetTop = $this.offset().top;
   if (scrolled + win_height_padded < offsetTop) {
     $(this).removeClass('animated fadeInUp flipInX lightSpeedIn')
   }
});


    </script>

</body>

</html>
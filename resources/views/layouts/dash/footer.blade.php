        <!-- footer content -->
        <footer>
          <div class="pull-right">
           Diseñado Por  <a href="https://colorlib.com">Sagaz S.A.S</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('dash/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('dash/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('dash/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{ asset('dash/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js')}} -->
    <!-- <script src="{{ asset('dash/vendors/Chart.js')}}/dist/Chart.min.js')}}"></script>-->
    <!-- jQuery Sparklines -->
    <script src="{{ asset('dash/vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- morris.js')}} -->
    <script src="{{ asset('dash/vendors/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('dash/vendors/morris.js/morris.js')}}"></script>
    <!-- gauge.js')}} -->
    <script src="{{ asset('dash/vendors/gauge.js/dist/gauge.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('dash/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{ asset('dash/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{ asset('dash/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('dash/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{ asset('dash/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{ asset('dash/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{ asset('dash/vendors/DateJS/build/date.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('dash/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('dash/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('dash/build/js/custom.min.js')}}"></script>
        <script src="{{ asset('js/tables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/tables/tablesfuncion.js')}}"></script>
    
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    
    
    
    <script type="text/javascript">
    $(document).ready(function(){
    function correo(){
    alert("cococ");
                var nombre = $("#nombre").val();
                var correo = $("#correo").val();
                var mensaje = $("#mensaje").val();
                $.ajax({
                    url : "portal/contacto",
                    type : "get",
                    data : "nombre="+nombre+"&correo="+correo+"&mensaje="+mensaje,
                    success:function(){
                    $("#alert").html('<div class="alert alert-success alert-dismissable">\
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\
  <strong>Notificación!</strong> Mensaje enviado exitosamente, pronto nos pondremos en contacto.\
</div>');
                    $("#form")[0].reset();
                }
                });
            }



        function correo(inv){
            $("#inv").attr("value",inv);
            $("#correo").modal("show");
        }

        function daraporte(inv){
            $("#id_aporte").attr("value",inv);
            $("#aporte").modal("show");
        }
        function editcategoria(){
            var des=$("#pro option:selected").text();
            var val=$("#valorproducto").val();

            $("#edit_desc").val(des);
            $("#edit_valor").val(val);


}


    function posteditar(){
            var edit_desc=$("#edit_desc").val();
            var edit_valor=$("#edit_valor").val();
            var id = $("#pro").val();
            
        $.ajax({
            url : "financiero/editpro",
            type : "get",
            data : "id="+id+"&edit_valor="+edit_valor+"&edit_desc="+edit_desc,
            success:function(){
            var nombrep = $("#nombrep").val(); 
             nompro(nombrep);
             $("#editcategoria").modal("hide");


            }
        })


}
  function nompro(id){
        $.ajax({
            url : "financiero/vercategorias",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#pro").html(datos);

            }
        })


}


    function valorpro(id_pro){
        
        $.ajax({
            url : "financiero/valorpro",
            type : "get",
            data : "id="+id_pro,
            success:function(datos){
             
             $("#valorproducto").attr('value',datos);

            }
        })


}
    function darresponable(id_cliente){

        $.ajax({
            url : "financiero/darresponsable",
            type : "get",
            data : "area="+id_cliente,
            success:function(datos){
             
             $("#responsable").html(datos);

            }
        })


}
    function darelemento(id){

        $.ajax({
            url : "inventario/darelemento",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#elemento").html(datos);

            }
        })


}

    function dararea(id_cliente){
alert("asdasd");
        $.ajax({
            url : "financiero/dararea",
            type : "get",
            data : "id="+id_cliente,
            success:function(datos){
             
             $("#area").html(datos);

            }
        })


}


  function ver(opc){
    if(opc == "1"){
        $("#facid").attr("target","_blank");
    }else if(opc == "2"){
        $("#facid").attr("target","");
    }
        
}
function formapago(forma){
 if(forma == "Credito"){
        $("#forma").html("<label>Plazo(dias) :</label><select class='form-control' name='plazo' onchange='darfecha(this.value)'><option>Elija Opcion</option><option>30</option><option>60</option><option>90</option></select");

    }else{
         $("#forma").html("");
    }
        
}

function darfecha(tiempo){
 
  var fecha = ("#fec_expedicion").val();
        dia = fecha.getDate(),
        mes = fecha.getMonth() + 1,
        anio = fecha.getFullYear(),
        addTime = tiempo * 86400;
        fecha.setSeconds(addTime);
        $("#venci").val(fecha);
}
   function quitar(id){
    
    var indice = id - 1;
    var cate = $("#cate"+id).val();
    var canti = $("#cate"+id).val();
    var precio = $("#precio"+id).val();
    var desc = $("#desc"+id).val();
    var des_valor = $("#des_valor"+id).val();
    var netpro = $("#netpro"+id).val();

    var totalbruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var totaliva = $("#totaliva"+id).val();
    var desc = $("#totalfactura"+id).val();
    
    var a = totalbruto - netpro;
    var b = descuentot - des_valor;
    var iva = a * 0.19;
    var c = a + iva;
    
            $.ajax({
            url : "financiero/quitarproducto",
            type : "get",
            data : "a="+a+"&b="+b+"&c="+c+"&iva="+iva,
            success:function(datos){
             
             $("#calculos").html(datos);
             $("#fila"+id).remove();
            }
        })

}


var conpro = $("#guia").val();
    

function addproducto(){

   var producto = $("#nombrep").val();
    var procategoria = $("#pro").val();
    var cantpro = $("#cantproducto").val();
 conpro = parseInt(conpro) + 1; 
  
    var descuento = $("#descuento").val();
    var bruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var ivacal = $("#iva").val();
    var totalneto = $("#totalneto").val();
    var valor = $("#valorproducto").val();


      $.ajax({
            url : "financiero/addproducto",
            type : "get",
            data : "producto="+producto+"&conpro="+conpro+"&categoria="+procategoria+"&cantidad="+cantpro+"&descuento="+descuento+"&bruto="+bruto+"&totalneto="+totalneto,
            success:function(datos){
             
             $("#items").append(datos);
              var descuento = $("#descuento").val();
              var procategoria = $("#pro").val();
              var neto = $("#neto").val();
              var descuentot = $("#descuentot").val();
              var cantpro = $("#cantproducto").val();

               totales(descuento,procategoria,bruto,descuentot,ivacal,neto,cantpro);

            }
        })
  
}
function totales(descuento,procategoria,bruto,descuentot,ivacal,neto,cantpro){
            $.ajax({
            url : "financiero/totales",
            type : "get",
            data : "cantpro="+cantpro+"&netoca="+neto+"&descuento="+descuento+"&categoria="+procategoria+"&bruto="+bruto+"&descuentot="+descuentot+"&ivacal="+ivacal,
            success:function(datos){
             $("#calculos").html(datos);
             
            }
        })
}



function ver(foto){
    $('#foto').html("Los cambios se guardaran al dar en el boton actualizar pefil");
}

    </script>
    <script type="text/javascript">
function creargrupo(){
                $.ajax({
            url : "cargargrupo",
            type : "get",
            beforeSend: function(){
                $("#carga").modal("show");
            },            
            success:function(datos){
             $("#contenido").html(datos);
            $("#carga").modal("hide");                  
            }
        })
    
}
</script>

    </script>
 </body>
</html>
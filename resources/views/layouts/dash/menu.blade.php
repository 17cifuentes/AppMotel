
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{ url('/home') }}" class="site_title"><span> Sagaz S.A.S</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset(Auth::user()->user_img)}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>{{ Auth::user()->rol }}</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Corporativo</h3>
                <ul class="nav side-menu">
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "R-Humanos" or Auth::user()->rol == "Administrador")
                  <li><a><i class="fa fa-user"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('usuarios/registrar') }}">Crear</a></li>
                      <li><a href="{{ url('usuarios/index') }}">Listar</a></li>
                    </ul>
                  </li>
                  @endif
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Financiero" or Auth::user()->rol == "Comercial" or Auth::user()->rol == "Administrador" or Auth::user()->rol == "Diseño")
                  @endif
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Financiero" or Auth::user()->rol == "Administrador" or Auth::user()->rol == "Comercial" or Auth::user()->rol == "Diseño")
                  @endif
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Financiero" or Auth::user()->rol == "Administrador" or Auth::user()->rol == "Comercial" or Auth::user()->rol == "Diseño")
                  <li><a><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i> Habitaciones <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('clientes/crear') }}">Añadir</a></li>
                      <li><a href="{{ url('clientes/index') }}">Listar</a></li> 
                    </ul>
                  </li>
                  @endif
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Financiero" or Auth::user()->rol == "Administrador" or Auth::user()->rol == "Comercial" or Auth::user()->rol == "Diseño")

                  @endif
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Financiero" or Auth::user()->rol == "Administrador" or Auth::user()->rol == "Comercial" or Auth::user()->rol == "Diseño")

                  @endif
                  

                  
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Administrador")

                  @endif
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Administrador")

                  @endif
                   <!--
                    @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "R-Humanos"  or Auth::user()->rol == "Administrador")
                  <li><a><i class="fa fa-windows"></i> Otros Gastos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('humanos/registrogasto') }}">Registro Nuevo Gasto</a></li>
                      <li><a href="{{ url('reportes/almuerzos') }}">Reportes de otros gastos</a></li>
                    </ul>
                  </li>
                  @endif
                 
                  @if(Auth::user()->rol == "Gerente" or Auth::user()->rol == "Diseño" or Auth::user()->rol == "Administrador")
                  <li><a><i class="fa fa-windows"></i> Bodega Diseños <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url('humanos/registrogasto') }}">Cargar Diseño
                      <li><a href="{{ url('reportes/almuerzos') }}">Listar Todos</a></li>
                    </ul>
                  </li>
                  @endif  -->                
                </ul>
              </div>
              <div class="menu_section">
                <!--<h3>Live On</h3>-->
                  <!-- <ul class="nav side-menu">

                 
               <li><a><i class="fa fa-sitemap"></i> Inventario <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('inventario/crear') }}">Agregar Elemento</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="{{ url('inventario/listar') }}">Listar Elementos</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>            
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>      
                </ul>-->
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{ asset(Auth::user()->user_img)}}" alt="">{{ Auth::user()->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="{{ url('usuarios/perfil') }}"> Perfil</a></li>
                                                               <li>                                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" ><i class="fa fa-sign-out pull-right"></i>
                                         Salir 
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                    
                  </ul>
                </li>

                <!--<li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="{{ asset('dash/production/images/img.jpg')}}" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="{{ asset('dash/production/images/img.jpg')}}" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="{{ asset('dash/production/images/img.jpg')}}" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="{{ asset('dash/production/images/img.jpg')}}" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>-->
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title"><center>Registro Clientes Sagaz S.A.S</center></h2>
      </div>
      <div class="modal-body">
      <form action="{{ url('clientes/registrar') }}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        Nombre Cliente: <input style="border-style: solid;border-color: #ff0000 ;" type="text" name="nombre" class="form-control">
        Nit<input style="border-style: solid;border-color: #ff0000 ;" type="text" name="nit" class="form-control">
        Telefono<input style="border-style: solid;border-color: #ff0000 ;" type="number" name="telefono" class="form-control">
        Ciudad<input style="border-style: solid;border-color: #ff0000;" type="text" name="ciudad" class="form-control">
        Direccion<input style="border-style: solid;border-color: #ff0000 ;" type="text" name="direccion" class="form-control">
        Correo<input style="border-style: solid;border-color: #ff0000 ;" type="email" name="correo" class="form-control"><br>
        <button type="submit" name="opc" value="1" class="btn btn-success">Registrarse</button>
      </div>
      
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
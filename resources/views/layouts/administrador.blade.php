<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>{{ $titulo  or "Sagaz"}}</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<style type="text/css">
    .b{
        margin-top: 1em;
    }

</style>
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('plugins/morrisjs/morris.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('css/themes/all-themes.css')}}" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html"><strong>SAGAZ-S.A.S</strong> <em>Hacemos Facil Lo Dificil</em></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
          
  
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>4 sales made</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 22 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy Doe</b> deleted account</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy</b> changed name</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> commented your post</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">cached</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> updated status</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">settings</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Settings updated</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> Yesterday
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>

                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Footer display issue
                                                <small>32%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Make new buttons
                                                <small>45%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Create new dashboard
                                                <small>54%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Solve transition issue
                                                <small>65%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Answer GitHub questions
                                                <small>92%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Tasks</a>
                            </li>
                        </ul>
                    </li>

                    <!-- #END# Tasks -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                                      <li>                                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesion
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" >
                
                    <center><img class="img-circle" src="{{ asset('images/user_foto/user.jpg')}}" width="90" height="90" alt="User" />
                <div class="name" data-toggle="dropdown" aria-haspopup="true" style="color:white" aria-expanded="false">John Doe</div>
                  </center>  
                </div>

            </div>
            <!-- #User Info -->
            <!-- Menu -->
        <div class="menu" >
            <ul class="list">
                <li class="header">MENU DE NAVEGACIÓN</li>
                    <li class="active">
                        <a href="{{ url('/home') }}">
                            <i class="material-icons">home</i>
                            <span>Incio</span>
                        </a>
                    </li>
                    <li >
                        <a href="{{ url('usuarios/index') }}">
                            <i class="material-icons">person</i>
                            <span>Usuarios</span>
                        </a>
                    </li>
                    <!--<li>
                        <a href="{{ url('facturacion/index') }}">
                            <i class="material-iconsss">aignment</i>
                            <span>Facturacion</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/helper-classes.html">
                            <i class="material-icons">layers</i>
                            <span>Helper Classes</span>
                        </a>
                    </li>-->
        <li>
                <a href="javascript:void(0);" class="menu-toggle">
                   <i class="material-icons">list</i>
                   <span>Facturacion y Cotizacion</span>
                </a>
            <ul class="ml-menu">
        <li>
                  <a href="{{ url('facturacion/index') }}" >
                     <span>Facturar</span>
                   </a>
                </li>
                <li>
                    <a href="{{ url('facturacion/listar') }}" >
                        <span>Listar</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('cotizacion/listar') }}" >
                        <span>Opciones de Cotizacion</span>
                    </a>
                </li>                
                <li>
                    <a href="{{ url('reportes/filtros') }}" >
                        <span>Reportes de Facturacion</span>
                    </a>
                </li>                
            </ul>
        </li>
        <li>
                <a href="javascript:void(0);" class="menu-toggle">
                   <i class="material-icons">widgets</i>
                   <span>Productos</span>
                </a>
            <ul class="ml-menu">
        <li>
                  <a href="{{ url('productos/crear') }}" >
                     <span>Crear Productos</span>
                   </a>
                </li>
                <li>
                    <a href="{{ url('productos/index') }}" >
                        <span>Opciones de Productos</span>
                    </a>
                </li>
            </ul>
        </li>
    <li>
            <a href="javascript:void(0);" class="menu-toggle">
              <i class="material-icons">person</i>
                <span>Clientes</span>
            </a>
        <ul class="ml-menu">
             <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>Clientes</span>
                    </a>
                <ul class="ml-menu">
                    <li>
                      <a href="{{ url('clientes/crear') }}" >
                        <span>Crear Cliente</span>
                      </a>
                    </li>
                    <li>
                       <a href="{{ url('clientes/index') }}" >
                        <span>Opciones de Cliente</span>
                       </a>
                    </li>

                </ul>
            </li>

              <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                       <i class="material-icons">person</i>
                        <span>Responsable</span>
                    </a>
                <ul class="ml-menu">
                  <li>
                    <a href="{{ url('responsables/crear') }}" >
                     <span>Crear Responsable</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('responsables/index') }}" >
                      <span>Opciones de Responsable</span>
                    </a>
                   </li>
                 </ul>
               </li>

        </ul>
    </li>                            <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">credit_card</i>
                        <span>Inversion</span>
                    </a>
                <ul class="ml-menu">
                    <li>
                       <a href="{{ url('inversion/listar') }}" >
                        <span>Opciones de Inversion</span>
                       </a>
                    </li>
                    <li>
                       <a href="{{ url('clientes/index') }}" >
                        <span>Hacer Aporte</span>
                       </a>
                    </li>
                </ul>
            </li>
            </div>
        </section>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Sagaz S.A.S</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 2.0.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Corporativo Sagaz</h2>
            </div>
        <!-- #END# Right Sidebar -->

      
       {{ $contenido }}
  
         <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('plugins/morrisjs/morris.js')}}"></script>

    <!-- ChartJs -->
    <script src="{{ asset('plugins/chartjs/Chart.bundle.js')}}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{ asset('plugins/flot-charts/jquery.flot.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.categories.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.time.js')}}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>





<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    
    
    
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>

    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable.js')}}"></script>



    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js')}}"></script>
    <script src="{{ asset('js/pages/index.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js')}}"></script>
    <script src="{{ asset('js/facturacion.js')}}"></script>

    <script type="text/javascript">

        function editcategoria(){
            var des=$("#pro option:selected").text();
            var val=$("#valorproducto").val();

            $("#edit_desc").val(des);
            $("#edit_valor").val(val);


}


    function posteditar(){
            var edit_desc=$("#edit_desc").val();
            var edit_valor=$("#edit_valor").val();
            var id = $("#pro").val();
            
        $.ajax({
            url : "http://localhost:8080/sagaz/public/financiero/editpro",
            type : "get",
            data : "id="+id+"&edit_valor="+edit_valor+"&edit_desc="+edit_desc,
            success:function(){
            var nombrep = $("#nombrep").val(); 
             nompro(nombrep);
             $("#editcategoria").modal("hide");


            }
        })


}
  function nompro(id){
        $.ajax({
            url : "http://localhost:8080/sagaz/public/financiero/vercategorias",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#pro").html(datos);

            }
        })


}


    function valorpro(id_pro){
        
        $.ajax({
            url : "http://localhost:8080/sagaz/public/financiero/valorpro",
            type : "get",
            data : "id="+id_pro,
            success:function(datos){
             
             $("#valorproducto").attr('value',datos);

            }
        })


}
    function darresponable(id_cliente){

        $.ajax({
            url : "http://localhost:8080/sagaz/public/financiero/darresponsable",
            type : "get",
            data : "area="+id_cliente,
            success:function(datos){
             
             $("#responsable").html(datos);

            }
        })


}

    function dararea(id_cliente){

        $.ajax({
            url : "http://localhost:8080/sagaz/public/financiero/dararea",
            type : "get",
            data : "id="+id_cliente,
            success:function(datos){
             
             $("#area").html(datos);

            }
        })


}


  function ver(opc){
    if(opc == "1"){
        $("#facid").attr("target","_blank");
    }else if(opc == "2"){
        $("#facid").attr("target","");
    }
        
}
function formapago(forma){
 if(forma == "Credito"){
        $("#forma").html("<label>Plazo(dias) :</label><select class='form-control' name='plazo' onchange='darfecha(this.value)'><option>Elija Opcion</option><option>30</option><option>60</option><option>90</option></select");

    }else{
         $("#forma").html("");
    }
        
}

function darfecha(tiempo){
 
  var fecha = ("#fec_expedicion").val();
        dia = fecha.getDate(),
        mes = fecha.getMonth() + 1,
        anio = fecha.getFullYear(),
        addTime = tiempo * 86400;
        fecha.setSeconds(addTime);
        $("#venci").val(fecha);
}
   function quitar(id){
    
    var indice = id - 1;
    var cate = $("#cate"+id).val();
    var canti = $("#cate"+id).val();
    var precio = $("#precio"+id).val();
    var desc = $("#desc"+id).val();
    var des_valor = $("#des_valor"+id).val();
    var netpro = $("#netpro"+id).val();

    var totalbruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var totaliva = $("#totaliva"+id).val();
    var desc = $("#totalfactura"+id).val();
    
    var a = totalbruto - netpro;
    var b = descuentot - des_valor;
    var iva = a * 0.19;
    var c = a + iva;
    
            $.ajax({
            url : "http://localhost:8080/sagaz/public/financiero/quitarproducto",
            type : "get",
            data : "a="+a+"&b="+b+"&c="+c+"&iva="+iva,
            success:function(datos){
             
             $("#calculos").html(datos);
             $("#fila"+id).remove();
            }
        })

}
    </script>
</body>

</html>
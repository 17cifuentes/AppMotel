<!DOCTYPE html>
<html>

<head>
<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>{{ $titulo  or "Sagaz"}}</title>
<link rel="shortcut icon" href="{{ asset('images/iconos/sagaz.png') }}">

<link rel="stylesheet" href="https://unpkg.com/flatpickr/dist/flatpickr.min.css">
<script src="https://unpkg.com/flatpickr"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('calendario/flatpickr.min.css') }}">
<script type="text/javascript" src="{{ asset('calendario/flatpickr.js') }}"></script>
Usage


    <!-- Favicon-->
            <link href='http://fonts.googleapis.com/css?family=Raleway:400,800,500,600' rel='stylesheet' type='text/css'>
        <!--<link rel="stylesheet" type="text/css" href="{{ asset('animacion/css/normalize.css')}}" />-->
        <link rel="stylesheet" type="text/css" href="{{ asset('like1/css/demo.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('animacion/css/set2.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('animacion/fonts/font-awesome-4.2.0/css/font-awesome.min.css')}}" />

<link href="{{ asset('like1/css/style.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/style.css')}}" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('plugins/morrisjs/morris.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('css/animacion.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('css/themes/all-themes.css')}}" rel="stylesheet" />

            <link href='http://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
       
        <link rel="stylesheet" type="text/css" href="{{ asset('animacion/css/demo.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('animacion/css/set1.css')}}" />
        <link rel="shortcut icon" href="{{ asset('images/iconos/sagaz.png') }}">
        <script type="text/javascript">
jQuery(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
        'Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['es']);
});    

        $(document).ready(function() {
           $("#datepicker").datepicker();
        });
    </script>
</head>

<body class="theme-red" style='background:url("../images/Fondos/fondo.png");'>
    <!-- #Top Bar -->
    <section>
           <nav class="navbar">
        <div class="container-fluid" id="cont">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#"><strong>{{ Auth::user()->rol }}</strong>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
          
  
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                  <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                                      <li style="margin-left:-15%">{{ $volver  }}</li>
                                      <li style="margin-top:6%;margin-left:6%">
                                          <div class="dropdown" >
  <a class="btn btn-danger dropdown-toggle"  data-toggle="dropdown">{{ Auth::user()->name }}
  <span  class="caret"></span></a><br><br><br>
  <ul  class="dropdown-menu" >
    <li ><a href="{{ url('usuarios/perfil') }}">Perfil</a></li>
    
                                           <li>                                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-danger">
                                         Salir <img style="margin-left:1em" src="{{ asset('images/iconos/off.png') }}"> 
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
  </ul>
</div>
                                      </li>


            </div>
        </div>
    </nav>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
    
    
        <div class="container-fluid" >

        <!-- #END# Right Sidebar -->

      
       {{ $contenido }}
  
         <!-- Jquery Core Js -->

                 <script>
            // For Demo purposes only (show hover effect on mobile devices)
            [].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
                el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
            } );
        </script>


    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>        
    <script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('plugins/morrisjs/morris.js')}}"></script>

    <!-- ChartJs -->
    <script src="{{ asset('plugins/chartjs/Chart.bundle.js')}}"></script>

    <!-- Flot Charts Plugin Js -->
        <script src="{{ asset('plugins/node-waves/waves.js')}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{ asset('plugins/morrisjs/morris.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js')}}"></script>
    <script src="{{ asset('js/pages/charts/morris.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js')}}"></script>

    <script src="{{ asset('plugins/flot-charts/jquery.flot.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.categories.js')}}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.time.js')}}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>





<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    
    
    
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>

    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable.js')}}"></script>

<script src="https://code.highcharts.com/highcharts.src.js"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js')}}"></script>
    <script src="{{ asset('js/pages/index.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js')}}"></script>
    <script src="{{ asset('js/facturacion.js')}}"></script>
    
<script type="text/javascript" src="{{ asset('table/html2canvas.js')}}"></script>
<script type="text/javascript" src="{{ asset('table/jspdf/libs/sprintf.js')}}"></script>
<script type="text/javascript" src="{{ asset('table/jspdf/jspdf.js')}}"></script>
<script type="text/javascript" src="{{ asset('table/jspdf/libs/base64.js')}}"></script>
    <script type="text/javascript">
    function correo(){
    alert("cococ");
				var nombre = $("#nombre").val();
				var correo = $("#correo").val();
				var mensaje = $("#mensaje").val();
				$.ajax({
            		url : "portal/contacto",
            		type : "get",
            		data : "nombre="+nombre+"&correo="+correo+"&mensaje="+mensaje,
            		success:function(){
 					$("#alert").html('<div class="alert alert-success alert-dismissable">\
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\
  <strong>Notificación!</strong> Mensaje enviado exitosamente, pronto nos pondremos en contacto.\
</div>');
 					$("#form")[0].reset();
            	}
        		});
			}

    $(function(){
        $( "#fecha" ).datepicker();
    })


        function correo(inv){
            $("#inv").attr("value",inv);
            $("#correo").modal("show");
        }

        function daraporte(inv){
            $("#id_aporte").attr("value",inv);
            $("#aporte").modal("show");
        }
        function editcategoria(){
            var des=$("#pro option:selected").text();
            var val=$("#valorproducto").val();

            $("#edit_desc").val(des);
            $("#edit_valor").val(val);


}


    function posteditar(){
            var edit_desc=$("#edit_desc").val();
            var edit_valor=$("#edit_valor").val();
            var id = $("#pro").val();
            
        $.ajax({
            url : "financiero/editpro",
            type : "get",
            data : "id="+id+"&edit_valor="+edit_valor+"&edit_desc="+edit_desc,
            success:function(){
            var nombrep = $("#nombrep").val(); 
             nompro(nombrep);
             $("#editcategoria").modal("hide");


            }
        })


}
  function nompro(id){
        $.ajax({
            url : "financiero/vercategorias",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#pro").html(datos);

            }
        })


}


    function valorpro(id_pro){
        
        $.ajax({
            url : "financiero/valorpro",
            type : "get",
            data : "id="+id_pro,
            success:function(datos){
             
             $("#valorproducto").attr('value',datos);

            }
        })


}
    function darresponable(id_cliente){

        $.ajax({
            url : "financiero/darresponsable",
            type : "get",
            data : "area="+id_cliente,
            success:function(datos){
             
             $("#responsable").html(datos);

            }
        })


}
    function darelemento(id){

        $.ajax({
            url : "inventario/darelemento",
            type : "get",
            data : "id="+id,
            success:function(datos){
             
             $("#elemento").html(datos);

            }
        })


}

    function dararea(id_cliente){

        $.ajax({
            url : "financiero/dararea",
            type : "get",
            data : "id="+id_cliente,
            success:function(datos){
             
             $("#area").html(datos);

            }
        })


}


  function ver(opc){
    if(opc == "1"){
        $("#facid").attr("target","_blank");
    }else if(opc == "2"){
        $("#facid").attr("target","");
    }
        
}
function formapago(forma){
 if(forma == "Credito"){
        $("#forma").html("<label>Plazo(dias) :</label><select class='form-control' name='plazo' onchange='darfecha(this.value)'><option>Elija Opcion</option><option>30</option><option>60</option><option>90</option></select");

    }else{
         $("#forma").html("");
    }
        
}

function darfecha(tiempo){
 
  var fecha = ("#fec_expedicion").val();
        dia = fecha.getDate(),
        mes = fecha.getMonth() + 1,
        anio = fecha.getFullYear(),
        addTime = tiempo * 86400;
        fecha.setSeconds(addTime);
        $("#venci").val(fecha);
}
   function quitar(id){
    
    var indice = id - 1;
    var cate = $("#cate"+id).val();
    var canti = $("#cate"+id).val();
    var precio = $("#precio"+id).val();
    var desc = $("#desc"+id).val();
    var des_valor = $("#des_valor"+id).val();
    var netpro = $("#netpro"+id).val();

    var totalbruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var totaliva = $("#totaliva"+id).val();
    var desc = $("#totalfactura"+id).val();
    
    var a = totalbruto - netpro;
    var b = descuentot - des_valor;
    var iva = a * 0.19;
    var c = a + iva;
    
            $.ajax({
            url : "financiero/quitarproducto",
            type : "get",
            data : "a="+a+"&b="+b+"&c="+c+"&iva="+iva,
            success:function(datos){
             
             $("#calculos").html(datos);
             $("#fila"+id).remove();
            }
        })

}


var conpro = $("#guia").val();
    

function addproducto(){

   var producto = $("#nombrep").val();
    var procategoria = $("#pro").val();
    var cantpro = $("#cantproducto").val();
 conpro = parseInt(conpro) + 1; 
  
    var descuento = $("#descuento").val();
    var bruto = $("#bruto").val();
    var descuentot = $("#descuentot").val();
    var ivacal = $("#iva").val();
    var totalneto = $("#totalneto").val();
    var valor = $("#valorproducto").val();


      $.ajax({
            url : "financiero/addproducto",
            type : "get",
            data : "producto="+producto+"&conpro="+conpro+"&categoria="+procategoria+"&cantidad="+cantpro+"&descuento="+descuento+"&bruto="+bruto+"&totalneto="+totalneto,
            success:function(datos){
             
             $("#items").append(datos);
              var descuento = $("#descuento").val();
              var procategoria = $("#pro").val();
              var neto = $("#neto").val();
              var descuentot = $("#descuentot").val();
              var cantpro = $("#cantproducto").val();

               totales(descuento,procategoria,bruto,descuentot,ivacal,neto,cantpro);

            }
        })
  
}
function totales(descuento,procategoria,bruto,descuentot,ivacal,neto,cantpro){
            $.ajax({
            url : "financiero/totales",
            type : "get",
            data : "cantpro="+cantpro+"&netoca="+neto+"&descuento="+descuento+"&categoria="+procategoria+"&bruto="+bruto+"&descuentot="+descuentot+"&ivacal="+ivacal,
            success:function(datos){
             $("#calculos").html(datos);
             
            }
        })
}



function ver(foto){
    $('#foto').html("Los cambios se guardaran al dar en el boton actualizar pefil");
}

    </script>
    <script type="text/javascript">
    $(function(){
        $(".calendario").flatpickr('altFormat');
    })
        
    </script>
    <script type="text/javascript">
function exportar(){
    alert("ASA");
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#customers').html()));
        e.preventDefault();
    
}

        <script type="text/javascript">
        $(function () {
    $('.js-basic-example').DataTable();

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});

        </script>

    </script>

</body>

</html>
@extends('layouts.head')
@section('content')
<br>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<div style="margin:1em"><br>
<div class="w3-card-4 col-lg-2" style="margin:1em;padding-bottom: 1.3em">
  <img src="{{ asset('images/iconos/sag.png') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center"><br><br>
    <a class="btn btn-success" href="http://www.sagazsas.com.co" target="_blank">Ir a la web</a>
  </div>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/genetest.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://sagazsas.com.co/home/login" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/tq.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://sagazsas.com.co/home/login/tecnoquimicas" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/buenaventura.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://alcaldia.sagazsas.com.co/" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/Akemi.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://akemi.com.co/" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/Akari.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://akari.com.co" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/HotelCordillera.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://www.hotelcordillera.com" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/bambu.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://spabambu.com.co" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/Aceros.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://a-av.co/" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/Exclusive.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://exclusivefe.com/" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/ColegioAlain.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://colegioalainvalencia.com/" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em">
  <img src="{{ asset('img/clientes/DivinaProv.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center">
    <a class="btn btn-success" href="http://liceomodernodivinaprovidencia.edu.co/" target="_blank">Ir a la web</a>
  </div><br>
</div>
<div class="w3-card-4 col-lg-2" style="margin:1em"><br>
  <img src="{{ asset('img/clientes/mallonline.jpg') }}" width="80%" width="80%" alt="Norway"><br><br>
  <div class="w3-container w3-center"><br>
    <a class="btn btn-success" href="http://mallonline.com.co/presta/es/" target="_blank">Ir a la web</a>
  </div><br>
</div>
</div>
@endsection
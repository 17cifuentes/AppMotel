 <script type="text/javascript">
    function valoresdos(ruta,contenedor,grafico){
        $.ajax({
            url : "http://localhost/sagaz/public/"+ruta,
            type : "get",
            success:function(datos){          
                facturas=eval(datos);
                enero = facturas[0];
                julio = facturas[1];
                diciembre = facturas[2];
                var ctx = document.getElementById(contenedor);
                var data = {
                    datasets: [{
                        data: [enero,julio,diciembre],
                        backgroundColor: ["#FF6384","#4BC0C0","#FFCE56","#E7E9ED","#36A2EB"],
                        label: 'Graficos Estadisticos' // for legend
                    }],
                labels: ["Enero","julio","Diciembre"],};
                new Chart(ctx, {
                    data: data,
                    type: grafico
                });
            }
        })
    }
    function otros(){
        valoresdos('graficos/valores','grafico','line');
        valoresdos('graficos/valores','grafico2','bar');
        valoresdos('graficos/valores','grafico3','horizontalBar');
        valoresdos('graficos/valores','grafico4','radar');
        valoresdos('graficos/valores','grafico5','polarArea');
        valoresdos('graficos/valores','grafico6','pie');
        valoresdos('graficos/valores','grafico7','doughnut');
        valoresdos('graficos/valores','grafico8','bar');
        valoresdos('graficos/valores','grafico9','line');
        valoresdos('graficos/valores','grafico10','pie');
    }
</script>   
<?php

namespace Sagaz\Http\Controllers;

use Illuminate\Http\Request;
use Sagaz\Cliente;
use Sagaz\Diseno;
use DB;
class DisenoController extends Controller
{
    public function __construct(){
    	$this->middleware("auth");
    	$this->middleware("diseno");
    }

    public function principal(){
    	//return view("Usuarios.Diseno.disenop");
        return view("dash");
    }
    public function listar(){
         $disenos = DB::table('bod_disenos')
        ->join('clientes', 'bod_disenos.id_cliente', '=', 'clientes.id_cliente')
		->select('clientes.*', 'bod_disenos.*')
		->paginate(5);        
    	return view("Usuarios.Diseno.listar",compact("disenos"));
    }
    public function cargar(){
        $clientes = Cliente::where("id_cliente","<>","1")->get();
    	return view("Usuarios.Diseno.cargar",compact("clientes"));
    }
    public function eliminar(){
        extract($_GET);
        $diseno = Diseno::where("diseño_id","=",$id)->delete();
        return redirect("diseno/listar")->with("correcto","Diseño Eliminado Correctamente ");
    }    
    public function postcargar(){
    	
        extract($_POST);
        if (isset($_FILES['diseno'])){
        	$file = $_FILES["diseno"];
            if($file["name"] == null){
                $carpeta = "images/disenos/";
                $src = $carpeta . "user.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/disenos/";
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
        $diseño = new Diseno;
        $diseño->id_cliente=$cliente;
        $diseño->diseño_nombre=$nombre;
        $diseño->diseño_des=$descripcion;
        $diseño->diseño_img=$src;
        if($diseño->save() == true){
            return redirect("diseno/cargar")->with("correcto","diseño Registrado Correctamente");    
        }else{
            return redirect("diseno/cargar")->with("incorrecto","Datos de diseño incorrectos verifiquelos");
        };

}
    }

}

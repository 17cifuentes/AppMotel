<?php 
namespace Sagaz\Http\Controllers;                   
 use Illuminate\Http\Request;
 use Sagaz\Cotizacion;
 use Sagaz\CotizacionDetalle;
 use Sagaz\Cliente;
 use Sagaz\Responsable;
 use Sagaz\Producto;
 use Sagaz\Area;
 use Sagaz\FacturaSagaz;
 use Sagaz\CategoriaProducto;
 use Sagaz\Facturadestalle;
 use PDF;
 use DB;
 use NumeroALetras;
 use Sagaz\Http\Controllers\FuncionesDBController;
 use Sagaz\Http\Controllers\FuncionesFacturaController;
class FinancieroController extends Controller{
     public function __construct()
     {
          $this->middleware('auth');
          $this->middleware('financiero', ['except' => ['store','dararea', 'darresponsable','listar','addproducto','totales']]);
     }
     /*FUNCION PARA RETORNAR VISTA DE CORPO*/
     public function principal()
     {
          //return view("Usuarios.Financiero.financieroF");
          return view("dash");
     }
          /*FUNCION PARA MOSTRAR VISTA DE FACTURACION*/
     public function index()
     {
          $clientes = FuncionesDBController::consultarRegistro("clientes","estado","Activo");
          $responsables = FuncionesDBController::consultarRegistrosTotales("responsables");
          $productos = FuncionesDBController::consultarRegistro("productos","pro_estado","Activo");
          $id_factura = FuncionesDBController::consultarRegistroMaximo("facturas_sagaz","id_factura");
          if(empty($id_factura))
          { 
               $id_factura=7150;
          }
          return view("Usuarios.Financiero.indexFacturacion",compact("clientes","responsables","productos","id_factura"));
     }
     /*METODO QUE VIENE POR AJAX PARA AGRAGAR UN PRODUCTO A LA FACTURA*/
     public function addproducto()
     {
          extract($_GET);
          $product = FuncionesDBController::consultarRegistro("productos","id_producto",$producto);
          $cate = FuncionesDBController::consultarRegistro("categoria_productos","id_cat",$categoria);
          $proxcan = $cate[0]->cat_precio_cliente * $cantidad;
          $descuentovalor = FuncionesFacturaController::GenerarDescuento($proxcan,$descuento);
          $neto = FuncionesFacturaController::GenerarNeto($proxcan,$descuentovalor);
          //conteo de filas
          if($conpro == ""){$conpro = 1;}else{$conpro++;}
          // funcion para organizar columnas y su indicativo
          $additem = FuncionesFacturaController::AddItem($conpro,$product[0]->nombre_producto,$cate[0]->cat_descripcion,$cantidad,$cate[0]->cat_precio_cliente,$descuento,$descuentovalor,$neto,$product[0]->id_producto,$cate[0]->id_cat);
          // imprimir fila nueva
          echo $additem;
     }
     //se generan los totales de la factura con su contabilidad en tiempo real             
     function totales()
     {
          extract($_GET);
          $cate = FuncionesDBController::consultarRegistro("categoria_productos","id_cat",$categoria);
          $proxcan = (int)$cantpro *  $cate[0]->cat_precio_cliente;
          $descuentovalor = FuncionesFacturaController::GenerarDescuento($proxcan,$descuento);
          $neto = FuncionesFacturaController::GenerarNeto($proxcan,$descuentovalor);
          $totalbruto = FuncionesFacturaController::GenerarTotalBruto($neto,$bruto);
          $descuentotal = FuncionesFacturaController::GenerarDescuentoTotal($descuentot,$descuentovalor);
          $iva = FuncionesFacturaController::GenerarIVA($totalbruto,"0.19");
          $totalfactura = FuncionesFacturaController::GenerarTotalFactura($totalbruto,$iva);
          $addtotales = FuncionesFacturaController::AddTotales($totalbruto,$descuentotal,$iva,$totalfactura);

          echo $addtotales; 
     }          
     
     public function store(Request $request)
     {
          extract($_POST);            
          if($fec_expedicion == "")
          {
               $fec_expedicion = date('Y-m-j');
               $nuevafecha = strtotime ( '+'.$plazo.' day' , strtotime ( $fec_expedicion ) ) ;
               $fec_vencimiento = date ( 'Y-m-j' , $nuevafecha );
          }
          if($fec_vencimiento == "")
          {
               $nuevafecha = strtotime ( '+'.$plazo.' day' , strtotime ( $fec_expedicion ) ) ;
               $fec_vencimiento = date ( 'Y-m-j' , $nuevafecha );
          }
          if(!isset($plazo)){$plazo = 0;}
          if($orden !=""){$estado = "Pendiente-R";}else{$estado ="Pendiente";}
          /*seguardan los datos de la cotizacion*/
          if($opc == "3")
          {
               $coti = new Cotizacion;
               $coti->coti_expedicion = $fec_expedicion;
               $coti->coti_vencimiento = $fec_vencimiento;
               $coti->forma_pago = $forma_pago;
               $coti->plazo = $plazo;
               $coti->iva = $totaliva;
               $coti->cliente_id = $cliente;
               $coti->empleado_id =1;
               $coti->responsable_id = $responsable;
               $coti->coti_descuento = $totaldes;
               $coti->estado = $estado;
               $coti->orden = $orden;
               $coti->bruto = $totalbruto;
               $coti->total = $totalfactura;
               $coti->area_id = $area;
               $coti->save();
               /*se guardan los codigos de factura por producto*/
               $id_factura =Cotizacion::max("id_coti");
               $con = COUNT($netopro);
               for($x=0; $x < $con ;$x++)
               {
                    $proCoti = new CotizacionDetalle;
                    $proCoti->coti_id=$id_factura;
                    $proCoti->cat_id=$categoriapro[$x];
                    $proCoti->producto=$producto[$x];
                    $proCoti->neto=$netopro[$x];
                    $proCoti->cantidad=$cantidadpro[$x];
                    $proCoti->precio_cliente=$precio_cliente[$x];
                    $proCoti->descuento=$descuentopro[$x];
                    $proCoti->valor_des=$desvalorpro[$x];
                    $proCoti->save();
               }
               return redirect("cotizacion/listar")->with("correcto","Se Registro Correctamente La cotizacion: COT-".$id_factura);
               }else if($opc == "2")
               {
                    $factura = new  FacturaSagaz;
                    $factura->fec_expedicion = $fec_expedicion;
                    $factura->fec_vencimiento = $fec_vencimiento;
                    $factura->forma_pago = $forma_pago;
                    $factura->plazo = $plazo;
                    $factura->iva = $totaliva;
                    $factura->cliente_id = $cliente;
                    $factura->empleado_id =1;
                    $factura->responsable_id = $responsable;
                    $factura->fac_descuento = $totaldes;
                    $factura->estado = $estado;
                    $factura->orden = $orden;
                    $factura->bruto = $totalbruto;
                    $factura->total = $totalfactura;
                    $factura->area_id = $area;
                    $factura->save();
                    //se guardan los codigos de factura por producto
                    $id_factura =FacturaSagaz::max("id_factura");
                    $con = COUNT($netopro);
                    //dd($categoriapro);
                    for($x=0; $x < $con ;$x++)
                    {
                         $proFactura = new Facturadestalle;
                         $proFactura->factura_id=$id_factura;
                         $proFactura->cat_id=$categoriapro[$x];
                         $proFactura->producto=$producto[$x];
                         $proFactura->neto=$netopro[$x];
                         $proFactura->cantidad=$cantidadpro[$x];
                         $proFactura->precio_cliente=$precio_cliente[$x];
                         $proFactura->descuento=$descuentopro[$x];
                         $proFactura->valor_des=$desvalorpro[$x];
                         $proFactura->save();
                    }
                    return redirect('financiero/index')->with('correcto','Se Registro Correctamente ');
               }
          }
          /*LISTA DE OPCIONES DE FACTURA*/       
          public function listar()
          {
               $facturas = DB::table('facturas_sagaz')
               ->join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
               ->select('facturas_sagaz.*', 'clientes.nombre_cliente')
               ->orderBy('id_factura', 'DESC')
               ->paginate(8); 
               return view("Usuarios.Financiero.facturalista",compact("facturas"));
          }
          /*DESCARGA FACTURA*/
          public function pdf()
          {
               extract($_GET);
               $facturas = DB::table('facturas_sagaz')
               ->join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
               ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
               ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
               ->join('productos_factura', 'facturas_sagaz.id_factura', '=', 'productos_factura.factura_id')
               ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
               ->join('productos', 'categoria_productos.producto_id', '=', 'productos.id_producto')
               ->select('area.*','facturas_sagaz.*', 'clientes.*','responsables.*','productos_factura.*','categoria_productos.*','productos.*')
               ->where('id_factura','=',$factura)
               ->get();
               $letras = NumeroALetras::convertir($facturas[0]->total);
                    
               return PDF::loadView('Usuarios.Financiero.sontener',compact("facturas","letras"))->stream('factura'.$factura.'.pdf');
          }
           /*MUSTRA Y CARGA LA VISTA DE EDITAR FACTURA*/
          public function editar()
          {
               extract($_GET);
               $clientes = Cliente::where("estado","=","Activo")->get();
               $responsables = Responsable::all();
               $productos = Producto::where("pro_estado","=","Activo")->get();
               $id_factura=FacturaSagaz::max('id_factura');
               if(empty($id_factura))
               {
                    $id_factura=7164;
               }
               $facturas = DB::table('facturas_sagaz')
               ->join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
               ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
               ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
               ->join('productos_factura', 'facturas_sagaz.id_factura', '=', 'productos_factura.factura_id')
               ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
               ->join('productos', 'categoria_productos.producto_id', '=', 'productos.id_producto')
               ->select('facturas_sagaz.*','area.*', 'clientes.*','responsables.*','productos_factura.*','categoria_productos.*','productos.*')
               ->where('id_factura','=',$factura)
               ->get();
                return view("Usuarios.Financiero.editarFacturacion",compact("facturas","clientes","responsables","productos","id_factura"));
          }
          /*SE GUARDAN LOS CAMBIOS HECHOS AL EDITAR LA FACTURA*/
          public function posteditar()
          {
               extract($_POST);
               //dd($_POST);
               if($cliente == ""){$cliente = $clientefijo;}
               if($responsable == ""){$responsable = $responsablefijo;}
               if($forma_pago == ""){$forma_pago = $formafijo;}
               if(!isset($plazo)){$plazo="";}
               if(!isset($totaldes)){$descuento=$descuentot;}else{$descuento=$totaldes;}
               if(!isset($totalfactura)){$total=$totalfact;}else{$total=$totalfactura;}
               if($orden !=""){$estado = "Pendiente-R";}else{$estado = "Pendiente";}
               if($fec_expedicion == ""){
                    $fec_expedicion = date('Y-m-j');
                    $nuevafecha = strtotime ( '+'.$plazo.' day' , strtotime ( $fec_expedicion ) ) ;
                    $fec_vencimiento = date ( 'Y-m-j' , $nuevafecha );
               }
               DB::table('facturas_sagaz')
               ->where('id_factura', $fac_id)
               ->update(['fec_expedicion' =>$fec_expedicion,'fec_vencimiento'=>$fec_vencimiento,'forma_pago'=>$forma_pago,
               'plazo'=>$plazo,'iva'=>$totaliva,'cliente_id'=>$cliente,'empleado_id'=>1,'responsable_id'=>$responsable,
               'fac_descuento'=>$descuento,'estado'=>$estado,'orden'=>$orden,'bruto'=>$totalbruto,'total'=>$total]);
               DB::table('productos_factura')->where('factura_id', '=',$fac_id)->delete();
               $con = COUNT($producto);
                    
               for($x=0; $x < $con ;$x++)
               {
                    $proFactura = new Facturadestalle;
                    $proFactura->factura_id=$fac_id;
                    $proFactura->cat_id=$categoriapro[$x];
                    $proFactura->producto=$producto[$x];
                    $proFactura->neto=$netopro[$x];
                    $proFactura->cantidad=$cantidadpro[$x];
                    $proFactura->precio_cliente=$precio_cliente[$x];
                    $proFactura->descuento=$descuentopro[$x];
                    $proFactura->valor_des=$desvalorpro[$x];
                    $proFactura->save();
               }
               return redirect("financiero/listar")->with("correcto","Se Edito Correctamente La Factura: SAG-".$fac_id);
          }
          /*CAMBIO DE ESTADO A FINALIZADO*/       
          public function finalizar()
          {
               extract($_GET);
               $fac=FacturaSagaz::where("id_factura","=",$factura)->get();
               if($fac[0]->estado == "Finalizado")
               {
                    $session="incorrecto";
                    $nota="La Factura: SAG-".$factura." Ya ah sido Finalizada.";
               }else
               {
                    DB::table('facturas_sagaz')
                    ->where('id_factura', $factura)
                    ->update(['estado' => 'Finalizado']);
                    $session="correcto";
                    $nota="La Factura: SAG-".$factura." Se Finalizo Correctamente";
               }
               return redirect("financiero/listar")->with($session,$nota);
          }
          /*CAMBIO DE ESTADO A ANULADA*/                 
          public function anular()
          { 
               extract($_GET);
               $fac=FacturaSagaz::where("id_factura","=",$factura)->get();
               if($fac[0]->estado == "Anulada")
               {
                    $session="incorrecto";
                    $nota="La Factura: SAG-".$factura." Ya ah sido Anulada.";
               }else
               {
                    DB::table('facturas_sagaz')
                    ->where('id_factura', $factura)
                    ->update(['estado' => 'Anulada']);
                    $session="correcto";
                    $nota="La Factura: SAG-".$factura." Se Anulo Correctamente";
                    
               }
               return redirect("financiero/listar")->with($session,$nota);
          }
          /*ELIMINA UN PRODUCTO DE LA FACTURA EN TIEMPO REAL*/                 
          function quitarproducto()
          {
               extract($_GET);
               echo '
               <tr>
                    <td><strong>Total Bruto :</strong></td>
                    <input type="hidden" value="'.$a.'" id="bruto" name="totalbruto">
                    <td >$'.number_format($a).'</td>
               </tr>
               <tr>
                    <td><strong>Total Descuento :</strong></td>
                    <input type="hidden" value="'.$b.'" id="descuentot" name="totaldes">
                    <td id="totalDto">$'.number_format($b).'</td>
               </tr>
               <tr>
                    <td><strong>IVA :</strong></td>
                    <input type="hidden" value="'.$iva.'" id="totaliva" name="totaliva">
                    <td id="totalIva">$ '.number_format($iva).'</td>
               </tr>
               <tr>
                    <td><strong>Retefuente :</strong></td>
                    <td>$0</td>
               </tr>
               <tr>
                    <td><strong>TOTAL :</strong></td>
                    <input type="hidden" value="'.$c.'" id="totalfactura" name="totalfactura">
                    <td id="totalFinal">$ '.number_format($c).'</td>
               </tr>';
          }
          /*FILTRO DEL CLIENTE A AREA*/                 
          public function dararea()
          {
               extract($_GET);
               $areas = Area::where('cliente_id','=',$id)
               ->get();
               echo '<option value="">seleccione</option>';
               foreach($areas as $area)
               {
                    echo '<option value="'.$area->id_area.'">'.$area->area_des.'</option>';
               }
          }
          /*FILTRO DEL AREA A RESPONSABLE*/                 
          public function darresponsable()
          {
               extract($_GET);
               $responsables = Responsable::where('area_id','=',$area)
               ->where("res_estado","=","Activo")
               ->get();
               echo '<option value="">seleccione</option>';
               foreach($responsables as $responsable)
               {
                    echo '<option value="'.$responsable->id_responsable.'">'.$responsable->contacto.'</option>';
               }
          }
          /*CAMBIO DE ESTADO ESPECIFICOS DE REVISION O PENDIENTE*/                 
          public function cambioestados()
          {
               extract($_GET);
               switch ($estado)
               {
                    case 'Pendiente-R':
                         $est = "Pendiente-P";
                    break;
                    case 'Pendiente-P':
                         $est = "Finalizado";
                    break;
               }
               DB::table('facturas_sagaz')
               ->where('id_factura', $factura)
               ->update(['estado' =>$est]);
               $session="correcto";
               $nota="La Factura: SAG-".$factura." Se Actualizo Correctamente";
               return redirect("financiero/listar")->with($session,$nota);
          }
     }

<?php

namespace Sagaz\Http\Controllers;

use Illuminate\Http\Request;

class GerenteController extends Controller
{
	    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('gerente');
    }



    public function principal(){
    	//return view("Usuarios.Gerente.gerentep");
    	return view("dash");
    }
}

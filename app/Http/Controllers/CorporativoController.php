<?php
namespace Sagaz\Http\Controllers;
use Illuminate\Http\Request;
use Sagaz\FacturaSagaz;
use Sagaz\Users;
class CorporativoController extends Controller
{
    public function index(){return view('viewcorporativo');}
    public function grafico1(){
        extract($_GET);
        $m1 = FacturaSagaz::where("fec_expedicion","like","%2017-".$mes1."%")->get()->max("total");
        $m2 = FacturaSagaz::where("fec_expedicion","like","%2017-".$mes2."%")->get()->max("total");
        $m3 = FacturaSagaz::where("fec_expedicion","like","%2017-".$mes3."%")->get()->max("total");
        $dat=array(0=>round($m1),1=>round($m2),2=>round($m3));
        echo json_encode($dat);
    }
}

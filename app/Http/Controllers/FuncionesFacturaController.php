<?php

namespace Sagaz\Http\Controllers;

use Illuminate\Http\Request;

class FuncionesFacturaController extends Controller
{
	/*
	Funcion para sacar el neto de una de un item de una factura donde se pasa el valor, su cantidad y si se aplico un descuento, se multiplica el valor por la cantidad y se le resta el descuento.
	*/
    public static function GenerarNeto($valor,$descuento)
    {
    	$neto = $valor - $descuento;
    	return $neto;
    }
    /*
	Funcion apara aplicar descuento a un valor, el parametro de descuento  debe ser un nuemero entero del 1 al 100 indicanto el porcentaje de descuento.
    */
    public static function GenerarDescuento($valor,$descuento)
    {
    	$descuento = $valor*$descuento/100;
    	return $descuento;
    }  
    public static function AddItem($nfila,$producto,$descripcion,$cantidad,$valor,$descuento,$descuentovalor,$neto,$id_producto,$id_cat)
    {
    	$item ='
	    	<tr id="fila'.$nfila.'">
	            <td><a onclick="quitar('.$nfila.')"  class="btn btn-danger">Quitar Producto</a></td>
	            <td>'.$producto.'</td>
	            <td>'.$descripcion.'</td>
	            <td>'.$cantidad.'</td>
	            <td>$ '.number_format($valor).'</td>
	            <td>'.$descuento.'%</td>
	            <td>$ '.number_format($descuentovalor).'</td>
	            <td>$ '.number_format($neto).'</td>        
	            <input type="hidden" value="'.$neto.'" id="neto" name="neto[]">
	            <input type="hidden" name="producto[]" value="'.$id_producto.'">
				<input type="hidden" id="cate'.$nfila.'" name="categoriapro[]" value="'.$id_cat.'">
	            <input type="hidden" id="canti'.$nfila.'" name="cantidadpro[]" value="'.$cantidad.'">
	            <input type="hidden" id="precio'.$nfila.'" name="precio_cliente[]" value="'.$valor.'">
	            <input type="hidden" id="desc'.$nfila.'"name="descuentopro[]" value="'.$descuento.'">
	            <input type="hidden" id="des_valor'.$nfila.'" name="desvalorpro[]" value="'.$descuentovalor.'">
	            <input type="hidden" id="netpro'.$nfila.'" name="netopro[]" value="'.$neto.'">
	        </tr>';
        return $item;
    }
    public static function GenerarTotalBruto($neto,$bruto)
    {
    	$totalbruto = $neto + $bruto;
    	return $totalbruto;
    }            
    public static function GenerarDescuentoTotal($descuento,$descuentot)
    {
    	$descuentototal = $descuento + $descuentot;
    	return $descuentototal;
    } 
    public static function GenerarIVA($totalbruto,$iva)
    {
    	$ivatotal = $totalbruto * $iva;
    	return $ivatotal;
    }   
    public static function GenerarTotalFactura($totalbruto,$iva)
    {
    	$ivatotal = $totalbruto + $iva;
    	return $ivatotal;
    }            
    public static function AddTotales($totalbruto,$descuentotal,$iva,$totalfactura)
    {
        $addtotales = '
        	<tr>
                <td><strong>Total Bruto :</strong></td>
                 <td >$'.number_format($totalbruto).'</td>
               </tr>
               <tr>
                   <td><strong>Total Descuento :</strong></td>
                   <td id="totalDto">$'.number_format($descuentotal).'</td>
               </tr>
               <tr>
                   <td><strong>IVA :</strong></td>
                   <td id="totalIva">$ '.number_format($iva).'</td> 
               </tr>
               <tr>
                   <td><strong>Retefuente :</strong></td>
                   <td>$0</td>
               </tr>
               <tr>
                   <td><strong>TOTAL :</strong></td>
                   <td id="totalFinal">$ '.number_format($totalfactura).'</td> 
               </tr>
               <input type="hidden" value="'.$totalbruto.'" id="bruto" name="totalbruto">
               <input type="hidden" value="'.$totalfactura.'" id="totalfactura" name="totalfactura">
               <input type="hidden" value="'.$iva.'" id="iva" name="totaliva">
               <input type="hidden" value="'.$descuentotal.'" id="descuentot" name="totaldes">';

       	return $addtotales;
    }    
}

<?php
namespace Sagaz\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Sagaz\FacturaSagaz;
use Sagaz\FacturaDetalle;
use Sagaz\Cliente;
use Sagaz\CategoriaProducto;
use Sagaz\Producto;
use Sagaz\Almuerzosagaz;
use Sagaz\Area;
use Sagaz\Facturadestalle;
use Sagaz\Responsable;
use Sagaz\User;
use Sagaz\Otrosgastos;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Illuminate\Support\Collection as Collection;
class ReportesController extends Controller
{
    private function darreportealmuerzo($id){

        $sql = Almuerzosagaz::join('users', 'almuerzos.usu_id', '=', 'users.id')
        ->where('users.id','=',$id)
        ->get();
        $total = 0; 
        foreach ($sql as $can) {
            if($can->almuerzo == "Completo"){
         
                $total += 7500; 
            }else if($can->almuerzo == "Bandeja"){
                $total += 7000;
            }else if($can->almuerzo == "Sopa"){
                $total += 3500;
            }
        }
        
       return $total;

    }

public function filtros(){
    	$responsables = Responsable::where("res_estado","=","Activo")->get();
    	$clientes = Cliente::where("estado","=","Activo")->get();
        $areas = Area::all();
    	return view("Reportes.filtros",compact("responsables","clientes","areas"));
	}
public function generaralmu(){
    Excel::create("Resportes Alumerzos", function($excel) {
    extract($_POST);
    $excel->sheet('Reporte '.$inicio." a ".$fin , function($sheet) {
        extract($_POST);
        $almuerzos = Almuerzosagaz::join('users', 'almuerzos.usu_id', '=', 'users.id')
        ->select('users.name','almuerzos.created_at','almuerzos.almuerzo','almuerzos.observacion','almuerzos.valor')
        ->where("almuerzos.created_at",">=",$inicio)
        ->Orwhere("almuerzos.created_at","<=",$fin)
        ->get();
        
    $sheet->fromArray($almuerzos->ToArray());
    });
        //valance de almuerzos
        $excel->sheet('Valance '.$inicio." a ".$fin , function($sheet) {
        extract($_POST);
        //dd($fin);
        $sql ='SELECT COUNT(almuerzos.usu_id) as Almeurzos ,sum(almuerzos.valor) as "valortotal", users.name FROM `almuerzos` INNER JOIN users ON almuerzos.usu_id=users.id where almuerzos.updated_at >="'.$inicio.'" and almuerzos.updated_at <="'.$fin.'" GROUP BY(users.name)';
        $almuer = DB::select($sql);
        //dd($almuer);
        foreach($almuer as $almu) {
            $a[$almu->name]=$almu->name;
            $a[$almu->valortotal]=$almu->valortotal;
            
        }
        //dd($a);
       // dd($nombres);
        
    $sheet->fromArray($a);
    });        
    $excel->sheet('Otros Gastos', function($sheet) {
        $gastos = Otrosgastos::all();
        $total = 0;
        foreach($gastos as $gasto) {
            $total += $gasto->valor;
        }
        
    $sheet->fromArray($gastos);
    });        
})->export('xls');
}
  
    public function filtrosAlmuerzo(){
        return view("Reportes.reporteAlmuerzo");
    }
  
        public function filtroscom(){
        $responsables = Responsable::where("res_estado","=","Activo")->get();
        $clientes = Cliente::where("estado","=","Activo")->get();
        $areas = Area::all();
        return view("Reportes.filtroscom",compact("responsables","clientes","areas"));
    }
	public function generar(){
		extract($_POST);
		if($opc == "Reporte Filtrado Facturas"){
            if($cliente == "" and $responsable == "" and $estado == "" and $area  == ""){
				return redirect("reportes/filtros")->with("incorrecto","No selecciono ningun tipo de filtro");
			}else
            {
				Excel::create($opc, function($excel) {
            	$excel->sheet('Total_Facturas', function($sheet) 
                {
                    extract($_POST);
	                if($cliente != "" and $estado =="" and $responsable == "" and $area=="")
                    {
                        $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                        'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')->where("id_cliente","=",$cliente)->get();
            		}
	               if($cliente != "" and $estado != "" and $responsable == "" and $area == "")
                   {
                         $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                         'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')
                        ->where("facturas_sagaz.estado","=",$estado)
                    	->where("id_cliente","=",$cliente)->get();	
                	}
                    if($cliente != "" and $estado == "" and $responsable == "" and $area != "")
                    {
                        $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                         'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')
                        ->where("facturas_sagaz.cliente_id","=",$cliente)
                    	->where("facturas_sagaz.area_id","=",$area)
            		    ->get();	
                    }
            		if($cliente != "" and $estado == "" and $responsable != "" and $area !="")
                    {
                        $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                        'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')
                        ->where("facturas_sagaz.cliente_id","=",$cliente)
                		->where("facturas_sagaz.area_id","=",$area)
                		->where("facturas_sagaz.responsable_id","=",$responsable)
                		->get();	
                        
                	}
                    if($cliente == "" and $estado != "" and $responsable != "" and $area !="")
                    {
                        $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                         'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')
                        ->where("facturas_sagaz.area_id","=",$area)
                        ->where("facturas_sagaz.estado","=",$estado)
                        ->where("facturas_sagaz.responsable_id","=",$responsable)
                        ->get();    
                    }
            		if($cliente == "" and $estado !="" and $responsable == "" and $area == "")
                    {

                    	$products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                        'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')->where("facturas_sagaz.estado","=",$estado)->get();
            		}
               		if($cliente == "" and $estado =="" and $responsable != "")
                    {
            		  $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                        'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')->where("responsable_id","=",$responsable)->get();
            		}
                    
                    if($cliente == "" and $estado =="" and $responsable == "" and $area !="" )
                    {
                        $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                        'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')->where("area_id","=",$area)->get();
                    }
                    if($cliente != "" and $estado !="" and $responsable != "" and $area !="" )
                    {
                        $products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                        ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                        ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                        ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                        'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                        'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                        'facturas_sagaz.total','facturas_sagaz.estado')->where("area_id","=",$area)->get();
                    }                    
                    if(empty($products->ToArray()))
                    {
            			return redirect("reportes/filtros")->with("incorrecto","Ningun Reporte encontrado en la Base de datos");
            		}else
                    {
            		    $sheet->fromArray($products);	
            		}
                	
            	});
                /*segunda hoja -> hoja de ´productos*/
            $excel->sheet('Productos', function($sheet) 
            {
                extract($_POST);
                if($cliente != "" and $estado =="" and $responsable == "" and $area=="")
                {
                    $products =  Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where('cliente_id','=',$cliente)->get();
                }
                if($cliente != ""  and $responsable != "" and $area !="" and $estado =="")
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("facturas_sagaz.cliente_id","=",$cliente)
                    ->where("facturas_sagaz.area_id","=",$area)
                    ->where("facturas_sagaz.responsable_id","=",$responsable)
                    ->get();    
                 
                }
                 if($cliente != "" and $estado == ""  and $responsable =="" and $area !="")
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("cliente_id","=",$cliente)
                    ->where("area_id","=",$area)
                    ->get();    

                }                
                if($cliente != "" and $estado == ""  and $responsable !="" and $area =="")
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("cliente_id","=",$cliente)
                    ->where("responsable_id","=",$responsable)
                    ->get();    

                }
                if($cliente != "" and $estado != "" and $responsable != "" and  $area == "")
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("cliente_id","=",$cliente)
                    ->where("estado","=",$estado)
                    ->where("responsable_id","=",$responsable)
                    ->get();    
                }
                if($cliente == "" and $estado !="" and $responsable == "" and $area == "")
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento','productos_factura.valor_des',
                    'productos_factura.neto')
                    ->where("estado","=",$estado)->get();
                }
                if($cliente == ""  and $responsable != "")
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoxria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("responsable_id","=",$responsable)->get();
                }
                if($cliente == "" and $estado =="" and $responsable == "" and $area !="" )
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("facturas_sagaz.area_id","=",$area)->get();
                }
                if($cliente !="" and $estado !="" and $responsable == "" and $area =="" )
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("facturas_sagaz.estado","=",$estado)
                    ->where("facturas_sagaz.cliente_id","=",$cliente)->get();
                }
                if($cliente == "" and $estado !="" and $responsable !="" and $area !="" )
                {
                    $products = Facturadestalle::join('facturas_sagaz', 'productos_factura.factura_id', '=', 'facturas_sagaz.id_factura')
                    ->join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento',
                    'productos_factura.valor_des','productos_factura.neto')
                    ->where("responsable_id","=",$responsable) 
                    ->where("facturas_sagaz.estado","=",$estado)
                    ->where("facturas_sagaz.area_id","=",$area)->get();
                }
                if(empty($products->ToArray()))
                {
                    return redirect("reportes/filtros")->with("incorrecto","Ningun Reporte encontrado en la Base de datos");
                }else
                {
                    $sheet->fromArray($products);   
                }
                    
            });
        })->export('xls');
	}
    }

		if($opc == "Reporte Total Facturas"){

			Excel::create($opc, function($excel) {
           			$excel->sheet('Facturas', function($sheet) {
                	$products = FacturaSagaz::join('clientes', 'facturas_sagaz.cliente_id', '=', 'clientes.id_cliente')
                    ->join('responsables', 'facturas_sagaz.responsable_id', '=', 'responsables.id_responsable')
                    ->join('area', 'facturas_sagaz.area_id', '=', 'area.id_area')
                    ->select('facturas_sagaz.id_factura','facturas_sagaz.fec_expedicion','facturas_sagaz.fec_vencimiento',
                    'clientes.nombre_cliente','clientes.nit','facturas_sagaz.forma_pago','facturas_sagaz.plazo',
                    'responsables.contacto','area.area_des','facturas_sagaz.bruto','facturas_sagaz.fac_descuento','facturas_sagaz.iva',
                    'facturas_sagaz.total')->get();
               		$sheet->fromArray($products);
                });
                $excel->sheet('Productos', function($sheet) {
                    $products = Facturadestalle::join('categoria_productos', 'productos_factura.cat_id', '=', 'categoria_productos.id_cat')
                    ->select('productos_factura.factura_id','categoria_productos.cat_descripcion',
                    'productos_factura.cantidad','productos_factura.precio_cliente','productos_factura.descuento'
                    ,'productos_factura.valor_des','productos_factura.neto')->get();
                $sheet->fromArray($products);
                });
            })->export('xls');
		}
	}
}

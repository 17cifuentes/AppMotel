<?php

namespace Sagaz\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Sagaz\FacturaSagaz;
class GraficosController extends Controller
{
    public function crear(){
    	return view("Graficos.crear_grafico");
    }


    public function graficogerente(){
    	extract($_GET);
        //dd($_GET);
    $titulo1 ="";
    $titulo2 ="";

    if($item == "finalizada" or $item == "pendiente"){
            $dat = "total";
            $titulo2 = "Total Facturado";
            $titulo1 = "# Facturas";
            $campo_estado="estado";
            if($item == "finalizada"){
                $estado = "Finalizado";
            }else{
                $estado = "Pendiente";    
            }
            $item = "facturas_sagaz";            
         }else if($item == "users"){
            $dat = "id";
            $titulo1 = "Numero de Empleados";
            $titulo2 = "# Suma en id";
            $estado = "Activo";
            $campo_estado="user_estado";
         }

     if($tiempo == "trimestres"){
        if($subtiempo == "1"){
            $mes1 = "2017-01";
            $mes2 = "2017-02";
            $mes3 = "2017-03";
        }else if($subtiempo == "2"){
            $mes1 = "2017-04";
            $mes2 = "2017-05";
            $mes3 = "2017-06";
        }else if($subtiempo == "3"){
            $mes1 = "2017-07";
            $mes2 = "2017-08";
            $mes3 = "2017-09";
        }else if($subtiempo == "4"){
            $mes1 = "2017-10";
            $mes2 = "2017-11";
            $mes3 = "2017-12";
        }
        $sql = "SELECT COUNT(".$dat.") as numero,SUM(".$dat.") as total FROM ".$item." WHERE created_at LIKE '%".$mes1."%' or created_at LIKE '%".$mes2."%' or created_at LIKE '%".$mes3."%' and ".$campo_estado." ='".$estado."'";
        $dato = DB::select($sql);

    }else if( $tiempo == "años"){
        $sql = "SELECT COUNT(".$dat.") as numero,SUM(".$dat.") as total FROM ".$item." WHERE created_at LIKE '%".$subtiempo."%' and ".$campo_estado." ='".$estado."'";
        $dato = DB::select($sql);
        
    }else if( $tiempo == "meses"){
        $fecha = "%2017-".$subtiempo."%";
        $sql = "SELECT COUNT(".$dat.") as numero,SUM(".$dat.") as total FROM ".$item." WHERE created_at LIKE '".$fecha."' and ".$campo_estado." ='".$estado."'";
        $dato = DB::select($sql);
    }
        	

           
            $numero = $dato[0]->numero;
            $total = $dato[0]->total;  
                        if($item == "users"){
            
            $titulo1 = "Numero de Empleados";
            $titulo2 = "Dato no comtable";
            
            $total=0;
         }          
            $grafi=array(
            	0=>round($numero),
            	1=>round($total),
                2=>$titulo1,
                3=>$titulo2
            	);
           echo json_encode($grafi);
    }
}

<?php

namespace Sagaz\Http\Controllers;

use Illuminate\Http\Request;
use Sagaz\Elemento;
use Sagaz\Inventario;
use DB;
class InventarioController extends Controller
{
	public function index(){
      $cod = Inventario::all();
      $codigo = COUNT($cod);
    	return view("Inventario.crear_inventario",compact("codigo"));
    }
    public function cargarPlantilla(){
    	return view("Inventario.cargarInventario");
    }
    public function editar(){
    	extract($_GET);
      $elemento=Inventario::where('id','=',$id)->get()->ToArray();
      
      return view("Inventario.EditarElemento",compact("elemento"));
    }
    public function editarpost(){
    	extract($_POST);
      if($estado == ""){
    		$estado = $estadofijo;
    	}
      if($observacion == ""){
        $observacion = $observacionfijo;
      }
    	DB::table('inventario')->where("id","=",$id)
        ->update(['observacion'=>$observacion,'estado'=>$estado,'cantidad'=>$cantidad]);
        $session="correcto";
        $nota="Elemento Editado Correctament";
        $elementos = Inventario::paginate(4);
        return view("Inventario.listar",compact("elementos"))->with($session,$nota);
     }
    public function eliminar(){
    	extract($_GET);
    	$area = Inventario::where("id","=",$id)->delete();
    	return redirect("inventario/listar");
    }
    public function listar(){
    	$elementos = Inventario::all();
    	//dd($areas);
    	return view("Inventario.listar",compact("elementos"));
    }
    public function crear(){
    	extract($_POST);
    	$Objeto = new Inventario;
    	$Objeto->nombre = $elemento;
    	$Objeto->estado = $estado;
        $Objeto->observacion = $observacion;
        $Objeto->cantidad = $cantidad;
        $Objeto->tipo_elemento = $tipo_elemento;
    	$Objeto->save();
        $session="correcto";
        $nota="Se registro el Elemento Correctamente";
		return redirect("inventario/crear")->with($session,$nota);;
    }   
     public function darelemento()
          {
               extract($_GET);
               $tipo_elemento = Elemento::where('tipo_elemento','=',$id)
               ->get();
               //dd($tipo_elemento);
               echo '<option value="">seleccione</option>';
               foreach($tipo_elemento as $elemento)
               {
                    echo '<option value="'.$elemento->nombre.'">'.$elemento->nombre.'</option>';
               }
          } 
}

<?php

namespace Sagaz\Http\Controllers;
use Illuminate\Http\Request;
use Sagaz\User;
use DB;
use Encrypter;
use Auth;
use Session;
class UsuarioController extends Controller
{

    public function __construct(){
        $this->middleware("UsuarioCheck",["only"=>["postregistro"]]);
    }

    public function index()
    {
    	$usuarios = User::paginate(8);
    	return view("Usuarios.indexUsuario",compact("usuarios"));
    }
    public function registrar()
    { 
        return view("Usuarios.crear_usuario");
    }
    public function postregistro()
    {
        extract($_POST);
        if (isset($_FILES['perfil'])){
            $file = $_FILES["perfil"];
            if($file["name"] == null){
                $carpeta = "images/user_foto/";
                $src = $carpeta . "user.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/user_foto/";
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
        $usuario = new User;
        $usuario->name=$nombre;
        $usuario->email=$correo;
        $usuario->documento=$doc;
        $usuario->rol=$cargo;
        $usuario->user_img=$src;
        $usuario->password=bcrypt($pass);
        $usuario->user_estado="Activo";
        if($usuario->save() == true){
            return redirect("usuarios/index")->with("correcto","Usuario Registrado Correctamente");    
        }else{
            return redirect("usuarios/index")->with("incorrecto","Datos de usuario incorrectos verifiquelos");
        };

}


    	
    }
    public function cambiarestado()
    {
    	extract($_GET);
    	$usuarios = User::where("id","=",$usuario)->get();
    	if($usuarios[0]->user_estado == $accion)
        {
    	   return redirect("usuarios/index")->with("incorrecto","Usuario ya ".$accion);
        }else
        {
        	DB::table('users')
            ->where('id', $usuario)
            ->update(['user_estado' => $accion]);
        	return redirect("usuarios/index")->with("correcto","Usuario ".$accion." Correctamente Correctamente");
        }	
    }
    public function eliminar()
    {
        extract($_GET);
        $usuario = User::where("id","=",$usuario)->delete();

        return redirect("usuarios/index")->with("correcto","Usuario Eliminado Correctamente");
           
    }
    public function perfil()
    {   
    return view("Usuarios.perfil");
           
    }  
     public function editar()
    {   
        extract($_POST);
        extract($_FILES); 
        
        if (isset($_FILES['perfil'])){
            $opc = 2;
            $file = $_FILES["perfil"];
            if($file["name"] == null){
                $carpeta = "images/user_foto/";
                $src = $carpeta . "user.jpg";
            }else{
            $opc =1;
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/user_foto/";
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
        if($opc == 1){
            $srcc=$src;
        }else{
            $srcc=Auth::user()->user_img;
        }
        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['name' => $nombre,'documento'=>$doc,'user_img'=>$srcc]);

        return redirect("usuarios/perfil")->with("correcto","Datos Actualizados Correctamente");
           
        }
    }    

}

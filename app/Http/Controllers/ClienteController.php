<?php
namespace Sagaz\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use DB;
use Sagaz\Cliente;
use Sagaz\Area;
class ClienteController extends Controller
{
    /*LISTA DE CLIENTES CON SUS OPCIONES*/
    public function index(){
        $clientes = Cliente::paginate(5);
        return view("Clientes.indexclientes",compact("clientes"));
    }
    /*MUESTRA LA VISTA DE EDITAR CLIENTES Y LA CARGA*/
    public function editar(){
        extract($_GET);
        $cliente = Cliente::where("id_cliente","=",$id)->get();
        return view("Clientes.editarclientes",compact("cliente"));
    }
    /*VISTA PARA CREAR CLIENTE*/                
    public function crear(){   
        $areas = Area::all();
        return view("Clientes.crearclientes",compact("areas"));
    }
    /*ELIMINA CLIENTE*/
    public function eliminar(){
        extract($_GET);
        DB::table('clientes')->where("id_cliente",$id)->delete();
        return redirect("clientes/index")->with("correcto","Cliente Eliminado Correctamente");
    }
    /*RESGISTRO DE CLIENTES*/        
    public function registrar(){
        extract($_POST);
        if(isset($opc)){
            $estado="Pendiente";
            $ruta = "/";
        }else{
            $estado="Activo";  
            $ruta = "clientes/crear";    
        }
        $cliente= new Cliente;
        $cliente->nombre_cliente=$nombre;
        $cliente->nit=$nit;
        $cliente->ciudad=$ciudad;
        $cliente->direccion=$direccion;
        $cliente->telefono=$telefono;
        $cliente->correo=$correo;
        $cliente->estado=$estado;
        $cliente->save();
        return redirect($ruta)->with("correcto","Se Registro El CLiente Correctamente");
    }
    /*EDITA CLIENTE*/        
    public function update(){
        extract($_POST);
        DB::table('clientes')->where('id_cliente', $id)->update(['correo'=>$correo,'telefono'=>$telefono,'direccion'=>$direccion,'ciudad'=>$ciudad,'nit'=>$nit,'nombre_cliente'=>$nombre]);
        return redirect("clientes/index")->with("correcto","Se Actualizo El CLiente Correctamente");
    }
    /* ACTIVA EL ESTADO DE UN CLIENTE */
    public function activar(){
        extract($_GET);
        DB::table('clientes')->where('id_cliente', $id)->update(['estado' => 'Activo']);
        return redirect("clientes/index")->with("correcto","Se ACTIVO El CLiente Correctamente");
    }              
}

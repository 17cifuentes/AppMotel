<?php

namespace Sagaz\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Sagaz\Http\Middleware\TrimStrings::class,        
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Sagaz\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
             \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Sagaz\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \Sagaz\Http\Middleware\RedirectIfAuthenticated::class,
        'rol' => \Sagaz\Http\Middleware\rol::class,
        'financiero' => \Sagaz\Http\Middleware\Financiero::class,
        'gerente' => \Sagaz\Http\Middleware\Gerente::class,
        'diseno' => \Sagaz\Http\Middleware\Diseno::class,
        'comercial' => \Sagaz\Http\Middleware\Comercial::class,
        'logistica' => \Sagaz\Http\Middleware\Logistica::class,
        'admin' => \Sagaz\Http\Middleware\Admin::class,
        'RHumanos' => \Sagaz\Http\Middleware\RHumanos::class,
        'UsuarioCheck' => \Sagaz\Http\Middleware\UsuarioCheck::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

    ];
}

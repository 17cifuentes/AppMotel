<?php

namespace Sagaz\Http\Middleware;

use Closure;
use Sagaz\User;
class UsuarioCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        
        $email=User::where("email","=",$request->correo)->get()->ToArray();
        $doc=User::where("documento","=",$request->doc)->get()->ToArray();
        if($email != null){
            return redirect("usuarios/registrar")->with("incorrecto","Correo ya registrado");
        }else if($doc != null){
            return redirect("usuarios/registrar")->with("incorrecto"," Documento ya registrado");
        }else{
            
            return $next($request);
        }
        
    }
}

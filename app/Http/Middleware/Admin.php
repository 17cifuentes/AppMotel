<?php

namespace Sagaz\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        protected $auth;
    public function __construct(Guard $auth){
        
        $this->auth=$auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($this->auth->user()->tipoUsuario );
        if($this->auth->user()->rol == "Administrador"){
            return $next($request);
        }else{
          return redirect()->to("login");            
        }            
    }
    }
}

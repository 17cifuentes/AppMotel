<?php

namespace Sagaz\Http\Middleware;
use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Redirect;
use Session;
class rol{
protected $auth;
    public function __construct(Guard $auth){
        
        $this->auth=$auth;

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($this->auth->user()->rol );
        if($this->auth->user()->user_estado == "Activo"){
        if($this->auth->user()->rol == "Gerente")
        {
            return redirect()->to("gerente/principal");

        }else if($this->auth->user()->rol == "Financiero")
        {
            return redirect()->to("financiero/principal");
        }
        else if($this->auth->user()->rol == "Logistica")
        {
            return redirect()->to("logistica/principal");
        }
        else if($this->auth->user()->rol == "Diseño")
        {
            return redirect()->to("diseno/principal");

        }else if($this->auth->user()->rol == "Comercial")
        {
            return redirect()->to("comercial/principal");

        }else if($this->auth->user()->rol == "Auxiliares")
        {
            return redirect()->to("comercial/principal");

        }
        else if($this->auth->user()->rol == "Administrador")
        {
            return redirect()->to("gerente/principal");

        }else if($this->auth->user()->rol == "R-Humanos")
        {
            
            return redirect()->to("humanos/principal");
        }else
        {
               return $next($request);
        }
        
        }else{
               return $next($request);
        }



               
    }
}

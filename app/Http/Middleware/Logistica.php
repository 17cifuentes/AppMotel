<?php

namespace Sagaz\Http\Middleware;
use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Redirect;

class Logistica
{
        public function __construct(Guard $auth){
        
        $this->auth=$auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->auth->user()->rol == "Logistica"){
            return $next($request);
        }else{
          return redirect()->to("login");            
        }   
    }
}

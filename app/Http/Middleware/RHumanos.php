<?php

namespace Sagaz\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
class RHumanos
{
        public function __construct(Guard $auth){
        
        $this->auth=$auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->auth->user()->rol == "R-Humanos"){
            return $next($request);
        }else{
            return redirect()->to("login");            
        }   
    }
}
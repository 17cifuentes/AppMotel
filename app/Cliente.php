<?php

namespace Sagaz;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
   protected $table = 'clientes';

   protected $fillable = [
        'nombre', 'nit', 'ciudad', 'direccion', 'telefono','correo'
    ];

        public function FacturaSagaz(){
    	return $this->hasMany('Sagaz\FacturaSagaz');
    }

        public function Responsable(){
    	return $this->hasMany('Sagaz\Responsable');
    }
}

<?php


Route::group(['prefix' => 'inventario'],function(){
    Route::get('crear', 'InventarioController@index');
    Route::get('cargarPlantilla', 'InventarioController@cargarPlantilla');
    Route::post('registrar','InventarioController@crear');
    Route::get('listar','InventarioController@listar');
    Route::get('editar','InventarioController@editar');
    Route::post('editarpost','InventarioController@editarpost');
    Route::get('eliminar','InventarioController@eliminar');
    Route::get('inventario/darelemento','InventarioController@darelemento');
});
Route::group(['prefix' => 'area'],function(){
    Route::get('crear', 'AreaController@index');
    Route::post('registrar','AreaController@crear');
    Route::get('listar','AreaController@listar');
    Route::get('editar','AreaController@editar');
    Route::post('editarpost','AreaController@editarpost');
    Route::get('eliminar','AreaController@eliminar');
});
Route::group(['prefix' => 'financiero'],function(){
    Route::get('principal', 'FinancieroController@principal');
    Route::get('index','FinancieroController@index');
    Route::get('listar','FinancieroController@listar');
    Route::get('financiero/vercategorias','CategoriaProductoController@vercategorias');
    Route::get('financiero/valorpro','CategoriaProductoController@valorpro');
    Route::get('financiero/addproducto','FinancieroController@addproducto');
    Route::get('financiero/totales','FinancieroController@totales');
    Route::any('crear','FinancieroController@store');
    Route::get('financiero/darresponsable','FinancieroController@darresponsable');
    Route::get('financiero/dararea','FinancieroController@dararea');
    Route::get('Financieropdf','FinancieroController@Financieropdf');
    Route::get('pdf','FinancieroController@pdf');
    Route::get('anular','FinancieroController@anular');
    Route::get('editar','FinancieroController@editar');
    Route::get('finalizar','FinancieroController@finalizar');
    Route::get('financiero/quitarproducto','FinancieroController@quitarproducto');
    Route::any('posteditar','FinancieroController@posteditar');
    Route::get('financiero/editpro','ProductoController@editpro');
    Route::get('cambioestados','FinancieroController@cambioestados');
    Route::get('pdfver','FinancieroController@pdfver');

});
Route::group(['prefix' => 'clientes'],function(){
    Route::get('index','ClienteController@index');
    Route::get('editar','ClienteController@editar');
    Route::get('eliminar','ClienteController@eliminar');
    Route::get('crear','ClienteController@crear');
    Route::post('registrar','ClienteController@registrar');
    Route::post('update','ClienteController@update');
    Route::get('activar','ClienteController@activar');
});
Route::group(['prefix' => 'diseno'],function(){
    Route::get('principal','DisenoController@principal');
    Route::get('cargar','DisenoController@cargar');
    Route::get('listar','DisenoController@listar');
    Route::get('eliminar','DisenoController@eliminar');
    Route::post('postcargar','DisenoController@postcargar');
    
});
Route::group(['prefix' => 'gerente'],function(){
    Route::get('principal','GerenteController@principal');
    Route::get('editar','ClienteController@editar');
    Route::get('eliminar','ClienteController@eliminar');
    Route::get('crear','ClienteController@crear');
    Route::post('registrar','ClienteController@registrar');
    Route::post('update','ClienteController@update');
});

Route::group(['prefix' => 'responsables'],function(){
    Route::get('index','ResponsableController@index');
    Route::get('crear','ResponsableController@crear');
    Route::post('registrar','ResponsableController@registrar');
    Route::get('eliminar','ResponsableController@eliminar');
    Route::get('editar','ResponsableController@editar');
    Route::post('update','ResponsableController@update');
    Route::get('financiero/dararea','FinancieroController@dararea');
    Route::get('financiero/darresponsable','FinancieroController@darresponsable');
});
Route::group(['prefix' => 'comercial'],function(){
    Route::get('principal','ComercialController@index');
    Route::get('crear','ResponsableController@crear');
    Route::post('registrar','ResponsableController@registrar');
    Route::get('eliminar','ResponsableController@eliminar');
    Route::get('editar','ResponsableController@editar');
    Route::post('update','ResponsableController@update');
});
Route::group(['prefix' => 'logistica'],function(){
    Route::get('principal','LogisticaController@principal');
    Route::get('crear','ResponsableController@crear');
    Route::post('registrar','ResponsableController@registrar');
    Route::get('eliminar','ResponsableController@eliminar');
    Route::get('editar','ResponsableController@editar');
    Route::post('update','ResponsableController@update');
});
Route::group(['prefix' => 'productos'],function(){
    Route::get('index','ProductoController@index');
    Route::get('crear','ProductoController@crear');
    Route::post('registrar','ProductoController@registrar');
    Route::get('eliminar','ProductoController@eliminar');
    Route::get('editar','ProductoController@editar');
    Route::post('update','ProductoController@update');
    Route::post('registrarpro','ProductoController@registrarpro');
});
Route::group(['prefix' => 'reportes'],function(){
    //Reportes de almuerzos
    Route::get('almuerzos','ReportesController@filtrosAlmuerzo');
    //Reportes de Facturacion
    Route::get('filtros','ReportesController@filtros');
    Route::post('generar','ReportesController@generar');
    //Reportes de Cotizacion
    Route::get('filtroscom','ReportesController@filtroscom');
    //Reportes de Inversion
    Route::post('generaralmu','ReportesController@generaralmu');
});
Route::group(['prefix' => 'usuarios'],function(){
    Route::get('index','UsuarioController@index');
    Route::get('cambiarestado','UsuarioController@cambiarestado');
    Route::get('registrar','UsuarioController@registrar');
    Route::get('eliminar','UsuarioController@eliminar');
    Route::get('perfil','UsuarioController@perfil');
    Route::post('postregistro','UsuarioController@postregistro');
    Route::post('editar','UsuarioController@editar');
});
Route::group(['prefix' => 'inversiones'],function(){
    Route::get('nueva','InversionController@nueva');
    Route::get('existente','Inversion@existente');
    Route::get('aportar','InversionController@aportar');

});

Route::group(['prefix' => 'cotizacion'],function(){
    Route::get('facturar', 'CotizacionController@facturar');
    Route::get('cotizar', 'CotizacionController@cotizar');
    Route::get('inversion', 'CotizacionController@inversion');
    Route::get('cotipdf', 'CotizacionController@cotipdf');
    Route::get('listar', 'CotizacionController@index');
    Route::get('anular', 'CotizacionController@anular');
    Route::get('editar', 'CotizacionController@editar');
    Route::post('posteditar', 'CotizacionController@posteditar');
    Route::get('enviarcorreo', 'CotizacionController@enviarcorreo');
    Route::get('financiero/dararea', 'FinancieroController@dararea');
    Route::get('financiero/darresponsable','FinancieroController@darresponsable');
    Route::get('financiero/quitarproducto','FinancieroController@quitarproducto');
    Route::get('financiero/vercategorias','CategoriaProductoController@vercategorias');
    Route::get('financiero/valorpro','CategoriaProductoController@valorpro');
    Route::get('financiero/addproducto','FinancieroController@addproducto');
    Route::get('financiero/totales','FinancieroController@totales');
    Route::get('financiero/editpro','ProductoController@editpro');

});
Route::group(['prefix' => 'inversion'],function(){
    Route::get('facturar', 'InversionController@facturar');
    Route::get('pdf', 'InversionController@invpdf');
    Route::get('listar', 'InversionController@index');
    Route::get('anular', 'InversionController@anular');
    Route::get('editar', 'InversionController@editar');
    Route::post('posteditar', 'InversionController@posteditar');
    Route::get('enviarcorreo', 'InversionController@enviarcorreo');
    Route::get('daraporte','InversionController@daraporte');
    Route::get('financiero/dararea', 'FinancieroController@dararea');
    Route::get('financiero/darresponsable','FinancieroController@darresponsable');
    Route::get('financiero/quitarproducto','FinancieroController@quitarproducto');
    Route::get('financiero/vercategorias','CategoriaProductoController@vercategorias');
    Route::get('financiero/valorpro','CategoriaProductoController@valorpro');
    Route::get('financiero/addproducto','FinancieroController@addproducto');
    Route::get('financiero/totales','FinancieroController@totales');
    Route::get('financiero/editpro','ProductoController@editpro');
});
Route::group(['prefix' => 'almuerzos'],function(){
    Route::get('reportar','AlmuerzosController@reportar');
    Route::post('elegir','AlmuerzosController@elegir');
    
});

Route::group(['prefix' => 'humanos'],function(){
    Route::get('principal','RHumanosController@principal');
    Route::get('registrogasto','RHumanosController@registrargasto');
    Route::post('nuevogasto','RHumanosController@nuevogasto');
});

Route::get('portal/contacto','PortalController@contacto');
Route::get('/', function () {
    return view('index');
});

Route::get('corporativo',function(){
    return view("viewcorporativo");
});
Route::get('corporativo1', 'CorporativoController@index');
Route::get('graficos/valores', 'CorporativoController@grafico1');
Auth::routes();


Route::get('home', 'HomeController@index');
Route::get('proyectos',function(){
    return view('proyectos');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
